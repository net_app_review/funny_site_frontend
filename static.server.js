'use strict';

//----------------------------------------------------- Define Constants -------------------------------------------//
const PORT = 8080;
const HOST = '0.0.0.0';

const express = require('express');
const path = require('path');
var cors = require('cors')
var compression = require('compression')
const app = express();

// using user agent
// var useragent = require('express-useragent');

//----------------------------------------------------- Start prerender server ------------------------------------//

// const prerender = require('prerender');
// const server = prerender();
// server.start();
// const rendertron = require('rendertron-middleware');
// const bodyParser = require('body-parser')

//----------------------------------------------------- User compression -------------------------------------------//
app.use(compression())



// const root = require('path').join(__dirname, 'client', 'build')



// disable cache

const nocache = require('nocache');

app.use(nocache());


// cors allow all
app.use(cors())

app.get('/ping', function (req, res) {
  //  return res.send('pondcmg');
  return res.redirect('google.com')
});

app.use(require('prerender-node').set('prerenderToken', 'HXJ0s4RZtS9A3ZXT59iZ'));

app.use((req, res, next) => {
  if (
    req.path.slice(-5) === '.jpeg' ||
    req.path.slice(-4) === '.jpg' ||
    req.path.slice(-4) === '.svg' ||
    req.path.slice(-4) === '.png' ||
    req.path.slice(-4) === '.gif' ||
    req.path.slice(-4) === '.css' ||
    req.path.slice(-3) === '.js'
  ) {
    res.redirect( `https://funniq-cdn.azureedge.net${req.path}` );
  } else {
      next();
  }
});

app.use(express.static(path.join(__dirname, 'build')));  

// app.use(require('prerender-node').set('host', 'example.com'));

// catch 404 and forward to error handler

app.get('/', function (req, res) {  
  res.sendFile(path.join(__dirname, 'build', 'index.html'));  
  // res.send(req.useragent);
  console.log(res.statusCode)
});

app.get('/categories/*', function (req, res) {    
  res.sendFile(path.join(__dirname, 'build', 'index.html'));  
  console.log(res.statusCode)
});

app.get('/articles/*', function (req, res) {    
  res.sendFile(path.join(__dirname, 'build', 'index.html'));  
  console.log(res.statusCode)
});

app.get('/users/*', function (req, res) {    
  res.sendFile(path.join(__dirname, 'build', 'index.html'));  
  console.log(res.statusCode)
});

app.get('/tags/*', function (req, res) {    
  res.sendFile(path.join(__dirname, 'build', 'index.html'));  
  console.log(res.statusCode)
});

app.get('/settings', function (req, res) {    
  res.sendFile(path.join(__dirname, 'build', 'index.html'));  
  console.log(res.statusCode)
});

app.get('/about/*', function (req, res) {    
  res.sendFile(path.join(__dirname, 'build', 'index.html'));  
  console.log(res.statusCode)
});

app.get('/search/*', function (req, res) {    
  res.sendFile(path.join(__dirname, 'build', 'index.html'));  
  console.log(res.statusCode)
});

app.get('/hot', function (req, res) {    
  res.sendFile(path.join(__dirname, 'build', 'index.html'));  
  console.log(res.statusCode)
});

app.get('/fresh', function (req, res) {    
  res.sendFile(path.join(__dirname, 'build', 'index.html'));  
  console.log(res.statusCode)
});

app.get('/trending', function (req, res) {    
  res.sendFile(path.join(__dirname, 'build', 'index.html'));  
  console.log(res.statusCode)
});

app.get('*', function(req, res, next) {        
  if (req.headers.host.includes("localhost")) {
    console.log(req.headers.host)
    console.log(req.url)
    res.status(301)
    console.log(res.statusCode)
    res.redirect('https://funniq.com' + req.url)
    // res.redirect('https://' + req.headers.host.replace(/^www\./, '') + req.url);    
  } else {
    next();     
  }

  // if (req.headers.host.match(/^www/) !== null ) {    
  //   res.redirect('https://funniq.com' + req.url);
  //   // res.redirect('https://' + req.headers.host.replace(/^www\./, '') + req.url);
  //   res.status(301);
  // } else {
  //   next();     
  // }
})


app.get('*', function (req, res) {    
  res.sendFile(path.join(__dirname, 'build', 'index.html'));  
  res.status(404)
  console.log(res.statusCode)
});



// app.use(function(req, res, next) {
//   // var err = new Error('Not Found');
//   // err.status = 404;
//   res.sendFile(path.join(__dirname, 'build', 'index.html'));  
//   res.status(404)
//   next();
//   console.log(res.statusCode)
// });


// app.use(function(req,res){
//   res.status(404).sendFile(path.join(__dirname, 'build', 'index.html'));
//   console.log(res.statusCode)
// });

// app.listen(process.env.PORT || 8080);
app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);