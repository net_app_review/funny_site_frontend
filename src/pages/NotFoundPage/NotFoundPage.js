import React, {useEffect} from 'react';
import './NotFoundPage.sass'
import { MDBContainer} from "mdbreact";

const NotFoundPage = props => {

    useEffect(() => {
        window.scrollTo(0, 0)
    }, []);    


    return (
        <MDBContainer className="home mt-5">            
            <h1 className='text-center mt-5'>
                404 page
            </h1>
        </MDBContainer>
    );
};

NotFoundPage.propTypes = {

};

export default NotFoundPage;
