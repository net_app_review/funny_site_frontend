import React, {useEffect, useState} from 'react';
// import PropTypes from 'prop-types';
import './PersonalArticles.sass'
// import {articlesService} from "../../../services/articlesService";
import Fun from "../../Shared/Fun/Fun"
import ScrollImageLoader from "../../Shared/ScrollImageLoader";
// import {LoggedInContext} from '../../../Store'
import {tagPageService} from "../../../services/tagPageService"
import {removeHidingArticles} from "../../../functions/removeHidingArticles"

const SCROLL_OFFSET = 50

const TagArticles = props => {

    const [stPage, setStPage] = useState(1);
    const [stArticles, setStArticles] = useState([]);    
    const [stIsBottom, setStIsBottom] = useState(false);
    // const [LoggedIn, setLoggedIn] = useContext(LoggedInContext);    

    useEffect(() => {
        fetchTagArticles()
    }, [stPage]);

    const fetchTagArticles = () => {
        tagPageService.getArticles(props.tag_id,stPage)
            .then(result => {                
                setStArticles([...stArticles, ...removeHidingArticles.removeHidedArticle(result.data,"hide")])
        })
    }


    useEffect(() => {        
        const onScroll = () => {
            if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - SCROLL_OFFSET) {
                if (!stIsBottom) {
                    setStIsBottom(true)                
                    setStPage(stPage+1)                    
                }
            } else {
                setStIsBottom(false)
            }
        }

        window.addEventListener('scroll', onScroll)

        return () => {
            window.removeEventListener('scroll', onScroll)
        };
    }, [stIsBottom, stPage])
    

    return (
        <div className="fun-zone">

            {stArticles.length > 0 && stArticles.map(article => {
                return (
                    <Fun key={article.id} article={article}/>
                )
            })}
            {stArticles >=5 && <ScrollImageLoader/>}

        </div>
    );
};


TagArticles.propTypes = {};

export default TagArticles;