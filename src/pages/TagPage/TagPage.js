import React, {useEffect, useState} from 'react';

import './TagPage.sass'
import TagArticles from "./TagArticles/TagArticles";
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import TagDescription from './TagDescription/TagDescription'
import LeftStickyNav from '../Shared/LeftStickyNav/LeftStickyNav'
import RightStickyNav from '../Shared/RightStickyNav/RightStickyNav'
import SEO from "../../services/seoService";

const TagPage = props => {

    const [stSeo, setstSeo] = useState({})

    useEffect(() => {
        window.scrollTo(0, 0)
        standardizeTitle()
        // setstSeo({ title: response.data.header})
					// image:"https://picsum.photos/id/52/1200/600", 
					// url:"yetanotherawesomeapp.com" })
    }, []);    

    const standardizeTitle = (tag_name) => {
        // let final_string = props.location.pathname.replace("/tags/", "")
        // final_string = final_string.replace("-", "")
        // final_string = final_string.replace(/[0-9]/g, '')       
        setstSeo({ title: "The greatest " + tag_name + " content ever"})    
    }    

    return (
        <MDBContainer className="home"> 
            <SEO pageProps={stSeo} />           
            <MDBRow className="tag_content">
                <MDBCol sm={3} lg={3} md={3} xl={3} xs={4} className="hidden-xs pl-0">
                    <LeftStickyNav/>                    
                </MDBCol>
                <MDBCol sm={6} lg={6} md={6} xl={6} xs={7} middle>
                    <TagDescription     pop_tag_title = {standardizeTitle}
                                        tag_id = {props.match.params.id}/>
                    <TagArticles key = {props.match.params.id}
                                      tag_id = {props.match.params.id}/>
                </MDBCol>
                <MDBCol sm={3} lg={3} md={3} xl={3} xs={4} className="hidden-xs pl-0 pr-0">
                    <RightStickyNav/>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    );
};

TagPage.propTypes = {

};

export default TagPage;
