import React, {useEffect, useState} from 'react';
import axios from 'axios'
import {MDBCol, MDBContainer, MDBRow} from "mdbreact";
import "./TagDescription.module.sass"
import swal from 'sweetalert'

const TagDescription = props => {

    const [stTagInfo, setstTagInfo] = useState({});    
    // const [stImgLightBox, setstImgLightBox] = useState(false);

    useEffect(() => {	        
        fetchTagInfo()          
	},[props.tag_id])    

	const fetchTagInfo = () => {
		axios.get(`${process.env.REACT_APP_API_URL}/api/v1/tags/${props.tag_id}`)
			.then(response => {								  
                setstTagInfo(response.data)
                props.pop_tag_title(response.data.name)        	        
			})
			.catch(error => swal("Error", `${error}, please contact us at https://facebook.com/FunniqPage`, "error"))
    }
    

    return (        
        <MDBContainer className="box">
            <MDBRow >               
                <MDBCol size="12" className="tag-info pl-lg-0 pr-lg-0">
                    <MDBRow className="m-0" id="row-name" > 
                        <h3>
                            <strong>Tag: </strong> {stTagInfo.name}
                        </h3>
                    </MDBRow>
                    <MDBRow className="desc" >
                        <p>
                            {stTagInfo.description}
                        </p>
                    </MDBRow>
                </MDBCol>                
            </MDBRow> 
        </MDBContainer>
    );
};


TagDescription.propTypes = {};

export default TagDescription;