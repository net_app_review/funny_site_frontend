import React, {useState, useEffect } from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBIcon} from "mdbreact";
import LeftStickyNav from '../Shared/LeftStickyNav/LeftStickyNav'
import RightStickyNav from '../Shared/RightStickyNav/RightStickyNav'
import {userRankService} from "../../services/userRankService";
import LoadMoreButton from '../../components/LoadMoreButton/LoadMoreButton'
import {Link} from 'react-router-dom'
import UserLevelBadge from '../../components/UserLevelBadge/UserLevelBadge'
import UserNationalFlag from '../../components/UserNationalFlag/UserNationalFlag'
import './RankingPage.sass'
import swal from 'sweetalert'

const RankingPage = props => {

    const [stPage, setStPage] = useState(1);    
    const [stUserRankings, setstUserRankings] = useState([]);
    const [stMoonLoader, setstMoonLoader] = useState(false);
    // const [stShowLoadMore, ] = useState('');
    const [stWeeklyRank, setstWeeklyRank ] = useState(true);
    const [stShowLoadMore, setstShowLoadMore] = useState('');

    useEffect(() => { 
        if (stWeeklyRank){
            fetchWeeklyUserRankings()            
        } else {
            fetchUserRankings()
        }
    }, [stPage]);

    useEffect(() => {         
        if (stWeeklyRank){            
            fetchWeeklyUserRankings()
        } else {
            fetchUserRankings()            
        } 
        //Error: React Hook useEffect has missing dependencies: 'fetchUserRankings' and 'fetchWeeklyUserRankings'.
        // Either include them or remove the dependency array.eslint(react-hooks/exhaustive-deps)
    }, [stWeeklyRank]);

    const showingLoadMore = (length) => {
        if (length >= 0 && length < 3 ){
            setstShowLoadMore('none')
        } else {
            setstShowLoadMore('')
        }
    }

    const fetchUserRankings = () => {
        setstMoonLoader(true)
        userRankService.getuserRankings(stPage)
            .then(result => {                                
                if (result.data) {
                    setstUserRankings([...stUserRankings, ...result.data.rankings])
                }
                showingLoadMore(result.data.rankings.length)
                setstMoonLoader(false)
            }).catch(error => {
                swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
                setstMoonLoader(false)
        })
    }

    const fetchWeeklyUserRankings = () => {
        setstMoonLoader(true)
        userRankService.getCompetitionUserRankings(stPage)
            .then(result => {
                if (result.data) {
                    setstUserRankings([...stUserRankings, ...result.data.rankings])
                }
                showingLoadMore(result.data.rankings.length)                
                setstMoonLoader(false)
            }).catch(error => {
                swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
                setstMoonLoader(false)
        })
    }

    const loadMoreRankingUsers = () => {
        setStPage(stPage + 1)
        // setstRankOverflowY("scroll")
    }

    const converToString = (input) => {
        return input === 0 ? 'zero' : ''
    }

    const handleSwitchChange = (e) => {
        // let switchNumber = `switch${nr}`;
        // e.preventDefault()
        setstWeeklyRank(!stWeeklyRank);
        setStPage(1)
        setstUserRankings([])
    }

    return (
        <MDBContainer className="home">
            <MDBRow className="content">
                <MDBCol sm={0} lg={3} md={3} xl={3} xs={4} className="hidden-xs pl-0">
                    <LeftStickyNav/>
                </MDBCol>
                <MDBCol sm={12} lg={6} md={6} xl={6} xs={7} className='ranking_page_cmp' >
                    <div className='_title'>
                        User Ranking
                        <div className='custom-control custom-switch timeframe'>
                            <input
                            type='checkbox'
                            className='custom-control-input'
                            id='customSwitches'
                            checked={stWeeklyRank}
                            onChange={(e) => handleSwitchChange(e)}
                            readOnly
                            />
                            <label className='custom-control-label' htmlFor='customSwitches'>
                                {stWeeklyRank ? 'competition' : 'global'}
                            </label>
                        </div>                        
                    </div>                 
                    {stUserRankings.length > 0 && stUserRankings.map((user, index) => {
                        return (
                            <MDBRow className={`user_info ${converToString(index)}_position`} key={index}>
                                <MDBCol size="1" className={`user_index ${converToString(index)}_position`} >
                                    {index + 1}
                                </MDBCol >
                                <MDBCol size="2" className={`user_image ${converToString(index)}_position`}>
                                    <Link to={`/users/${user.uuid}-${user.slug}`}>
                                        <img src={user.image.url} alt={user.name}></img>                                
                                    </Link>
                                </MDBCol>
                                <MDBCol size="6" className="user_stats">
                                    <MDBRow className={`name ${converToString(index)}_position`}>
                                        <Link to={`/users/${user.uuid}-${user.slug}`} >                                
                                            {user.name}
                                        </Link>                                                           
                                        <UserLevelBadge level={user.level} height="1px"/>
                                        <div className={`flag ${converToString(index)}_position`}>
                                            <UserNationalFlag country_code={user.country_code} height="15vw"/>
                                        </div>                                                                  
                                        
                                    </MDBRow>
                                    
                                </MDBCol>                          
                                <MDBCol size="3" className="user_award">
                                    <div className={`points ${converToString(index)}_position`}>
                                        {user.points}                                                                              
                                    </div>           
                                    <div className={`trophy_cmp ${converToString(index)}_position`}>
                                        <MDBIcon icon="trophy" />
                                    </div>                                    
                                </MDBCol>
                            </MDBRow>
                        )
                    })}
                    <div className='load_more_btn'>
                        <LoadMoreButton loadMore={loadMoreRankingUsers} 
                            displayedText={'More users...'} 
                            isShown={stShowLoadMore}
                            stIsLoading={stMoonLoader}                            
                            />
                    </div>                    
                </MDBCol>
                <MDBCol sm={0} lg={3} md={3} xl={3} xs={4} className="hidden-xs pl-0 pr-0">
                    <RightStickyNav/>                    
                </MDBCol>
            </MDBRow>           
        </MDBContainer>
    );
};

RankingPage.propTypes = {

};

export default RankingPage;
