import React from 'react';
// import PropTypes from 'prop-types';
import {MDBCol, MDBContainer, MDBRow} from "mdbreact";

const TrendingDescription = props => {
    
    return (
        <MDBContainer>
            <MDBRow >
                <MDBCol size="2" className="pr-0">
                    <img className="img-fluid rounded" src="https://firebasestorage.googleapis.com/v0/b/funniq-78dbd.appspot.com/o/funniq_internal%2Ftrending.gif?alt=media&token=8186cf28-f373-469a-bb3c-859a7f59c491" alt="blank_user_icon"/>
                </MDBCol>
                <MDBCol size="10" >
                    <MDBRow style={{margin: '5px' }}> 
                        <h3>
                            Trending
                        </h3>
                    </MDBRow>
                    <MDBRow  style={{margin: '5px' }}>
                        <p>
                            Trending stuff here
                        </p>
                    </MDBRow>
                </MDBCol>                
            </MDBRow> 
        </MDBContainer>
    );
};


TrendingDescription.propTypes = {};

export default TrendingDescription;