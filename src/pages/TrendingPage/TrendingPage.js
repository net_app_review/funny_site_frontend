import React, {useEffect} from 'react';
import './TrendingPage.sass'
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import TrendingDescription from './TrendingDescription/TrendingDescription'
import LeftStickyNav from '../Shared/LeftStickyNav/LeftStickyNav'
import RightStickyNav from '../Shared/RightStickyNav/RightStickyNav'
import TrendingPageArticles from './TrendingArticles/TrendingArticles'


const TrendingPage = props => {

    useEffect(() => {
        window.scrollTo(0, 0)
    }, []);    

    return (
        <MDBContainer className="home">            
            <MDBRow className="trending_content">
                <MDBCol sm={3} lg={3} md={3} xl={3} xs={4} className="hidden-xs pl-0">
                    <LeftStickyNav/>                    
                </MDBCol>
                <MDBCol sm={6} lg={6} md={6} xl={6} xs={7} middle>
                    <TrendingDescription category_id = {props.match.params.id}/>
                    <TrendingPageArticles key = {props.match.params.id}
                                      category_id = {props.match.params.id}/>
                </MDBCol>
                <MDBCol sm={3} lg={3} md={3} xl={3} xs={4} className="hidden-xs pl-0 pr-0">
                    <RightStickyNav/>                    
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    );
};

TrendingPage.propTypes = {

};

export default TrendingPage;
