
import React ,{useEffect, useState, useContext} from 'react';
import { MDBContainer, 
         MDBInput,
         MDBCol,         
         MDBIcon,
         MDBBtn,
         MDBDropdown,
         MDBDropdownToggle,
         MDBDropdownMenu,
         MDBDropdownItem
        } from "mdbreact";
import './Feedback.sass';
// import {getDate} from '../../../functions/getDate'
import StarRatings from 'react-star-ratings';
import {feedbackService} from '../../../services/feedbackService'
// import {handleLikeFunction} from '../../../functions/handleLikeFunction'
import LoadMoreButton from '../../../components/LoadMoreButton/LoadMoreButton'
import {CurrentUserContext, LoggedInContext, SignInModalContext} from '../../../Store'
import Cookies from 'js-cookie';
import {calculateElapsedTime} from '../../../functions/calculateElapsedTime'
import {LikeService} from '../../../services/likeService';
import {calculateQuality} from '../../../functions/calculateQuality'
import {calculateLikePercent} from '../../../functions/calculateLikePercent'
import swal from 'sweetalert'

const Feedback = () => {
    
  const [stRating, setstRating] = useState(5)
  const [stUsername, setstUsername] = useState('');
  const [stEmail, setstEmail] = useState('');
  const [stFeedbacks, setstFeedbacks] = useState([]);
  const [stFeedbacksType, setstFeedbacksType] = useState(1);
  const [stNewFeedback, setstNewFeedback] = useState('');
  const [stNewFeedbackType, setstNewFeedbackType] = useState(1);
  const [stPage, setstPage] = useState(1)
  const [stIsShownLoadMore, setstIsShownLoadMore] = useState('')
  const [stMoonLoader, setstMoonLoader] = useState(false);
  const [stCurrentUser, ] = useContext(CurrentUserContext)
  const [LoggedIn, ] = useContext(LoggedInContext)
  const [, setSignInModal] = useContext(SignInModalContext)
  const [stFocusedFeedback, setstFocusedFeedback] = useState(0);
  const [stLikeId, setstLikeId] = useState(0);
  // const [stLikeType, setStLikeType] = useState('');
  // const  = useRef(null);

  useEffect(() => {        
    window.scrollTo(0, 0)    
    getAllFeedbacks()    
  }, []);

  useEffect(() => {            
    getAllFeedbacks()    
  }, [stPage]);

  useEffect(() => {      
    getAllFeedbacks()    
  }, [stFeedbacksType]);

  // useEffect(() => {      
  //   getAllFeedbacks()    
  // }, [stFeedbacks]);

  // const getFeedbackUserState = () => {
  //   localStorage.getItem('myValueInLocalStorage')
  // }

  // const isLoggedIn = () =>{
  //   let logged = false
  //   console.log(stCurrentUser.id)
  //   if(stCurrentUser.id !== undefined || stCurrentUser.id !== 'undefined') {
  //     logged = true
  //   }
  //   return logged
  // } 

  const getAllFeedbacks = () => {
    setstMoonLoader(true)    
    feedbackService.getAllFeedbacks(stPage, stFeedbacksType)
                   .then(result => {                                                        
                     setstFeedbacks([...stFeedbacks, ...result.data])
                     if (result.data.length < 5){
                      setstIsShownLoadMore('none')
                     }
                     setstMoonLoader(false)
                   }).catch(error => {
                    swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
                    setstMoonLoader(true)
                   })
  }

  const addNewCommentClient = () => {

    // let data = {
    //   content: "fedba1",
    //   email: "tuanvu0695@gmail.com",
    //   feedback_type: "feedback",
    //   is_anonymous: false,
    //   rating: 5,
    //   user_id: "35",
    //   user_imageurl: "https://firebasestorage.googleapis.com/v0/b/funniq-78dbd.appspot.com/o/funniq_production%2Fuser35-1598848095845?alt=media&token=70866926-cc20-49d6-a960-06b7bf900082",
    //   username: "Tuan Vu"
    // }

    if (LoggedIn === "NOT_LOGGED_IN") {        
      setstFeedbacks([{
        content: stNewFeedback,
        email: stEmail,
        feedback_type: getFeedbackType(stNewFeedbackType),
        is_anonymous: true,
        like_status: 'no_action',
        rating: stRating,
        user_id: "",
        user_imageurl: `https://ui-avatars.com/api/?name=${stEmail}`,
        username: stEmail,
        total_like_count: 0,
        elapsed_time: 0
      } , ...stFeedbacks])
    } else {
      setstFeedbacks([{
        content: stNewFeedback,
        email: stCurrentUser.email,
        feedback_type: getFeedbackType(stNewFeedbackType),
        is_anonymous: false,
        like_status: 'no_action',
        rating: stRating,
        user_id: "",
        user_imageurl: stCurrentUser.image.url,
        username: stCurrentUser.name,
        total_like_count: 0,
        elapsed_time: 0
      } , ...stFeedbacks])
      
      
    }
    window.scrollTo({
      behavior: "smooth",
      top: 0
    });
    // setstFeedbacks([data, ...stFeedbacks])

  }

  const changeRating = (newRating, name) => {
    setstRating(newRating)
  }

  const _handleFeedbackSubmit = (event) => {
    if(event){
      event.preventDefault()
      if (LoggedIn === "NOT_LOGGED_IN") {        
        feedbackService.createFeedback(Cookies.get("GuestTkn"), stUsername, stEmail, true, stNewFeedback, stRating, stNewFeedbackType)
      } else {
        feedbackService.createFeedback('', '', '', false, stNewFeedback, stRating, stNewFeedbackType)
      }      
      cleanForm()
      addNewCommentClient()
    }
  }

  const cleanForm = () => {
    setstUsername('')
    setstEmail('')
    setstNewFeedback('')
  }

  const FeedbackTypeonClick = (nr) => () => {
    setstNewFeedbackType (nr)
  }

  const getFeedbackType = (code) => {
    let type = ''
    if( code === 1 ){
      type = 'feedback'
    } else if (code === 2){
      type = 'bug'
    } else if (code === 3){
      type = 'new feature'
    }
    return type
  }  

  const renderFeedbackForm = () => {

    if (LoggedIn === "NOT_LOGGED_IN") {
      return (
        <div className='anonymous_feedback_form'>
          <form onSubmit={(e) => _handleFeedbackSubmit(e)}>
            <div className='_title'>              
              Please help us with your feedbacks              
            </div>
              <div className='feedback_types'>
                <MDBInput gap onClick={FeedbackTypeonClick(1)} 
                          checked={stNewFeedbackType===1 ? true : false} 
                          label="Feedback" 
                          type="radio"
                          id="radio1" />
                <MDBInput gap onClick={FeedbackTypeonClick(2)}
                          checked={stNewFeedbackType===2 ? true : false}
                          label="Bugs" 
                          type="radio"
                          id="radio2" />
                <MDBInput gap onClick={FeedbackTypeonClick(3)} 
                          checked={stNewFeedbackType===3 ? true : false}
                          label="New feature" 
                          type="radio"
                          id="radio3" />
                <div className='pt-2'>
                  <StarRatings
                    rating={stRating}
                    // starRatedColor="#FF9529"
                    // starHoverColor='#FDCC0D'
                    starDimension="1.8rem"
                    starSpacing="8px"
                    starRatedColor="#318f31"
                    starHoverColor='#318f31'
                    changeRating={changeRating}
                    numberOfStars={5}
                    name='rating'
                  />
                </div>            
              </div>
                <div className='d-flex flex-row justify-content-center'>
                  <input
                    type="text"
                    id="feedbackUsername"
                    className="form-control mt-1 mr-2"
                    name="username"
                    placeholder="Enter your name"
                    value={stUsername}
                    onChange={(e) => {setstUsername(e.target.value)}}
                    required
                    />

                  <input
                    type="email"
                    id="feedbackEmail"
                    className="form-control mt-1"

                    name="email"
                    placeholder="Email"
                    value={stEmail}
                    onChange={(e) => {setstEmail(e.target.value)}}
                    required
                    />
                          
                      </div>                        
                      <textarea                   
                        maxLength="200"                 
                        onChange={(e) =>{setstNewFeedback(e.target.value)}}
                        value={stNewFeedback}
                        className="form-control feedback-textarea mt-3"
                        placeholder='Your feedback...'
                        name="feedback"
                        rows="5"
                        onKeyDown={(e) =>{setstNewFeedback(e.target.value)}}                          
                      />
                      <div className='feedback_btn d-flex flex-row justify-content-center'>
                        <button className='btn'>
                          Send Feedback
                        </button>
                      </div>
                      
              {/* How was your exprericnes
                             */}
          </form>          
        </div>
      )
    } else {
      return (
        <div className='anonymous_feedback_form'>
          <form onSubmit={(e) => _handleFeedbackSubmit(e)}>
            <div className='_title'>              
              Please help us with your feedbacks
            </div>
              <div className='feedback_types'>
                <MDBInput gap onClick={FeedbackTypeonClick(1)} 
                          checked={stNewFeedbackType===1 ? true : false} 
                          label="Feedback" 
                          type="radio"
                          id="radio1" />
                <MDBInput gap onClick={FeedbackTypeonClick(2)}
                          checked={stNewFeedbackType===2 ? true : false}
                          label="Bugs" 
                          type="radio"
                          id="radio2" />
                <MDBInput gap onClick={FeedbackTypeonClick(3)} 
                          checked={stNewFeedbackType===3 ? true : false}
                          label="New feature" 
                          type="radio"
                          id="radio3" />
                <div className='pt-2'>
                  <StarRatings
                    rating={stRating}
                    // starRatedColor="#FF9529"
                    // starHoverColor='#FDCC0D'
                    starDimension="1.8rem"
                    starSpacing="8px"
                    starRatedColor="#318f31"
                    starHoverColor='#318f31'
                    changeRating={changeRating}
                    numberOfStars={5}
                    name='rating'
                  />
                </div>            
              </div>                
                <textarea                   
                  maxLength="200"                 
                  onChange={(e) =>{setstNewFeedback(e.target.value)}}
                  value={stNewFeedback}
                  className="form-control feedback-textarea mt-3"
                  placeholder='Your feedback...'
                  name="feedback"
                  rows="5"
                  onKeyDown={(e) =>{setstNewFeedback(e.target.value)}}                          
                />
                <div className='feedback_btn d-flex flex-row justify-content-center'>
                  <button className='btn'>
                    Send Feedback
                  </button>
                </div>
                      
              {/* How was your exprericnes
                             */}
          </form>          
        </div>
      )
    }

    
    }
  
  const changeFeedbackRequestType = (event, _type) => {
    event.preventDefault()    
    // setstNewFeedback(_type)
    setstFeedbacks([])
    setstPage(1)
    setstFeedbacksType(_type)
  }

  const renderFeedbacksSection = () => {
    return(
      <div className="_feedbacks_cmp">        
        <MDBDropdown className='feedback_filter'>
          <MDBDropdownToggle color="primary" className={getFeedbackType(stFeedbacksType)}>            
            {getFeedbackType(stFeedbacksType)}
            <MDBIcon icon="caret-down" className='pl-2'/>
          </MDBDropdownToggle>
          <MDBDropdownMenu basic>
            <MDBDropdownItem onClick={(e) => changeFeedbackRequestType(e,1)}>{getFeedbackType(1)}</MDBDropdownItem>
            <MDBDropdownItem onClick={(e) => changeFeedbackRequestType(e,2)}>{getFeedbackType(2)}</MDBDropdownItem>
            <MDBDropdownItem onClick={(e) => changeFeedbackRequestType(e,3)}>{getFeedbackType(3)}</MDBDropdownItem>            
          </MDBDropdownMenu>
        </MDBDropdown>
        {stFeedbacks.length > 0 && stFeedbacks.map((feedback, index) => {
          return (
            <div className='feedback_content' key={index }>
              <MDBCol size="1" className="image_col">
                <img className="image" src={feedback.user_imageurl} alt="" />                
              </MDBCol>
              <div className='d-flex flex-column pl-3'>
                <div className='d-flex flex-row'>
                  <div className={`feedback_type ${feedback.feedback_type}`}>
                    {feedback.feedback_type}
                  </div>
                  <div className='feedback_rating'>
                    {Math.round(feedback.rating).toFixed(1)}
                    <MDBIcon icon="star" />
                  </div>
                  <div className={`feedback_anonymous ${feedback.is_anonymous ? 'anonymous' : ''}`}>
                    {feedback.is_anonymous ? '' : 'Verified User'}
                  </div>
                </div>                                                
                <div className="feedback_col">                
                  <div className='d-flex flex-row'>                                                    
                  </div>                
                  <div className='content'>
                    {feedback.content}
                  </div>                
                  <div className='d-flex flex-row'>
                    {renderLikeBtnSection(feedback)}
                    <div className="info_col">                                    
                      {feedback.username}
                    </div>
                    <div className='created_time'>
                      {calculateElapsedTime.elapsedTime(feedback.elapsed_time)}
                    </div>  
                  </div>
                </div>
              </div>
            </div>
          )
        })}
        <div className='load_more_btn'>
          <LoadMoreButton loadMore={loadMoreFeedbacks} 
                              displayedText={`More ${getFeedbackType(stFeedbacksType)}...`} 
                              isShown={stIsShownLoadMore}
                              stIsLoading={stMoonLoader}                            
                            />
        </div>        
      </div>
    )
  }

  const openLoginModal = () => {
    setSignInModal(true)
  }

  const onLikeClick = (event, feedback) => {
    event.preventDefault()    
    
    // console.log(stFeedbacks)
    let likeType = ''
    let likeId = 0

    // const [stLikeType, setStLikeType] = useState(props.article.like_status.liketype? props.article.like_status.liketype: props.article.like_status);

    if (LoggedIn === "NOT_LOGGED_IN") {
      openLoginModal()
      return
    }

    if (feedback.like_status === 'no_action'){
      likeType = feedback.like_status
      likeId = 0
    } else {
      likeType = feedback.like_status.liketype
      likeId = feedback.like_status.id
    }

    LikeService.like('feedback_id', feedback.id, likeType, likeId).then(result => 
      {                
        // setstFeedbacks([])
        window.location.reload()
        setstLikeId(result.id)                
      }
    )        
    // feedback.like_status.liketype = handleLikeFunction.handleLike(likeType)       
  }

  const onDislikeClick = (event, feedback) => {
    event.preventDefault()    
    let likeType = feedback.like_status
    let likeId = 0

    if (LoggedIn === "NOT_LOGGED_IN") {
      openLoginModal()
      return
    }

    if (feedback.like_status === 'no_action'){
      likeType = feedback.like_status
      likeId = 0
    } else {
      likeType = feedback.like_status.liketype
      likeId = feedback.like_status.id
    }

    LikeService.dislike('feedback_id', feedback.id, likeType, likeId).then(result => 
      {                
        window.location.reload()
        setstLikeId(result.id)
      }
    )
  }  

  // switch (stLikeType) {
  //     case LikeType.LIKE: // => change like to unlike
  //         setStLikeCount(stLikeCount-1)
  //         setStLikeType(LikeType.NO_VALUE);
  //         break
  //     case LikeType.DISLIKE:
  //         setStLikeType(LikeType.LIKE);
  //         setStLikeCount(stLikeCount+1)
  //         setStDislikeCount(stDislikeCount-1)
  //         break
  //     case LikeType.NO_VALUE:
  //         setStLikeCount(stLikeCount+1)
  //         setStLikeType(LikeType.LIKE);
  //         break
  //     case 'no_action':
  //         setStLikeType(LikeType.LIKE);
  //         setStLikeCount(stLikeCount+1)
  //         break
  //     default:
  //         console.log('default')
  // }
    
  
  const loadMoreFeedbacks = () => {
    setstPage(stPage + 1)
  }

  const isLiked = (like_status) => {
    return like_status === "like"
  }

  const isDisliked = (like_status) => {
    return like_status === "dislike"
  } 
   
  const renderLikeBtnSection = (feedback) => {
    return (
      <div className='like_section'>
        <MDBBtn className="button-like" onClick={(e) => onLikeClick(e,feedback)}>
          {
            isLiked(feedback.like_status.liketype)?<MDBIcon icon='arrow-alt-circle-up' className='liked'/>:<MDBIcon far icon='arrow-alt-circle-up'/>
          }          
        </MDBBtn>
        <div className={`percent_section`}>
          <div className={`percent ${calculateQuality.calcQuality(calculateLikePercent.calcPercent(feedback.like_count,feedback.total_like_count))}`}>
            {calculateLikePercent.calcPercent(feedback.like_count,feedback.total_like_count)}%
          </div>
          <div className='total_vote'>
            ({feedback.total_like_count})
          </div>
        </div>
        <MDBBtn className="button-dislike" onClick={(e) => onDislikeClick(e,feedback)}>
          {
            isDisliked(feedback.like_status.liketype)?<MDBIcon icon="arrow-alt-circle-down" className='disliked'/>:<MDBIcon far icon='arrow-alt-circle-down' />
          }          
        </MDBBtn>
      </div>
    )
  }

	return (
		<MDBContainer className='feedback_cmp'>
            <div className='feedback_parent'>              
              {renderFeedbacksSection()}
            </div>            
            <div className='d-flex flex-row justify-content-center'>              
              {renderFeedbackForm()}
            </div>
		</MDBContainer>   
	);
};

export default Feedback;