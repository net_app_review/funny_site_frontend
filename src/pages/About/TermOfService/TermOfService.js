import React, {useEffect} from 'react';
import { MDBContainer } from "mdbreact";
import {getDate} from '../../../functions/getDate'

const TermOfService = () => {

    // let tos_markdown =" \
    // # Funniq Terms of Service \
    // These Terms of Service (“Terms”) govern your access to and use of our services, and any information, text, links, graphics, photos, videos, or other materials or arrangements of materials uploaded, downloaded or appearing on the Services (collectively referred to as “Content”). By using the Services you agree to be bound by these Terms. \
    // ## 1. Who May Use the Services \
    // You may use the Services only if you agree to form a binding contract with Funniq and are not a person barred from receiving services under the laws of the applicable jurisdiction. In any case, you must be at least 13 years old to use the Services. If you are accepting these Terms and using the Services on behalf of a company, organization, government, or other legal entity, you represent and warrant that you are authorized to do so. \
    // "

    useEffect(() => {        
        window.scrollTo(0, 0)
    }, []); 

    return (
       <MDBContainer className='tos_container mt-5'>
           <div className='d-flex flex-column text-left mt-5 px-3'>
                <h1 className=''>
                    <strong>
                        Funniq Terms of Service
                    </strong>                                        
                </h1>
                <p>
                    These Terms of Service (“Terms”) govern your access to and use of our services,
                    and any information, text, links, graphics, photos, videos,
                    or other materials or arrangements of materials uploaded, downloaded or appearing on the Services 
                    (collectively referred to as “Content”). By using the Services you agree to be bound by these Terms.
                </p>
                <h2>
                    1. Who May Use the Services
                </h2>
                <p>
                    You may use the Services only if you agree to form a binding contract with Funniq and 
                    are not a person barred from receiving services 
                    under the laws of the applicable jurisdiction. In any case, you must be at least 13 years old to use the Services. 
                    If you are accepting these Terms and using the Services on behalf of a company, organization, government, or other 
                    legal entity, you represent and warrant that you are authorized to do so.
                </p>
                <h2>
                    2. Privacy
                </h2>
                <p>
                    Our Privacy Policy (https://funniq.com/about/privacy) describes how we handle the information you provide to us when you use our Services.
                    Your understand that through your use of the Services you consent to the collection and use (as set forth in the Privacy Policy) of this
                    information, including the transfer of this information to the United States, Ireland, and/or other countries forstorage, 
                    processing and use by Funniq and its affiliates.
                </p>
                <h2>
                    3. Content on the Services
                </h2>
                <p>
                    You are responsible for your use of the Services and for any Content you provide, including compliance with applicable laws, rules, and regulations. You should only provide Content that you are comfortable sharing with others.
                    Any use or reliance on any Content or materials posted via the Services or obtained by you through the Services is at your own risk. We do not endorse, support, represent or guarantee the completeness, truthfulness, accuracy, or reliability of any Content or communications posted via the Services or endorse any opinions expressed via the Services. You understand that by using the Services, you may be exposed to Content that 
                    might be offensive, harmful, inaccurate or otherwise inappropriate, or in some cases, postings that have been mislabeled or are otherwise deceptive. All Content is the sole responsibility of the person who originated such Content. We may not monitor or control the Content posted via the Services and, we cannot take responsibility for such Content.
                    Funniq respects the intellectual property rights of others and expects users of the Services to do the same. We reserve the right to remove Content alleged to be infringing without prior notice, at our sole discretion, and without liability to you. We will respond to notices of alleged copyright infringement that comply with applicable law and are properly provided to us, as described in our Copyright policy (https://Funniq.com/dmca).
                </p>
                    <h3>
                        Your Rights
                    </h3>
                    <p>
                        You retain your rights to any Content you submit, post or display on or through the Services. What’s yours is yours — you own your Content (and your photos and videos are part of the Content).
                        By submitting, posting or displaying Content on or through the Services, you grant us a worldwide, non-exclusive, royalty-free license (with the right to sublicense) to use, copy, reproduce, process, adapt, modify, publish, transmit, display and distribute such Content in any and all media or distribution methods (now known or later developed). 
                        This license authorizes us to make your Content available to the rest of the world and to let others do the same. You agree that this license includes the right for Funniq to provide, promote, and improve the Services and to make Content submitted to or through the Services available to other companies, organizations or individuals for the syndication, broadcast, distribution, promotion or publication of such Content on other media and services, subject to our terms and conditions for such Content use. 
                        Such additional uses by Funniq, or other companies, organizations or individuals, may be made with no compensation paid to you with respect to the Content that you submit, post, transmit or otherwise make available through the Services.
                        Funniq has an evolving set of rules for how ecosystem partners can interact with your Content on the Services. These rules exist to enable an open ecosystem with your rights in mind. You understand that we may modify or adapt your Content as it is distributed, 
                        syndicated, published, or broadcast by us and our partners and/or make changes to your Content in order to adapt the Content to different media. You represent and warrant that you have all the rights, power and authority necessary to grant the rights granted herein to any Content that you submit.
                    </p>
                <h2>
                    4. Using the Services
                </h2>
                <p>
                    Please review the Funniq Terms, which are part of the User Agreement and outline what is prohibited on the Services.
                    You may use the Services only in compliance with these Terms and all applicable laws, rules and regulations.
                    Our Services evolve constantly. As such, the Services may change from time to time, at our discretion. 
                    We may stop (permanently or temporarily) providing the Services or any features within the Services to you or to users generally. 
                    We also retain the right to create limits on use and storage at our sole discretion at any time. 
                    We may also remove or refuse to distribute any Content on the Services, suspend or terminate users, and reclaim usernames without liability to you.
                    In consideration for Funniq granting you access to and use of the Services, you agree that Funniq and its third-party providers and partners 
                    may place advertising on the Services or in connection with the display of Content or information from the Services whether submitted by you or others. You also agree not to misuse our Services, for example, by interfering with them or accessing them using a method other than the interface and the instructions that we provide. You may not do any of the following while accessing or using the Services: (i) access, tamper with, or use non-public areas of the Services, Funniq’s computer systems, or the technical delivery systems of Funniq’s providers; (ii) probe, scan, or test the vulnerability of any system or network or breach or circumvent any security or authentication measures;
                    (iii) access or search or attempt to access or search the Services by any means (automated or otherwise) other than through our currently available,
                    published interfaces that are provided by Funniq (and only pursuant to the applicable terms and conditions), unless you have been specifically allowed to do so in a separate agreement with Funniq (NOTE: crawling the Services is permissible if done in accordance with the provisions of the robots.txt file, however, scraping the Services without the prior consent of Funniq is expressly prohibited); (iv) forge any TCP/IP packet header or any part of the header information in any email or posting, or in any way use the Services to send altered, deceptive or false source-identifying information; or (v) interfere with, or disrupt, (or attempt to do so), the access of any user, host or network, including, without limitation, sending a virus, overloading, flooding, spamming, mail-bombing the Services, or by scripting the creation of Content in such a manner as to interfere with or create an undue burden on the Services. We also reserve the right to access, read, preserve, and disclose any information as we reasonably believe is necessary to (i) satisfy any applicable law, regulation, legal process or governmental request, (ii) enforce the Terms, including investigation of potential violations hereof, (iii) detect, prevent, or otherwise address fraud, security or technical issues, (iv) respond to user support requests, or (v) protect the rights, property or safety of Funniq, its users and the public. Funniq does not disclose personally-identifying information to third parties except in accordance with our Privacy Policy.
                </p>
                <h2>
                    5. Age restriction
                </h2>
                <p>
                    Generally, if you are under 13, you are not supposed to join this community.
                </p>
                <p>
                    Updated on: {getDate.getTheDate(10)}
                </p>             
           </div>           
       </MDBContainer>       
    );
};


export default TermOfService;