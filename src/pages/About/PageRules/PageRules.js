
import React ,{useEffect} from 'react';
import { MDBContainer } from "mdbreact";
import './PageRules.module.sass';
import {getDate} from '../../../functions/getDate'

const PageRules = () => {
    
    useEffect(() => {        
        window.scrollTo(0, 0)
    }, []); 

	return (
		<MDBContainer className='pl-3'>
            <h1 >				               
                <strong>
                    Funniq rules               
                </strong>                
			</h1>
            <h5>
                Updated on: {getDate.getTheDate(10)}
            </h5>
            <div className="pl-0">
            <br></br>      
            <p>
            Website rules and regulations
            Funniq, its subsidiaries and affiliates (‘Funniq’) requires that all users of our online sites, services, tools and applications (‘Online Sites & Services’) adhere to the following rules and regulations. By using the Online Sites & Services you indicate your acknowledgment and acceptance of these terms and conditions.
            <h5>
            Laws and regulations 
            </h5>
            User access to and use of the Online Sites & Services is subject to all applicable federal, state and local laws and regulations.
            <h5>
                Copyright/Trademarks 
            </h5>            
            The trademarks, logos and service marks (‘Marks’) displayed as part of the Online Sites & Services are the property of Funniq and other parties. Users are prohibited from using any Marks for any purpose including, but not limited to use as metatags on other pages or sites on the World Wide Web without the written permission of Funniq or such third party which may own the Marks. All information and content including any software programs available on or through the Online Sites & Services (‘Content’) is protected by copyright. Users are prohibited from modifying, copying, distributing, transmitting, displaying, publishing, selling, licensing, creating derivative works or using any Content available on or through the Online Sites & Services for commercial or public purposes.
            <h5>
            Hyperlinking 
            </h5>            

            THE ONLINE SITES & SERVICES MAY PROVIDE OR FACILITATE LINKS TO OTHER SITES BY ALLOWING THE USER TO LEAVE THE ONLINE SERVICE TO ACCESS THIRD-PARTY MATERIAL OR BY BRINGING THE THIRD-PARTY MATERIAL INTO AN ONLINE SERVICE VIA ‘INVERSE’ HYPERLINKS AND FRAMING TECHNOLOGY (A ‘LINKED SITE’). Funniq HAS NO DISCRETION TO ALTER, UPDATE, OR CONTROL THE CONTENT ON A LINKED SITE. THE FACT THAT Funniq HAS PROVIDED A LINK TO A SITE IS NOT AN ENDORSEMENT, AUTHORIZATION, SPONSORSHIP, OR AFFILIATION WITH RESPECT TO SUCH SITE, ITS OWNERS, OR ITS PROVIDERS. THERE ARE INHERENT RISKS IN RELYING UPON USING, OR RETRIEVING ANY INFORMATION FOUND ON THE INTERNET, AND Funniq URGES YOU TO MAKE SURE YOU UNDERSTAND THESE RISKS BEFORE RELYING UPON, USING, OR RETRIEVING ANY SUCH INFORMATION ON A LINKED SITE.

            <h5> 
                No warranties 
            </h5>

            CONTENT PROVIDED OR DISPLAYED AS PART OF THE ONLINE SITES & SERVICES IS ‘AS IS’ WITHOUT WARRANTY OF ANY KIND EITHER EXPRESS OR IMPLIED INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, TITLE, NON-INFRINGEMENT, SECURITY OR ACCURACY. Funniq DOES NOT ENDORSE AND IS NOT RESPONSIBLE FOR (A) THE ACCURACY OR RELIABILITY OF ANY OPINION, ADVICE OR STATEMENT MADE THROUGH AN ONLINE SERVICE BY ANY PARTY OTHER THAN Funniq, (B) ANY CONTENT PROVIDED ON LINKED SITES OR (C) THE CAPABILITIES OR RELIABILITY OF ANY PRODUCT OR SERVICE OBTAINED FROM A LINKED SITE. OTHER THAN AS REQUIRED UNDER APPLICABLE CONSUMER PROTECTION LAW, UNDER NO CIRCUMSTANCE WILL Funniq BE LIABLE FOR ANY LOSS OR DAMAGE CAUSED BY A USER'S RELIANCE ON INFORMATION OBTAINED THROUGH AN ONLINE SERVICE OR A LINKED SITE, OR USER'S RELIANCE ON ANY PRODUCT OR SERVICE OBTAINED FROM A LINKED SITE. IT IS THE RESPONSIBILITY OF THE USER TO EVALUATE THE ACCURACY, COMPLETENESS OR USEFULNESS OF ANY OPINION, ADVICE OR OTHER CONTENT AVAILABLE THROUGH AN ONLINE SERVICE, OR OBTAINED FROM A LINKED SITE. PLEASE SEEK THE ADVICE OF PROFESSIONALS, AS APPROPRIATE, REGARDING THE EVALUATION OF ANY SPECIFIC OPINION, ADVICE, PRODUCT, SERVICE, OR OTHER CONTENT.

            Limitation of liability for use of the Online Sites & Services and linked sites 

            ANY ERROR OR OMISSION IN ANY OF THE INFORMATION, SOFTWARE, PRODUCTS AND DESCRIPTIONS OF SERVICES PROVIDED OR DISPLAYED AS PART OF THE ONLINE SITES & SERVICES OR A LINKED SITE SHALL BE SUBJECT TO CORRECTION PROVIDED THAT THE CORRECTION DOES NOT MATERIALLY AFFECT THE CONTRACT. Funniq MAY MAKE IMPROVEMENTS TO THE ONLINE SITES & SERVICES AT ANY TIME.

Funniq, ITS AFFILIATES AND ANY OF THEIR RESPECTIVE OFFICERS, DIRECTORS, EMPLOYEES, OR AGENTS WILL NOT BE LIABLE FOR ANY LOSSES THAT WERE NOT CAUSED BY ANY BREACH OF CONTRACT OR STATUTORY DUTY OR NEGLIGENCE ON THE PART OF Funniq AND AMERCIAN EXPRESS SHALL NOT BE LIABLE FOR ANY LOSSES THAT WERE NOT REASONABLY FORESEEABLE TO BOTH PARTIES WHEN THE CONTRACT WAS FORMED. Funniq SHALL NOT IN ANY EVENT BE LIABLE FOR LOSSES RELATING TO ANY BUSINESS YOU HAVE (INCLUDING WITHOUT LIMITATION LOST PROFITS, COST OF PROCURING SUBSTITUTE SERVICE OR LOST OPPORTUNITY) ARISING OUT OF OR IN CONNECTION WITH THE USE OF THE ONLINE SITES & SERVICES OR A LINKED SITE, OR WITH THE DELAY OR INABILITY TO USE THE ONLINE SITES & SERVICES OR A LINKED SITE. NOTHING IN THESE TERMS SHALL EXCLUDE OR LIMIT Funniq' LIABILITY FOR FRAUD OR DEATH OR PERSONAL INJURY CAUSED BY Funniq' NEGLIGENCE OR ANY OTHER MATTER FOR WHICH IT WOULD BE ILLEGAL OR UNLAWFUL FOR Funniq TO EXCLUDE (OR ATTEMPT TO EXCLUDE) OR LIMIT.
            </p>
            <p>
                If it's not too much trouble be decent and adhere to the standards beneath. On the off chance that you don't your record will 
                be prohibited.
            </p>
            <p>
                Content principles
            </p>
            <p>
                Remember these guidelines while transferring images:
            </p>
            <ol>
                <li>                    
                    No nudes, sex entertainment or explicitly explicity material.
                </li>
                <li>
                    
                    No violence, harassment or bullying.
                </li>
                <li>
                    
                    No hate speech or insults.
                </li>
                <li>
                    
                    No impersonation or deceptive behaviour.
                </li>
                <li>
                   
                    No copyright infringement.
                </li>
                <li>
                    
                    No personal or confidential information.
                </li>
                <li>
                    
                    No reposts of other user's uploads or submissions of repeated content.
                </li>
            </ol>
            <p>
                Comment rules
            </p>
            <p>
                Be respectful with the rest of the community and follow these rules when
                posting comments:
            </p>
            <ol>
                <li>
                   
                    No sexually explicit comments.
                </li>
                <li>
                    
                    No violence, harassment or bullying.
                </li>
                <li>                   
                    No hate speech or insults.
                </li>
                <li>                    
                    No impersonation or deceptive behaviour.
                </li>
                <li>                    
                    No other languages different from the server language.
                </li>
                <li>                    
                    No personal or confidential information.
                </li>
                <li>                    
                    No spam or rehashed remarks.
                </li>
                <li>                    
                    No connections to obscene, hostile, impolite, express or copyright-encroaching locales.
                </li>
            </ol>
            <p>
                Avatar conditions
            </p>
            <p>
                Your symbol picture and status distinguish you at Funniq. If it's not too much trouble follow 
                these guidelines when refreshing them:
            </p>
            <ol>
                <li>                    
                    No nudes, pornography or sexually explicity material.
                </li>
                <li>
                    
                    No violence, harassment or bullying.
                </li>
                <li>
                    
                    No hate speech or insults.
                </li>
                <li>                    
                    No pantomime or tricky conduct. Try not to utilize other client's name, 
                    symbol or status for pantomime purposes.
                </li>
                <li>
                    
                    No copyright infringement.
                </li>
                <li>
                    
                    No personal or confidential information.
                </li>
            </ol>
            <br/>
            <p>
            If you need any help, please dont hesitate to <a href="https://facebook.com/FunniqPage" target='_blank' rel="noopener noreferrer">contact us</a>.
            or email us at <a href="mailto:support@funniq.com">support@funniq.com</a>
            </p>     
            </div>      			            		
		</MDBContainer>   
	);
};

export default PageRules;