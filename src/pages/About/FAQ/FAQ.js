
import React, {useEffect, useState} from 'react';
import { MDBContainer } from "mdbreact";
import './FAQ.sass'
import firebase from './../../../config/firebase'
// import { goToTop } from 'react-scrollable-anchor'
import {getDate} from '../../../functions/getDate'
const ReactMarkdown = require('react-markdown')


const FAQ = () => {
    
    const [stContent, setStContent] = useState({
        content: '',
        title: ''
    })

	useEffect(() => {
        fetchContent();
        window.scrollTo(0, 0)        
    },[])
    

    const fetchContent = () => {
        firebase.firestore().collection('pages').doc('faqs')
        .get()
        .then(doc => {            
            setStContent(doc.data())
        })                                                                                                                                                                         
    }
    
	return (
		<MDBContainer className='mt-3'>
            {
                stContent.content.split("\\n").map(function(item, idx) {
                    return (                        
                        <ReactMarkdown source={item} key={idx}/>                                                    
                     )
                })
            }  
            {/* <button onclick={handleClick}> "asdas" </button>                  */}
            <p>
                If you need any help, please dont hesitate to <a href="https://facebook.com/FunniqPage" target='_blank' rel="noopener noreferrer">contact us</a>,
                or email us at <a href="mailto:support@funniq.com">support@funniq.com</a>
            </p>
            <p>
                Updated on: {getDate.getTheDate(10)}
            </p>
			           			            		
		</MDBContainer>   
	);
};

export default FAQ;
