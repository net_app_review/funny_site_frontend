import React, {useEffect} from 'react';
import { MDBContainer,MDBRow,MDBCol } from "mdbreact";
import {getDate} from '../../../functions/getDate'

const Privacy = () => {

    useEffect(() => {        
        window.scrollTo(0, 0)
    }, []); 

    return (
       <MDBContainer>
           <h2 className="h1-responsive font-weight-bold my-5 text-center">
            Funniq Privacy Policy
            <h5 className='pt-2'>
            Updated on: {getDate.getTheDate(10)}
            </h5>
          </h2>
          
        <MDBRow className="m-0 p-0">
            <MDBCol size="4">
                <strong>
                Basic Account Information:
                </strong>
            </MDBCol>
            <MDBCol size="8">
                If you choose to create a Funniq account, you must provide us with some personal information,
                 such as your name, username, password, email address, or phone number.
                On Funniq, your name and username are always listed publicly,
                 including on your profile page and in search results,
                 and you can use either your real name or a pseudonym. 
                 You can create and manage multiple Funniq accounts.
            </MDBCol>   
        </MDBRow>
        <br></br>
        <MDBRow className="m-0 p-0">
            <MDBCol size="4">
                <strong>
                Contact Information:
                </strong>
            </MDBCol>
            <MDBCol size="8">
                You may use your contact information, such as your email address or phone number,
                 to customize your account or enable certain account features, for example, for login verification.
                  If you provide us with your phone number, you agree to receive text messages to that number from us.
                   We may use your contact information to send you information about our Services, to market to you, to help prevent spam, fraud, or abuse, and to helpothers find your account, including through third-party services and client applications. You may use your settings for email and mobile notifications to control notifications you receive from Funniq. You may also unsubscribe from a notification by following the instructions contained within the notification or the instructions on our website.
            </MDBCol>
        </MDBRow>
        <br></br>
        <MDBRow className="m-0 p-0">
            <MDBCol size="4">
                <strong>
                Funniq, Following, Lists, Profile, and Other Public Information:
                </strong>
            </MDBCol>
            <MDBCol size="8">
            Funniq is primarily designed to help you share information with the world. Most of the information you provide us through Funniq is information you are asking us to make public. You may provide us with profile information such as a short biography, your location, your website, date of birth, or a picture. Additionally, your public information includes the messages you cosplay content; the metadata provided with Funniq, such as when you cosplay contented and the client application you used to cosplay content; information about your account, such as creation time, language, country, and time zone; and the lists you create, people you follow, Funniq you Like or Recosplay content, and Periscope broadcasts you click or otherwise engage with (such as by commenting or hearting) on Funniq. Funniq broadly and instantly disseminates your public information to a wide range of users, customers, and services, including search engines, developers, and publishers that integrate Funniq content into their services, and organizations such as universities, public health agencies, and market research firms that analyze the information for trends and insights. When you share information or content like photos, videos, and links via the Services, you should think carefully about what you are making public.
            </MDBCol>
        </MDBRow>
        <br></br>
        <MDBRow className="m-0 p-0">
            <MDBCol size="4">
                <strong>
                Cookies
                </strong>
            </MDBCol>
            <MDBCol size="8">
            Like many websites, we use cookies and similar technologies to collect additional website usage data and to improve our Services, but we do not require cookies for many parts of our Services such as searching and looking at public user profiles. A cookie is a small data file that is transferred to your computer or mobile device. We may use both session cookies and persistent cookies to better understand how you interact with our Services, to monitor aggregate usage by our users and web traffic routing on our Services, and to customize and improve our Services. Although most web browsers automatically accept cookies, some browsers’ settings can be modified to decline cookies or alert you when a website is attempting to place a cookie on your computer. However, some Services may not function properly if you disable cookies.
            </MDBCol>
        </MDBRow>
        <br></br>
        <MDBRow className="m-0 p-0">
            <MDBCol size="4">
                <strong>
                Advertising:
                </strong>
            </MDBCol>
            <MDBCol size="8">
            Our Services are supported by advertising. We may use the information described in this Privacy Policy to help make our advertising more relevant to you, to measure its effectiveness, and to help recognize your devices to serve you ads on and off of Funniq. We do not use the content you share privately in Direct Messages to serve you ads. Our Funniq Ads Policy also prohibits advertisers from targeting ads based on categories we consider sensitive such as race, religion, politics, sex life, or health. Funniq adheres to the Digital Advertising Alliance Self-Regulatory Principles for Online Behavioral Advertising (also referred to as “interest-based advertising”).
            </MDBCol>
        </MDBRow>
        <br></br>
        <MDBRow className="m-0 p-0">
            <MDBCol size="4">
                <strong>
                Personalizing Across Your Devices:
                </strong>
            </MDBCol>
            <MDBCol size="8">
            Our Services are supported by advertising. We may use the information described in this Privacy Policy to help make our advertising more relevant to you, to measure its effectiveness, and to help recognize your devices to serve you ads on and off of Funniq. We do not use the content you share privately in Direct Messages to serve you ads. Our Funniq Ads Policy also prohibits advertisers from targeting ads based on categories we consider sensitive such as race, religion, politics, sex life, or health. Funniq adheres to the Digital Advertising Alliance Self-Regulatory Principles for Online Behavioral Advertising (also referred to as “interest-based advertising”).
            </MDBCol>
        </MDBRow>
        <br></br>
        <MDBRow className="m-0 p-0">
            <MDBCol size="4">
                <strong>
                User Consent or Direction:
                </strong>
            </MDBCol>
            <MDBCol size="8">
            We may share or disclose your information at your direction, such as when you authorize a third-party web client or application to access your account or when you direct us to share your feedback with a business. When you use Digits by Funniq to sign up for or log in to a third-party application, you are directing us to share your contact information, such as your phone number, with that application. If you’ve shared information, like Direct Messages or protected Funniq, with another user who accesses Funniq through a third-party service, keep in mind that the information may be shared with the third-party service.
            </MDBCol>
        </MDBRow>
        <br></br>
        <MDBRow className="m-0 p-0">
            <MDBCol size="4">
                <strong>
                Law and Harm:
                </strong>
            </MDBCol>
            <MDBCol size="8">
            Notwithstanding anything to the contrary in this Privacy Policy, we may preserve or disclose your information if we believe that it is reasonably necessary to comply with a law, regulation, legal process, or governmental request;to protect the safety of any person; to address fraud, security or technical issues; or to protect our or our users’ rights or property. However, nothing in this Privacy Policy is intended to limit any legal defenses or objections that you may haveto a third party’s, including a government’s, request to disclose your information.
            </MDBCol>
        </MDBRow>
        <br></br>
        <MDBRow className="m-0 p-0">
            <MDBCol size="4">
                <strong>
                Public Information:
                </strong>
            </MDBCol>
            <MDBCol size="8">
            We may share or disclose your public information, such as your public user profile information, public Funniq, or the people you follow or that follow you. Remember: your privacy and visibility settings control whether your Funniq and certain profile information are made public. Other information, like your name and username, is always public on Funniq, unless you delete your account, as described below.            </MDBCol>
        </MDBRow>
        <br></br>
        <MDBRow className="m-0 p-0">
            <MDBCol size="4">
                <strong>
                Non-Personal, Aggregated, or Device-Level Information:
                </strong>
            </MDBCol>
            <MDBCol size="8">
            We may share or disclose non-personal, aggregated, or device-level information such as the total number of times people engaged with a cosplay content, the number of users who clicked on a particular link or voted on a poll in a cosplay content (even if only one did), the characteristics of a device or its user when it is available to receive an ad, the topics that people are cosplay contenting about in a particular location, or aggregated or device-level reports to advertisers about users who saw or clicked on their ads. This information does not include your name, email address, phone number, or Funniq handle. We may, however, share non-personal, aggregated, or device-level information through partnerships with entities that may use data in their possession (including data you may have given them) to link your name, email address, or other personal information to the information we provide them. These partnerships require that they get your consent before doing so.            </MDBCol>
        </MDBRow>
        <br></br>
        <p>
            If you need any help, please dont hesitate to <a href="https://facebook.com/FunniqPage" target='_blank' rel="noopener noreferrer">contact us</a>,
            or email us at <a href="mailto:support@funniq.com">support@funniq.com</a>
        </p>     
       </MDBContainer>       
    );
};


export default Privacy;