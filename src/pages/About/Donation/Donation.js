import React, { useEffect} from 'react';
// import PropTypes from 'prop-types';
import { MDBContainer,MDBRow,MDBCol, MDBView, MDBMask } from "mdbreact";
import './Donation.sass'
// import ScrollableAnchor from 'react-scrollable-anchor'

const Policies = () => {

    // const [stPaypalCollapse, setstPaypalCollapse] = useState(false)

    // const toggleCollapse = collapseID => () => {
    //     setstPaypalCollapse(!stPaypalCollapse);
    // }

    useEffect(() => {      
      window.scrollTo(0, 100)
    }); 

    return (        
        <div className="donate_page">
          <MDBView  src="https://firebasestorage.googleapis.com/v0/b/funniq-78dbd.appspot.com/o/funniq_development%2Fu1-46-1594731300034?alt=media&token=4cfe1cd3-68ad-4223-aaf8-f520d8acc5cd">
            <MDBMask overlay="black-strong" className="flex-center flex-column text-white text-center">
              <h1>
              <strong>
                Help us</strong></h1>
              {/* <h5>When you scroll down it will disappear</h5> */}
              <br />
              <h5>Our works are to bring funs to the community, we hope that with our content, you will feel relaxed and release stresses, </h5>
              <h5>we absolutely appreciate your help for us to keep developing the platform.</h5>
            </MDBMask>
          </MDBView>   
          &nbsp;
          &nbsp;     
          <MDBContainer>
              <MDBRow style={{margin:"0px", padding:"0px"}}>
                <MDBCol sm={6} lg={12} md={6} xl={6} xs={6} style={{"borderLeft":"2px"}} className="text-center">
                    <h1>
                      <strong>
                        Donation
                        </strong>
                    </h1>
                    <h5 className="Donation_paragraph">
                      Funniq is primarily designed to help you share the fun to the world. 
                      We are happy to bring fun and good attitudes to everyone. Your help and donation is absolutely precious to us.
                      Thanks so much.
                    </h5>
                </MDBCol>
                <MDBCol sm={6} lg={12} md={6} xl={6} xs={6} className="text-center">
                    <h1>
                    <strong>Via Streamlab</strong></h1>
                    <a href={`https://streamlabs.com/supportfunniq/`} className="" target="_blank" rel="noopener noreferrer">
                        <img className=" donate_image" src="https://fiverr-res.cloudinary.com/images/t_main1,q_auto,f_auto,q_auto,f_auto/gigs/127997158/original/a9dfde2bc608c350b4e9f76541a47a26be3c4bab/installation-and-setup-of-streamlabs-obs.jpg" alt='_donate_img'>                        
                        </img>

                    </a>
                    
                    {/* <MDBBtn
                        color="primary"
                        onClick={toggleCollapse("basicCollapse")}
                        style={{ marginBottom: "1rem" }}
                        >
                        Paypal
                        </MDBBtn> */}

                    {/* <MDBCollapse id="basicCollapse" isOpen={stPaypalCollapse}>
                    <h5>
                      Funniq is primarily designed to help you share information with the world. 
                      We are happy to bring fun and good attitudes to everyone. Your help and donation is absolutely precious to us.
                      Thanks so much.
                    </h5>
                    </MDBCollapse> */}
                </MDBCol>
              </MDBRow>
          </MDBContainer>
        </div>
    );
};


export default Policies;