import React from 'react';
import PropTypes from 'prop-types';
import './PublicContent.sass'
// import FunZone from "./FunZone/FunZone";
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import CategoryNav from "./CategoryNav/CategoryNav";
import Home from '../Home/Home'

const PublicContent = props => {
    return (
        <MDBContainer className="home">
            <MDBRow className="content">
                <MDBCol sm={0} lg={4} md={4} xl={4} xs={4} className="hidden-xs">
                    <CategoryNav/>
                </MDBCol>
                <MDBCol sm={12} lg={7} md={7} xl={7} xs={7} middle>
                    <Home/>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    );
};

Home.propTypes = {

};

export default PublicContent;
