import React, {useState} from 'react';
// import PropTypes from 'prop-types';
import {MDBRow, MDBBtn, MDBContainer,MDBCol} from "mdbreact";
import axios from 'axios'   
import swal from 'sweetalert'

const PasswordReset = () => {
            
    const [stToken, setstToken] = useState("")
    const [stPassword, setstPassword] = useState("")

    //----------------------------------- handle form actions -----------------------------------//

    // const _handleUsernameChange = (event) => {
    //     event.preventDefault();        
    //     // ststSignUpForm(event.target.value)
    //     setstSignUpForm({
    //       ...stSignUpForm,
    //       username: event.target.value
    //     })
    //   };

    const _handleTokenChange = (event) => {
        event.preventDefault();        
        // ststSignUpForm(event.target.value)
        setstToken(event.target.value)
    }

    const _handlePasswordChange = (event) => {
        event.preventDefault();        
        // ststSignUpForm(event.target.value)
        setstPassword(event.target.value)
    }

    const _handleChangePasswordSubmit = (event) => {
        event.preventDefault();                
        reset_password()
    }

    const reset_password = () => {
        axios.post(`${process.env.REACT_APP_API_URL}/api/v1/password/reset`,{            
            token: stToken,
            password: stPassword
        }).then(response => {            
            if(response.status === 200){
                swal("success", "Your password has been changed, please log in with the new password", "success")                
            }
        }).catch(error=>{            
            if (error.response.status === 404) {
                swal("Something is wrong", "Either your token or password is having some issues, please check again, or email us at funniqpage@gmail.com", "error")                                
            }
        })
    }   

    return (
        <MDBContainer>
            <MDBCol>
                <form onSubmit={_handleChangePasswordSubmit} className="mt-4">
                    <MDBRow style ={{padding: "0px", margin: "0px"}}>
                        <h2>Reset Password</h2>                    
                    </MDBRow>
                    &nbsp;
                    <h4>Your token</h4>
                    <input
                        type="password"
                        id="defaultFormLoginEmailEx"
                        className="form-control"
                        name="token"
                        placeholder="token"
                        value={stToken}
                        onChange={(e) => {_handleTokenChange(e)}}
                        required
                    />
                    &nbsp;
                    <h4>Your new password</h4>
                    <input
                        type="password"
                        id="defaultFormLoginEmailEx"
                        className="form-control"
                        name="password"
                        placeholder="password"
                        value={stPassword}
                        onChange={(e) => {_handlePasswordChange(e)}}
                        minLength={6}
                        required
                    />
                    <MDBBtn color="primary" type="submit">
                        Reset password
                    </MDBBtn>
                </form>    
            </MDBCol>       
        </MDBContainer>
    );
};

PasswordReset.propTypes = {};

export default PasswordReset;
