import React, {useState} from 'react';
// import PropTypes from 'prop-types';
import { MDBContainer, MDBCol} from "mdbreact";
import axios from 'axios'   
import useUserAuthenForm from '../../components/authentication/useUserAuthenForm'
import {Link} from 'react-router-dom'
import './PasswordForgot.sass'
import { MoonLoader } from 'react-spinners';
import FacebookMoonLoaderCss from '../../config/ReactLoader/GoogleMoonLoaderCss'
import swal from 'sweetalert'

const PasswordForgot = () => {
        
    
    const [stWaitMailLoading, setstWaitMailLoading] = useState(false);

    const send_reset_password_token = () => { 
        setstWaitMailLoading(true)        
        axios.post(`${process.env.REACT_APP_API_URL}/api/v1/password/forgot`,{
            email: values.email
        }).then(response => {                  
            if(response.status === 200){      
                swal("success", "Your information has been sent to your email, please click the button 'Go to reset password' to reset your password", "success")                                          
                setstWaitMailLoading(false)             
            }
        }).catch(error=>{
            swal("Errors", "Something is wrong, please contact us on our Facebook page https://facebook.com/FunniqPage or send email to support@funniq.com", "error")            
            setstWaitMailLoading(false)
        })
        
    }

    const { values, handleChange, handleSubmit } = useUserAuthenForm(send_reset_password_token);

    return (
        <MDBContainer className="password_forgot">
            <MDBCol >
                <form onSubmit={handleSubmit} className="mt-4">                    
                    <h2>
                        Forgot Password
                    </h2>                                        
                    <h4>Your email</h4>                    
                    <input
                        type="email"
                        id="defaultFormLoginEmailEx"
                        className="form-control"
                        name="email"
                        placeholder="example@gmail.com"
                        value={values.email}
                        onChange={handleChange}
                        required
                    />
                    <button className="btn btn-success send_reset_token_btn"  type="submit">
                        Send reset information
                        &nbsp;
                        <MoonLoader          
                            css={FacebookMoonLoaderCss}            
                            size={18}
                            color={'#fafafa'}
                            loading={stWaitMailLoading}                        
                        />   
                    </button>
                    <Link to={`/password_reset`} style={{ textDecoration: 'none' }}>
                        <button className="btn btn-primary m-0" type="submit">
                            Go to reset password
                        </button>
                    </Link>
                </form> 
                
            </MDBCol>         
        </MDBContainer>
    );
};

PasswordForgot.propTypes = {};

export default PasswordForgot;
