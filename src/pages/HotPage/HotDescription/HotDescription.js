import React  from 'react';
// import PropTypes from 'prop-types';
import {MDBCol, MDBContainer, MDBRow} from "mdbreact";

const SCROLL_OFFSET = 50

const HotDescription = props => {
    
    return (
        <MDBContainer>
            <MDBRow >
                <MDBCol size="2" className="pr-0">
                    <img className="img-fluid rounded" src="https://media.giphy.com/media/h2HTvVuKVS4daQaVuS/giphy.gif" alt=""/>
                </MDBCol>
                <MDBCol size="10" >
                    <MDBRow style={{margin: '5px' }}> 
                        <h3>
                            Hot
                        </h3>
                    </MDBRow>
                    <MDBRow  style={{margin: '5px' }}>
                        <p>
                            Hottest stuff here
                        </p>
                    </MDBRow>
                </MDBCol>                
            </MDBRow> 
        </MDBContainer>
    );
};


HotDescription.propTypes = {};

export default HotDescription;