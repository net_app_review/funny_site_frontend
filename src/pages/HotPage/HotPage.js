import React, {useEffect} from 'react';

import './HotPage.sass'
import HotArticles from "./HotArticles/HotArticles";
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import HotDescription from './HotDescription/HotDescription'
import LeftStickyNav from '../Shared/LeftStickyNav/LeftStickyNav'
import RightStickyNav from '../Shared/RightStickyNav/RightStickyNav'

const HotPage = props => {

    useEffect(() => {
        window.scrollTo(0, 0)
    }, []);    


    return (
        <MDBContainer className="home">            
            <MDBRow className="content">
                <MDBCol sm={3} lg={3} md={3} xl={3} xs={4} className="hidden-xs pl-0">
                    <LeftStickyNav/>                    
                </MDBCol>
                <MDBCol sm={6} lg={6} md={6} xl={6} xs={7} middle >
                    <HotDescription category_id = {props.match.params.id}/>
                    &nbsp;
                    <HotArticles key = {props.match.params.id}
                                      category_id = {props.match.params.id}/>
                </MDBCol>
                <MDBCol sm={3} lg={3} md={3} xl={3} xs={4} className="hidden-xs pl-0 pr-0">
                    <RightStickyNav/>                    
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    );
};

HotPage.propTypes = {

};

export default HotPage;
