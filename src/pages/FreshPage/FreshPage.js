import React, {useEffect} from 'react';

import './FreshPage.sass'
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import FreshDescription from './FreshDescription/FreshDescription'
import LeftStickyNav from '../Shared/LeftStickyNav/LeftStickyNav'
import RightStickyNav from '../Shared/RightStickyNav/RightStickyNav'
import FreshPageArticles from './FreshArticles/FreshArticles'

const FreshPage = props => {

    useEffect(() => {
        window.scrollTo(0, 0)
    }, []);    

    return (
        <MDBContainer className="home">            
            <MDBRow className="content">
                <MDBCol sm={3} lg={3} md={3} xl={3} xs={4} className="hidden-xs pl-0">
                    <LeftStickyNav/>                    
                </MDBCol>
                <MDBCol sm={6} lg={6} md={6} xl={6} xs={7} middle>
                    <FreshDescription category_id = {props.match.params.id}/>
                    <FreshPageArticles key = {props.match.params.id}
                                      category_id = {props.match.params.id}/>
                </MDBCol>
                <MDBCol sm={3} lg={3} md={3} xl={3} xs={4} className="hidden-xs pl-0 pr-0">
                    <RightStickyNav/>                    
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    );
};

FreshPage.propTypes = {

};

export default FreshPage;
