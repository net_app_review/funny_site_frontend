import React from 'react';
// import PropTypes from 'prop-types';
import {MDBCol, MDBContainer, MDBRow} from "mdbreact";

const FreshDescription = props => {
    
    return (
        <MDBContainer>
            <MDBRow >
                <MDBCol size="2" className="pr-0">
                    <img className="img-fluid rounded" src="https://firebasestorage.googleapis.com/v0/b/funniq-78dbd.appspot.com/o/funniq_internal%2Ffresh.gif?alt=media&token=d60dc1b0-9aed-42ea-894d-8409d450ec0d" alt="blank_user_icon"/>
                </MDBCol>
                <MDBCol size="10" >
                    <MDBRow style={{margin: '5px' }}> 
                        <h3>
                            Fresh
                        </h3>
                    </MDBRow>
                    <MDBRow  style={{margin: '5px' }}>
                        <p>
                            Off-the-shelf stuff here
                        </p>
                    </MDBRow>
                </MDBCol>                
            </MDBRow> 
        </MDBContainer>
    );
};


FreshDescription.propTypes = {};

export default FreshDescription;