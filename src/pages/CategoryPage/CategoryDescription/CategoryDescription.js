import React, {useEffect, useState} from 'react';
import axios from 'axios'
import {MDBCol, MDBContainer, MDBRow} from "mdbreact";
import "./CategoryDescription.module.sass"
import FsLightbox from 'fslightbox-react';
import LazyLoad from 'react-lazyload';
import swal from 'sweetalert'

const CategoryDescription = props => {

    const [stCategoryInfo, setstCategoryInfo] = useState({});    
    const [stImgLightBox, setstImgLightBox] = useState(false);

    useEffect(() => {	        
        fetchCategoryInfo()		        
	},[props.category_id])    

	const fetchCategoryInfo = () => {
		axios.get(`${process.env.REACT_APP_API_URL}/api/v1/categories/${props.category_id}`)
			.then(response => {								  
				setstCategoryInfo(response.data)
			})
			.catch(error => swal("Error", `${error}, please contact us at https://facebook.com/FunniqPage`, "error"))
    }
    
    const categoryImage = () =>  {
        if (stCategoryInfo.image) {
            return stCategoryInfo.image.url
        }
        return null
    }    

    return (        
        <MDBContainer className="box pr-0 pl-0">
            <div className="d-flex flex-row" >
                <MDBCol size="2"  className="p-0" >
                    <FsLightbox toggler={stImgLightBox}
                                customSources={[
                                    <img className="image img-fluid rounded"
                                        alt=""
                                        src={categoryImage()}
                                        onClick={ () => setstImgLightBox(!stImgLightBox) }/>
                                    ]} 
                                    customSourcesGlobalMaxDimensions={ 
                                        { width: 600, height: 700 } 
                                    } 
                                    customSourcesMaxDimensions={ 
                                        [null, null, { width: 300, height: 50 }] 
                                    }
                                />
                        <LazyLoad height={"100%"} 
                        offset={300} 
                        placeholder={<img src="https://firebasestorage.googleapis.com/v0/b/funniq-78dbd.appspot.com/o/MeekAdorableIaerismetalmark-size_restricted%5B1%5D.gif?alt=media&token=9f30e23c-282d-44ea-a3f2-e110ac11df9d"
                        className="image img-fluid" alt="placeholder"></img>}>
                            <img className="card-img-64 img-fluid rounded" 
                                src={categoryImage()} alt="" 
                                onClick={ () => setstImgLightBox(!stImgLightBox)} />
                        </LazyLoad>                        
                </MDBCol>
                <MDBCol size="10" className="category-info pl-1 pr-1">
                    <MDBRow className="name" id="row-name" style={{margin: '5px' }}> 
                        <h3>
                            {stCategoryInfo.name}
                        </h3>
                    </MDBRow>
                    <MDBRow className="desc" style={{margin: '5px' }}>
                        <p>
                            {stCategoryInfo.description}
                        </p>
                    </MDBRow>
                </MDBCol>                
            </div> 
        </MDBContainer>
    );
};


CategoryDescription.propTypes = {};

export default CategoryDescription;