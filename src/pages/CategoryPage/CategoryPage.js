import React, {useEffect, useState} from 'react';
// import PropTypes from 'prop-types';
import './CategoryPage.sass'
import CategoryArticles from "./CategoryArticles/CategoryArticles";
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import CategoryDescription from './CategoryDescription/CategoryDescription'
import LeftStickyNav from '../Shared/LeftStickyNav/LeftStickyNav'
import RightStickyNav from '../Shared/RightStickyNav/RightStickyNav'
import SEO from "../../services/seoService";

const CategoryPage = props => {

    const [stSeo, setstSeo] = useState({})

    useEffect(() => {
        window.scrollTo(0, 0)
        standardizeTitle()
        // setstSeo({ title: response.data.header})
					// image:"https://picsum.photos/id/52/1200/600", 
					// url:"yetanotherawesomeapp.com" })
    }, []);    

    const standardizeTitle = () => {
        let final_string = props.location.pathname.replace("/categories/", "")
        final_string = final_string.replace("-", "")
        final_string = final_string.replace(/[0-9]/g, '')        
        setstSeo({ title: final_string})
	}

    return (
        <MDBContainer className="home"> 
            <SEO pageProps={stSeo} />           
            <MDBRow className="content">
                <MDBCol sm={3} lg={3} md={3} xl={3} xs={4} className="hidden-xs pl-0">
                    <LeftStickyNav/>                    
                </MDBCol>
                <MDBCol sm={6} lg={6} md={6} xl={6} xs={7} middle>
                    <CategoryDescription 
                                        category_id = {props.match.params.id}/>
                    <CategoryArticles key = {props.match.params.id}
                                      category_id = {props.match.params.id}/>
                </MDBCol>
                <MDBCol sm={3} lg={3} md={3} xl={3} xs={4} className="hidden-xs pl-0 pr-0">
                    <RightStickyNav/>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    );
};

CategoryPage.propTypes = {

};

export default CategoryPage;
