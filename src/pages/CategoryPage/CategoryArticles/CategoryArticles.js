import React, {useEffect, useState} from 'react';
// import PropTypes from 'prop-types';
import './PersonalArticles.sass'
// import {articlesService} from "../../../services/articlesService";
import Fun from "../../Shared/Fun/Fun"
import ScrollImageLoader from "../../Shared/ScrollImageLoader";
// import {LoggedInContext} from '../../../Store'
import {categoryPageService} from "../../../services/categoryPageService"
import {removeHidingArticles} from "../../../functions/removeHidingArticles"

const SCROLL_OFFSET = 50

const CategoryArticles = props => {

    const [stPage, setStPage] = useState(1);
    const [stArticles, setStArticles] = useState([]);    
    const [stIsBottom, setStIsBottom] = useState(false);
    // const [LoggedIn, setLoggedIn] = useContext(LoggedInContext);    

    useEffect(() => {        
        categoryPageService.getArticles(props.category_id,stPage)
            .then(result => {                
                // setStArticles([...stArticles, ...result.data])
                setStArticles([...stArticles, ...removeHidingArticles.removeHidedArticle(result.data,'hide')])
            })
    }, [stPage]);


    useEffect(() => {        
        const onScroll = () => {
            if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - SCROLL_OFFSET) {
                if (!stIsBottom) {
                    setStIsBottom(true)                                        
                    setStPage(stPage+1)                    
                }
            } else {
                setStIsBottom(false)
            }
        }

        window.addEventListener('scroll', onScroll)

        return () => {
            window.removeEventListener('scroll', onScroll)
        };
    }, [stIsBottom, stPage])
    

    return (
        <div className="fun-zone">

            {stArticles.length > 0 && stArticles.map(article => {
                return (
                    <Fun key={article.id} article={article}/>
                )
            })}
            {stArticles.length >=5  && <ScrollImageLoader/>}

        </div>
    );
};


CategoryArticles.propTypes = {};

export default CategoryArticles;