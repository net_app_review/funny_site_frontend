import React, {useContext, useEffect, useState} from 'react'
import {MDBBtn, MDBContainer, MDBRow} from 'mdbreact'
// import {SignInModalContext} from '../../Store'
import useUserAuthenForm from '../../../components/authentication/useUserAuthenForm'

import {confirmAlert} from 'react-confirm-alert'; // Import alert
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import alert css
import apiClient from "../../../config/apiClient";
import {LoggedInContext, CurrentUserContext} from '../../../Store'
import swal from 'sweetalert';
import { useTranslation } from "react-i18next";

const SettingAccount = () => {

  const { values, handleChange, handleSubmit } = useUserAuthenForm(update_user_info);
  const [stUserInfo, setstUserInfo] = useState();
  const [setLoggedIn] = useContext(LoggedInContext)
  const [setstCurrentUser] = useContext(CurrentUserContext)
  const { t, i18n } = useTranslation();

  useEffect(() => {		
    fetchUserInfo()  
  },[])

	const fetchUserInfo = () => {
		apiClient.get(`${process.env.REACT_APP_API_URL}/api/v1/users/current_user`,)
			.then(response => {        
        if (response.status === 200){          
          setstUserInfo(response.data.current_user.name)          
        }								
			})
			.catch(error => swal("Error", `${error}, please contact us at https://facebook.com/FunniqPage`, "error"))
	}

function update_user_info  () {

  let data = {
      user_info: {
          name: values.username
        }
  }

  apiClient.post(`${process.env.REACT_APP_API_URL}/api/v1/users/update`, data
  ).then(response => {    
    if (response.status === 200){
      swal("Successful!", "Your account name has been changed successfully", "success");            
    }
  })
  .catch(error => {    
    if (error.response.data.messages === 'Reached_limits'){
      swal("Limit reached", `You have reached the limit, you can updated after 1 month, If you want to update now please contact us at https://facebook.com/FunniqPage`, "error")    
    } else {
      swal("Error", `${error}, please contact us at https://facebook.com/FunniqPage`, "error")    
    }    
  })

}

const showDisableAccountConfirmation = () => {
  confirmAlert({
      title: 'Disable your account',
      message: 'This will disable your account, are you sure to proceed?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
              disable_Account()
          }
        },
        {
          label: 'No',
          onClick: () => {}
        }
      ]
  });
  }

  const disable_Account = () => {
    apiClient.post(`${process.env.REACT_APP_API_URL}/api/v1/users/update`,{
      user_info: {
        status: "inactive"
      }
    }).then(response => {
      if (response.status === 200){
        apiClient.delete(`${process.env.REACT_APP_API_URL}/api/v1/log_out`,)
        .then(response => {            
            setLoggedIn("NOT_LOGGED_IN")
            setstCurrentUser({})                        
        }).catch(error => {          
          swal("Error", `${error}, please contact us at https://facebook.com/FunniqPage`, "error")
        }) 
      }
    })
    .catch(error => {      
      swal("Error", `${error}, please contact us at https://facebook.com/FunniqPage`, "error")
    })
  }

    return (
      
      <MDBContainer>              
        
          <MDBRow style={{margin: '0px', padding: '0px' }}>            
            <form onSubmit={handleSubmit}>
              <label
                htmlFor="defaultFormRegisterNameEx"
                className="black-text"
              >
                {t("userSetting.username")}
              </label>
              <input
                type="text"
                id="defaultFormLoginEmailEx"
                className="form-control"
                name="username"
                placeholder={stUserInfo}
                value={values.username}
                onChange={handleChange}
                key={stUserInfo}
                defaultValue={stUserInfo}
                required
              />                    
              <MDBBtn color="primary" type="submit">
              {t("userSetting.updateAccount")}
              </MDBBtn>
              <MDBBtn color="primary" onClick={showDisableAccountConfirmation}>
              {t("userSetting.disable")}
              </MDBBtn>
            </form>              
          </MDBRow>                                 
    </MDBContainer>
    );

}

export default SettingAccount
