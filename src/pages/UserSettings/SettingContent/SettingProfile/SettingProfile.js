import React, {useContext, useState,useEffect, useRef} from 'react'
import {MDBBtn, MDBContainer} from 'mdbreact'
// import {SignInModalContext} from '../../Store'
import "react-datepicker/dist/react-datepicker.css";
import {CountryDropdown} from 'react-country-region-selector';
import "./SettingProfile.module.sass"
import apiClient from "../../../../config/apiClient";
import {CurrentUserContext, LoggedInContext} from '../../../../Store'
import UserAvatar from './UserAvatar/UserAvatar';
import { Redirect } from 'react-router-dom'
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import swal from 'sweetalert';
import { useTranslation } from "react-i18next";
const SettingProfile = (props) => {  


  // const [stBirthdayPicker, setstBirthdayPicker] = useState();
  const [stCountryPicker, setstCountryPicker] = useState();
  // const [stPicture, setstPicture] = useState([]);  
  // const [stCurrentUser, setstCurrentUser] = useState({})
  const [stUserDescription, setstUserDescription] = useState("")
  const [stUserGender, setstUserGender] = useState("")
  const [stUserprofile, setstUserprofile] = useState({});  
  const [stCurrentUser, setstCurrentUser] = useContext(CurrentUserContext)
  const[LoggedIn,] = useContext(LoggedInContext)
  const [stRedirect, setstRedirect] = useState(false);
  const [stBirthDay, setstBirthDay] = useState("");  
  const inputRef = useRef(null);
  const { t, i18n } = useTranslation();
  

  const _handleBirthDayChange = (day) => {
    setstBirthDay(day)
    // {new Intl.DateTimeFormat("en-GB", {
    //   year: "numeric",
    //   month: "long",
    //   day: "2-digit"
    // }).format(customer.firstSale)}    
  }

  useEffect(() => {		
    fetchUserInfo()      
    },[LoggedIn])

  useEffect(() => {		
    if(stCurrentUser.userprofile){      
      setstUserprofile(stCurrentUser.userprofile)
    }      
  },[stCurrentUser])

  const renderRedirect = () => {
    if (stRedirect) {
      return <Redirect to='/' />
    }
  }

  const fetchUserInfo = () => {
    apiClient.get(`${process.env.REACT_APP_API_URL}/api/v1/users/current_user`,)
      .then(response => {                 
        if (response.status === 200){                    
          setstCurrentUser(response.data.current_user)
          setstUserDescription(response.data.current_user.description)
          setstUserGender(response.data.current_user.gender)
          setstBirthDay(response.data.current_user.birthday)


        } else if (response.status === 204) {
          setstRedirect(true)
        }
      })
      .catch(error => swal("Error", `${error}, please contact us at https://facebook.com/FunniqPage`, "error"))
  }


  const selectCountry = (val) => {
    setstCountryPicker(val);
  }


  // function handleBirthdayPicker (date) {
  //   setstBirthdayPicker(date)
  // }

  
  // const fetchUserDescription = () => {
  //   if (stCurrentUser.userprofile) {
  //     return stCurrentUser.userprofile.description
  //   }
  //   return null
  // }

const update_user_profile = (e) => {
  e.preventDefault();   
  let data = {
    userprofile: {      
      gender: stUserGender,
      birthday: stBirthDay,
      country: stCountryPicker,
      description: stUserDescription
    }
  }
  apiClient.post(`${process.env.REACT_APP_API_URL}/api/v1/userprofiles/update`, data
  ).then(response => {        
    if (response.status === 200){      
      fetchUserInfo()
      swal("Successful!", "Your information has been updated", "success");            
    }
  })
  .catch(error => {
    if (error.response.status === 400 ){
      swal("Error!", "There is nothing changed in your profile, please check again", "error");            
    } else {
      swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
    }
  })

}

const userCountry = () => {
  if (stCurrentUser.userprofile) {
    return stCurrentUser.userprofile.country
  }
  return null
}

const _handleUserDescriptionChange = (event) => {
  event.preventDefault();        
  setstUserDescription(event.target.value)            
};

// const handleOnKeyDown=(event)=> {    
//   if (event.keyCode === 13) {
//       event.preventDefault();                
//       props.history.push(`/search/${stSearch}`)
//       setstSearch("")            
//   }
// }

const _handleUserGenderChange = (event) => {
  event.preventDefault();        
  setstUserGender(event.target.value)     
}

    return (
      
      <MDBContainer onSubmit={update_user_profile} className="setting_profile">
        {renderRedirect()}
        <div className="d-flex flex-row flex-wrap">
          <UserAvatar stCurrentUser={stCurrentUser}/>        
        <div className="d-flex flex-column  ml-lg-4 flex-fill ">                        
          
          
          <form >            
            <div className="mb-0">
              <h5 htmlFor="defaultFormRegisterNameEx" className="black-text" >
              {t("userSetting.country")}
              </h5>
            </div>
            <div className="mb-3">
            <CountryDropdown
                value={stCountryPicker}
                onChange={(val) => selectCountry(val)} 
                key={userCountry}
                defaultOptionLabel={stUserprofile.country}
                style={{
                  backgroundColor: 'white',
                  color: 'green',
                  fontSize: 20,
                  width: '10em'
                }}
                />
            </div>      

              <div className="mb-0">
                <h5 htmlFor="defaultFormRegisterNameEx" className="black-text" >
                {t("userSetting.gender")}
                </h5>
              </div>

              <div className="mb-4">
                <input type="text" id="exampleDisabled" className="form-control" color="green" defaultValue={stUserprofile.gender} disabled/>
                <select className="browser-default custom-select"
                        name="gender"
                        value={stUserGender}
                        onChange={(e) => _handleUserGenderChange(e)}                        
                        >
                  <option>{t("userSetting.chooseGender")}</option>
                  <option value="unspecified">{t("userSetting.unspecified")}</option>
                  <option value="male">{t("userSetting.male")}</option>
                  <option value="female">{t("userSetting.female")}</option>
                </select>
              </div>          
                      
              <h5 htmlFor="defaultFormRegisterNameEx" className="black-text" >
              {t("userSetting.description")}
              </h5>
              <div>

              </div>
              <textarea
                type="text"
                id="defaultFormLoginEmailEx"
                className="form-control"
                name="description"
                key={stUserprofile.description}
                placeholder={stUserprofile.description}
                value={stUserDescription}
                ref={inputRef}
                onChange={(e) => _handleUserDescriptionChange(e)}                
                maxLength="120"
              >
                {stUserprofile !== {} ? stUserprofile.description : ""}
              </textarea>
              <h5 className="black-text mt-2" >
              {t("userSetting.date")}
              </h5> 
              <div>
                {stUserprofile.birthday}
                &nbsp;
                &nbsp;
                <DayPickerInput
                    onDayChange={_handleBirthDayChange}
                    dayPickerProps={{
                      month: new Date(2018, 10),
                      showWeekNumbers: true,
                      todayButton: 'Today',
                    }}
                  />
                  
              </div>
              
          
            <MDBBtn color="dark-green" type="submit" className="ml-0" >
            {t("userSetting.update")}
            </MDBBtn>            
          </form>
          </div>     
      </div>
    </MDBContainer>
    );

}

export default SettingProfile
                                                                                                                                                                                                                                                                                                                                