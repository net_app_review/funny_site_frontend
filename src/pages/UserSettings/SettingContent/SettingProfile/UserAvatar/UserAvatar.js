import React, {useState} from 'react'
import {MDBIcon} from 'mdbreact'
import styles from './UserAvatar.module.sass'
import ModalUserAvatar from '../../../../../components/ModalUserAvatar/ModalUserAvatar'

const UserAvatar = (props) => {  
  
  // const [stCurrentUser, setstCurrentUser] = useState({})
  const [stimagePreviewUrl, ] = useState();
  const [stIsOpenAvatarModal, setstIsOpenAvatarModal] = useState(false);

  const ToggleAvatarModal = () => {
    setstIsOpenAvatarModal(!stIsOpenAvatarModal)
  }

  const renderAvatar= () => {
    if (stimagePreviewUrl){
      return <img src={stimagePreviewUrl} className="img-fluid" alt="aligment"/>
    } else {
      return <img src={avatar()} className="img-fluid" alt="aligment" />            
    }
  }

  const avatar = () => {
    if (props.stCurrentUser.image) {
      return props.stCurrentUser.image.url
    }
    return null
  }
  
  
  
    return (
        <div  className={`${styles.UserAvatar} d-flex flex-column  mb-lg-4`}>            
            <ModalUserAvatar isOpen={stIsOpenAvatarModal}
                           toggle={ToggleAvatarModal}/>
            
            <div className={`${styles.avatar_component} d-flex justify-content-center`}>
              <div className={styles.Avatar}>
                {
                  renderAvatar()                    
                }   
                <button className={`btn ${styles.change_avatar_button}`} onClick={ToggleAvatarModal}>
                  <MDBIcon icon="camera-retro" />
                </button> 
              </div>              
            </div>        
        </div>
    )  
}

export default UserAvatar
 