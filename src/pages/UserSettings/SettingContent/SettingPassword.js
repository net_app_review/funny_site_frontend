import React, {useState, useContext} from 'react'
import axios from 'axios'
import {MDBBtn, MDBContainer} from 'mdbreact'
// import {SignInModalContext} from '../../Store'
import useUserAuthenForm from '../../../components/authentication/useUserAuthenForm'
// import {store} from 'react-notifications-component';
import {CurrentUserContext} from '../../../Store'
import swal from 'sweetalert';
import { useTranslation } from "react-i18next";

const SettingPassword = () => {

  // const { values, handleChange, handleSubmit } = useUserAuthenForm(update_user_password);  
  // const [stUserInfo, setstUserInfo] = useState({});  
  const [stCurrentUser] = useContext(CurrentUserContext)
  const [stFormPassword, setstFormPassword] = useState({
    old_password: '',
    new_password: '',
    new_password_confirmation: ''
  });
  const { t, i18n } = useTranslation();

const update_user_password = () => {

  let header = {
    headers: {
        'X-User-Token': stCurrentUser.authentication_token
    }                            
  }

  let data = {}

  if (stCurrentUser.password_init === true) {
    data = {
        old_password: stFormPassword.old_password,
        user_info: {
            password: stFormPassword.new_password,
            password_confirmation: stFormPassword.new_password_confirmation
        }
    }
  } else if (stCurrentUser.password_init === false){
    data = {     
        user_info: {
            password: stFormPassword.new_password,
            password_confirmation: stFormPassword.new_password_confirmation
        }
    }
  }  
  

  axios.post(`${process.env.REACT_APP_API_URL}/api/v1/users/update_password`, data, header
  ).then(response => {    
    if (response.status === 200){
      swal("Successful!", "Your password has been changed successfully", "success");      
      cleanStatePassword()

    }
  })
  .catch(error => {    
    if(error.response.status === 422) {
      swal("Error!", "Something is wrong, either your password is having fewer than 6 characters, or your password confirmation doesnt match", "error");      
    } else if (error.response.status === 406) {
      swal("Wrong old password!", "Your old password does not match, please try again", "error");      
      // cleanStatePassword()
    } else {
      swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
    }
  }) 
  
  

}

// ------------------------------------------ handle password form actions -------------------------------------- //

const cleanStatePassword = () => {

  setstFormPassword ({old_password: "",
                      new_password: "",
                      new_password_confirmation: ""
                    })

  // setstFormPassword ({new_password: ""})

  // setstFormPassword ({new_password_confirmation: ""})

  // setstFormPassword({
  //   ...stFormPassword,
  //   "old_password": ""})
  
  // setstFormPassword({
  //   ...stFormPassword,
  //   new_password: ""})   

  // setstFormPassword({
  //   ...stFormPassword,
  //   new_password_confirmation: ""})   
}

const _onSubmitResetPassword = (e) => {
  e.preventDefault()
  update_user_password()
}

const _handleInputChange = (event, state ) => {
  event.preventDefault();
  // setValues(values => ({ ...values, [event.target.name]: event.target.value }));      

  setstFormPassword({
    ...stFormPassword,
    [state]: event.target.value})   
}


    return (
      
      <MDBContainer>
          {
              stCurrentUser.password_init === false
              ? 
              <form onSubmit={_onSubmitResetPassword} className="mt-2">
                <p className="h4 text-center mb-4">{t("userSetting.changePass")}</p>
                <div className="social-buttons-group">
                    
                </div>             
                <br />            
                <br />
            <label htmlFor="defaultFormLoginPasswordEx" className="grey-text">
            {t("userSetting.newPassword")}
            </label>            
            <input
              type="password"
              id="defaultFormLoginPasswordEx"
              className="form-control"

              name="password"    
              placeholder={t("userSetting.password")}          
              value={stFormPassword.new_password}
              onChange={(e) => _handleInputChange(e, "new_password")}
              required
            />
            <br/>
            <label htmlFor="defaultFormLoginPasswordEx" className="grey-text">
            {t("userSetting.passConfirm")}
            </label>            
            <input
              type="password"
              id="defaultFormLoginPasswordEx"
              className="form-control"

              name="password_confirmation"    
              placeholder={t("userSetting.confirmPass")}            
              value={stFormPassword.new_password_confirmation}
              onChange={(e) => _handleInputChange(e, "new_password_confirmation")}
              required
            />
            <br />
            <div className="text-center mt-4">
              {/* <MDBBtn color="indigo" type="submit">Sign Up</MDBBtn> */}
              <MDBBtn type="submit" color="default">{t("userSetting.changePass")}</MDBBtn>              
            </div>           
          </form>:
            <form onSubmit={_onSubmitResetPassword} className="mt-4">
                <p className="h4 text-center mb-4">{t("userSetting.changePass")}</p>
            <div className="social-buttons-group">
                
            </div>             
            <br />            
            <br />
            <label htmlFor="defaultFormLoginPasswordEx" className="grey-text">
            {t("userSetting.oldPass")}
            </label>            
            <input
            type="password"
            id="defaultFormLoginPasswordEx"
            className="form-control"

            name="old_password"    
            placeholder="Password"            
            value={stFormPassword.old_password}
            onChange={(e) => _handleInputChange(e, "old_password")}
            required
            />
            <br/>
            <label htmlFor="defaultFormLoginPasswordEx" className="grey-text">
            {t("userSetting.newPass")}
            </label>            
            <input
            type="password"
            id="defaultFormLoginPasswordEx"
            className="form-control"

            name="password"    
            placeholder="Password"            
            value={stFormPassword.new_password}
            onChange={(e) => _handleInputChange(e, "new_password")}
            required
            />
            <br/>
            <label htmlFor="defaultFormLoginPasswordEx" className="grey-text">
            {t("userSetting.confirmPass")}
            </label>            
            <input
              type="password"
              id="defaultFormLoginPasswordEx"
              className="form-control"

              name="password_confirmation"    
              placeholder="password confirmation"
              value={stFormPassword.new_password_confirmation}            
              onChange={(e) => _handleInputChange(e, "new_password_confirmation")}              
              required
            />
            <br />
            <div className="text-center mt-4">
            {/* <MDBBtn color="indigo" type="submit">Sign Up</MDBBtn> */}
            <MDBBtn type="submit" color="primary">{t("userSetting.changePass")}</MDBBtn>              
            </div>           
        </form>
        }
      
    </MDBContainer>
    );

}

export default SettingPassword
