import React, {useState} from 'react';
// App.js
import {MDBCol, MDBContainer} from 'mdbreact'
import Tab from 'react-bootstrap/Tab'
import Tabs from 'react-bootstrap/Tabs'
import SettingProfile from './SettingContent/SettingProfile/SettingProfile'
import SettingAccount from './SettingContent/SettingAccount'
import SettingPassword from './SettingContent/SettingPassword'
// import {LoggedInContext} from '../../Store'
import './UserSettings.sass'
// import { Redirect } from 'react-router'
import { useTranslation } from "react-i18next";



const UserSettings = props => {

    const [key, setKey] = useState('profile');
    const { t } = useTranslation();
    // const [LoggedIn, setLoggedIn] = useContext(LoggedInContext)
      
    // const redirect = () => {
    //     if (LoggedIn === "NOT_LOGGED_IN") return <Redirect to="/"/>
    // }

    return (
        <MDBContainer className="user_settings">
            {/* {redirect()} */}
            &nbsp;
            &nbsp;
            &nbsp;
            <div className="d-flex justify-content-center">
                <MDBCol size ="10">
                    <Tabs id="controlled-tab-example" activeKey={key} onSelect={k => setKey(k)}>
                        <Tab eventKey="profile" title={t("userSetting.profile")}>
                            &nbsp;
                            &nbsp;
                            <SettingProfile/>
                        </Tab>
                        <Tab eventKey="account" title={t("userSetting.account")}>
                            &nbsp;
                            &nbsp;
                            <SettingAccount/>
                        </Tab>
                        <Tab eventKey="security" title={t("userSetting.security")}>
                            &nbsp;
                            &nbsp;
                            <SettingPassword/>
                        </Tab>
                    </Tabs>    
                </MDBCol>
            </div>
                    
        </MDBContainer>
    );
};

UserSettings.propTypes = {

};

export default UserSettings;
