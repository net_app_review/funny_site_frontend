import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import { MDBNav, MDBNavItem, MDBNavLink, MDBContainer, MDBListGroup, MDBListGroupItem } from "mdbreact";
import {Link} from 'react-router-dom'

const SettingList = props => {

    return (
        <MDBContainer>
            <MDBListGroup style={{ width: "22rem" }}>
                <Link to={`/settings/profile`} style={{ textDecoration: 'none' }}>
                    <MDBListGroupItem>Profile</MDBListGroupItem>
                </Link>
                <Link to={`/settings/account`} style={{ textDecoration: 'none' }}>
                    <MDBListGroupItem>Account</MDBListGroupItem>
                </Link>
                <Link to={`/settings/security`} style={{ textDecoration: 'none' }}>
                    <MDBListGroupItem>Security</MDBListGroupItem>            
                </Link>            
            </MDBListGroup>
        </MDBContainer>
    );
};

export default SettingList;