import React from 'react';
import PropTypes from 'prop-types';
import './DetailArticle.sass'
import Fun from "../../Shared/Fun/Fun"
// import {LoggedInContext} from '../../../Store'

const DetailArticle = props => {

    // const [LoggedIn, setLoggedIn] = useContext(LoggedInContext);

    const renderArticle = (comment) => {
        if (props.article.id) {
            return <Fun 
                    key={props.article.id}
                    article={props.article}/>
        }
        return null
    }

    return (
        <div className="fun-zone">
            {
                renderArticle()
            }
        </div>
    );
};


DetailArticle.propTypes = {
    article: PropTypes.any
};

export default DetailArticle;