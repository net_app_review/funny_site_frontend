import React, { useState, useEffect } from 'react'

// import Trending from './Trending'
import { MDBContainer, MDBRow, MDBCol } from "mdbreact"

import CommentForm from '../../components/Comments/CommentForm'
import CommentList from '../../components/Comments/CommentList'
import LeftStickyNav from '../Shared/LeftStickyNav/LeftStickyNav'
import RightStickyNav from '../Shared/RightStickyNav/RightStickyNav'
import DetailArticle from './DetailArticle/DetailArticle'
import apiClient from "../../config/apiClient";
import SEO from "../../services/seoService";
import {fetchCommentService} from "../../services/fetchCommentService"
import LoadMoreButton from '../../components/LoadMoreButton/LoadMoreButton'
import swal from 'sweetalert'
import './Details.sass'

export const CommentsContext = React.createContext();

const Detail = (props) => {
	const [stArticles, setstArticles] = useState({})
	const [ , setCategory] = useState({})
	const [ , setUser] = useState({})
	const [Comments, setComments] = useState([])
	const [stMyComments, setstMyComments] = useState([])
	const [stSeo, setstSeo] = useState({})	
	const [stPage, setStPage] = useState(1);
	const [, setStMaxPage] = useState(1);
	const [stShowLoadMore, setstShowLoadMore] = useState('');
	const [stMoonLoading, setstMoonLoading] = useState(false);

	useEffect(() => {		
		fetchArticle()
		// fetchComment()
		window.scrollTo(0, 0)				
	},[])

	useEffect(() => {		
		setstMoonLoading(true)
        fetchCommentService.fetchComment("article", props.match.params.id,stPage)
            .then(result => {
                setStMaxPage(parseInt(result.totalPage))                
				setComments([...Comments, ...result.data.comments])
				setstMyComments([...stMyComments, ...result.data.my_comments])

				if (result.data.comments.length < 10 ){
                    setstShowLoadMore('none')
				} else {
					setstShowLoadMore('')
				}
				setstMoonLoading(false)
			})
			.catch(error => {
				setstMoonLoading(false)
			})
    }, [stPage]);
		
	const fetchArticle = () => {
		apiClient.get(`${process.env.REACT_APP_API_URL}/api/v1/articles/${props.match.params.id}.json`)
			.then(response => {
				setstArticles(response.data)
				setCategory(response.data.category)
				setUser(response.data.user)

				if (response.data.video !== null){
					setstSeo({ 
						title: response.data.header,
						type: "website",
						description: response.data.header,
						image: response.data.video.thumbnail,						
						url:"https://funniq.com/articles/" +  response.data.uuid + "-" + response.data.slug
					})
				} else {
					setstSeo({ 
						title: response.data.header,
						type: "website",
						description: response.data.header,
						image: response.data.image.url,
						image_width: response.data.image.width,
						image_height: response.data.image.height,
						url:"https://funniq.com/articles/" +  response.data.uuid + "-" + response.data.slug
					})
				}
				
			})
			.catch(error => {
                setstShowLoadMore('none')
            })
	}

	const fetchComment = () => {
		apiClient.get(`${process.env.REACT_APP_API_URL}/api/v1/comments/article_comments?article_id=${props.match.params.id}`)
			.then(response => {				
				// setUser(response.data.user)				
				setComments([
					// ...Comments,
					...response.data.comments])
			})
		.catch(error => swal("Error", `${error}, please contact us at https://facebook.com/FunniqPage`, "error"))
	}
	

	const loadMoreUsers = () => {
        setStPage(stPage + 1)
	}
	
	// const renderArticleNavigation = () => {
	// 	return (
	// 		<div className='d-flex flex-row justify-content-between' >
	// 			<button className="p-0 btn">Previous post</button>
    //   			<button className="p-0 btn">Next post</button>
	// 		</div>
	// 	)
	// }	

	const renderMyComments = () => {

		if (stMyComments.length > 0){
			return (
				<>
					<div className='text-left my_comments'>
						Your comments
					</div>

					<CommentList 
						comments = {stMyComments}
						onSubmit={fetchComment}
						relatedArticle={stArticles}/>

					<div className='border mt-1 px-5 comment_divider' style={{
						width: '90%',
						marginLeft: 'auto',
						marginRight: 'auto'
					}}>

					</div>
				</>
			)
		}
	}

	const renderComments = () => {		
		return (<>
			<div className='text-left public_comments'>
										Comments
									</div>
			<CommentList 
				comments = {Comments}
				onSubmit={fetchComment}
				relatedArticle={stArticles}/>
		</>)				
	}

	const renderLoadMore = () => {
		return (
			<div className='pl-2 pr-2'>
				<LoadMoreButton loadMore={loadMoreUsers} 
					displayedText={'More Comments...'} 
					isShown={stShowLoadMore}
					stIsLoading={stMoonLoading}
				/>	
			</div>
		)
	
	}

        return (
			// <CommentsContext.Provider value={[Comments, setComments]}>						
				<MDBContainer className="home">
					<SEO pageProps={stSeo} />
						<MDBRow className="content m-0 pr-1 pl-1">
								<MDBCol sm={0} lg={3} md={3} xl={3} xs={4} className="hidden-xs pl-0">
										<LeftStickyNav/>
								</MDBCol>
								<MDBCol sm={12} lg={6} md={6} xl={6} xs={7} middle className="p-0 mb-5" key={props.match.params.id}>									
									<DetailArticle 										
										article={stArticles}
										/>
								
									<CommentForm														
										obj="article"
										id={props.match.params.id}
										onSubmit={fetchComment}
									/>

									{renderMyComments()}
									
									{renderComments()}

									{renderLoadMore()}
										
									<div className='mb-5'>
										
									</div>											
									<div style={{ height: '100vh'}}>
                        
                    				</div>                       
								</MDBCol>
								<MDBCol sm={0} lg={3} md={3} xl={3} xs={4} className="hidden-xs pl-0 pr-0">
										<RightStickyNav/>                    
								</MDBCol>							
						</MDBRow>
					
				</MDBContainer>
			// </CommentsContext.Provider>   
          )
}

export default Detail;