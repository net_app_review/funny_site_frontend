import React, {useEffect, useState} from 'react';
// import PropTypes from 'prop-types';
import {searchPageService} from "../../../services/searchPageService"
import './SearchUsers.sass'
import {Link} from 'react-router-dom'
import UserLevelBadge from '../../../components/UserLevelBadge/UserLevelBadge'
import UserNationalFlag from '../../../components/UserNationalFlag/UserNationalFlag'
import LoadMoreButton from '../../../components/LoadMoreButton/LoadMoreButton'
import swal from 'sweetalert'

const SearchUsers = props => {

    const [stPage, setStPage] = useState(1);
    const [stUsers, setstUsers] = useState([]);    
    const [stShowLoadMore, setstShowLoadMore] = useState('');
    const [stMoonLoading, setstMoonLoading] = useState(false);    
    

    useEffect(() => {
        setstMoonLoading(true)
        searchPageService.getUsers(props.query,stPage)
            .then(result => {
                // setStMaxPage(parseInt(result.totalPage))                
                setstUsers([...stUsers, ...result.data])                
                if (result.data.length >= 0 && result.data.length <= 4 ){
                    setstShowLoadMore('none')
                }
                setstMoonLoading(false)
            })
            .catch(error => {
                // swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
                setstShowLoadMore('none')
                setstMoonLoading(false)
            })        
    }, [stPage]);

    useEffect(()=>{
        window.scrollTo(0, 0)
    }, [])

    const loadMoreUsers = () => {
        setStPage(stPage + 1)
    }

    const renderLoadMoreBtn = () => {
        return (                
                <LoadMoreButton loadMore={loadMoreUsers} 
                    displayedText={'More Users...'} 
                    isShown={stShowLoadMore}
                    stIsLoading={stMoonLoading}
                />
                )
    }
    return (
        <div className="user_list">
            {stUsers.length > 0 && stUsers.map((user, index) => {
                return (                                        
                    <Link to={`/users/${user.uuid}`} className="user_item d-flex flex-row" key={index}>
                        <div className='user_image d-flex flex-column'>
                            <img src={user.image.url} className='avatar' alt={user.name}></img>
                        </div>
                        <div className='user_stats d-flex flex-column'>
                            <div className='d-flex flex-row'>
                                <div className='flag'>
                                    <UserNationalFlag country_code={user.country_code} height="16px"/>
                                </div>                                
                                <div className='level_badge'>
                                    <UserLevelBadge level={user.level} />
                                </div>
                            </div>                            
                            <div className='name'>
                                {user.name}
                            </div>
                            <div className='d-flex flex-row justify-content-around rank_points'>
                                <div className='rank'>
                                    # Rank:                                    
                                    <span className='pl-1'>{user.rank}</span>                                    
                                </div>
                                <div className='points'>
                                    Points:
                                    <span className='pl-1'>{user.points}</span>                                    
                                </div>
                            </div>                            
                        </div>                            
                    </Link>                    
                )
            })}
            {renderLoadMoreBtn()}
        </div>
    );
};


SearchUsers.propTypes = {};

export default SearchUsers;