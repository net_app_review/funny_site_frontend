import React, {useEffect, useState} from 'react';
import {searchPageService} from "../../../services/searchPageService"
import './SearchTags.sass'
import {Link} from 'react-router-dom'
import LoadMoreButton from '../../../components/LoadMoreButton/LoadMoreButton'
import swal from 'sweetalert'

const SearchTags = props => {

    const [stPage, setStPage] = useState(1);
    const [stTags, setstTags] = useState([]);
    const [, setStMaxPage] = useState(1);
    const [stShowLoadMore, setstShowLoadMore] = useState('');
    const [stMoonLoading, setstMoonLoading] = useState(false);
    
    useEffect(()=>{
        window.scrollTo(0, 0)
    }, [])

    useEffect(() => {
        setstMoonLoading(true)
        searchPageService.getTags(props.query,stPage)
            .then(result => {                                         
                setStMaxPage(parseInt(result.totalPage))                
                setstTags([...stTags, ...result.data.tags])
                if (result.data.length >= 0 && result.data.length <= 4 ){
                    setstShowLoadMore('none')
                    
                }
                setstMoonLoading(false)
            })
            .catch(error => {
                // swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");              
                setstShowLoadMore('none')  
                setstMoonLoading(false)           
            })        
    }, [stPage]);

    const loadMoreTags = () => {
        setStPage(stPage + 1)        
    }

    return (
        <div className="tag_list">
            {stTags.length > 0 && stTags.map((tag, index) => {
                return (                       
                    <Link to={`/tags/${tag.id}`} className="tag_item d-flex flex-row" key={index}>                    
                        {tag.name}                                            
                    </Link>                 
                )
            })}
            <LoadMoreButton loadMore={loadMoreTags} 
                            displayedText={'More Tags...'} 
                            isShown={stShowLoadMore}
                            stIsLoading={stMoonLoading}
                            />
        </div>
    );
};


SearchTags.propTypes = {};

export default SearchTags;