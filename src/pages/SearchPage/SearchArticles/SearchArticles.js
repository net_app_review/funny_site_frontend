import React, {useEffect, useState} from 'react';
import Fun from "../../Shared/Fun/Fun"
import {searchPageService} from "../../../services/searchPageService"
import LoadMoreButton from '../../../components/LoadMoreButton/LoadMoreButton'
import {removeHidingArticles} from "../../../functions/removeHidingArticles"
import swal from 'sweetalert'
import './SearchArticles.sass'

const SearchArticles = props => {

    const [stPage, setStPage] = useState(1);
    const [stArticles, setStArticles] = useState([]);
    const [, setStMaxPage] = useState(1);    
    const [stShowLoadMore, setstShowLoadMore] = useState('');
    const [stMoonLoading, setstMoonLoading] = useState(false);        

    useEffect(()=>{
        window.scrollTo(0, 0)
    }, [])

    useEffect(() => {
        setstMoonLoading(true)
        searchPageService.getArticles(props.query,stPage)
            .then(result => {                         
                setStMaxPage(parseInt(result.totalPage))            
                setStArticles([...stArticles, ...removeHidingArticles.removeHidedArticle(result.data,'hide')])
                if (result.data.length >= 0 && result.data.length <= 4){
                    setstShowLoadMore('none')
                }
                setstMoonLoading(false)
            })
            .catch(error => {                
                setstShowLoadMore('none')
                setstMoonLoading(false)
                // swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
            })          
    }, [stPage]);

    const loadMoreArticles = () => {
        setStPage(stPage + 1)        
    }

    return (
        <div className="fun-zone">
            {stArticles.length > 0 && stArticles.map(article => {
                return (
                    <Fun key={article.id} article={article}/>
                )
            })}            
            <LoadMoreButton loadMore={loadMoreArticles} 
                            displayedText={'More Articles...'} 
                            isShown={stShowLoadMore}
                            stIsLoading={stMoonLoading}
                            />       
        </div>
    );
};


SearchArticles.propTypes = {};

export default SearchArticles;