import React from 'react';
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import LeftStickyNav from '../Shared/LeftStickyNav/LeftStickyNav'
import RightStickyNav from '../Shared/RightStickyNav/RightStickyNav'
import SearchArticles from './SearchArticles/SearchArticles'
import SearchTags from './SearchTags/SearchTags'
import SearchUsers from './SearchUsers/SearchUsers'
import SearchDescription from './SearchDescription/SearchDescription'

const SearchPage = props => {

const renderSearchPage = () =>{    
    if (props.location.state !== undefined && props.location.state !== 'undefined'){
        if (props.location.state.detail.search_object === 'post'){
            return (  
                <MDBCol sm={12} lg={6} md={6} xl={6} xs={7} middle>
                    <SearchDescription 
                        obj={props.location.state.detail.search_object}
                        query={props.location.state.detail.search_string}/>
                    <SearchArticles key={props.location.state.detail.search_string}
                        query={props.location.state.detail.search_string}
                        />                      
                </MDBCol>                
            )
        } else if (props.location.state.detail.search_object === 'tag') {
            return (
                <MDBCol sm={12} lg={6} md={6} xl={6} xs={7} middle>
                    <SearchDescription 
                        obj={props.location.state.detail.search_object}
                        query={props.location.state.detail.search_string}/>
                    <SearchTags key={props.location.state.detail.search_string}
                        query={props.location.state.detail.search_string}
                    />
                </MDBCol>       
                
            )
        } else if (props.location.state.detail.search_object === 'user') {
            return (     
                <MDBCol sm={12} lg={6} md={6} xl={6} xs={7} middle>
                    <SearchDescription 
                        obj={props.location.state.detail.search_object}
                        query={props.location.state.detail.search_string}/>
                    <SearchUsers key={props.location.state.detail.search_string}
                    query={props.location.state.detail.search_string}
                    />
                </MDBCol>                        
            )
        }    
    } else {
        return (
            <MDBCol sm={12} lg={6} md={6} xl={6} xs={7} middle>                    
            </MDBCol>  
        )
    }
}

    return (
        <MDBContainer className="home">
            <MDBRow className="content">
                <MDBCol sm={0} lg={3} md={3} xl={3} xs={4} className="hidden-xs pl-0">
                    <LeftStickyNav/>
                </MDBCol>                
                {renderSearchPage()}      
                <MDBCol sm={0} lg={3} md={3} xl={3} xs={4} className="hidden-xs pl-0 pr-0">
                    <RightStickyNav/>                    
                </MDBCol>
            </MDBRow>           
        </MDBContainer>
    );
};

SearchPage.propTypes = {

};

export default SearchPage;
