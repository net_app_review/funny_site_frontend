import React from 'react';
// import PropTypes from 'prop-types';
// import axios from 'axios'
import {MDBCol, MDBContainer, MDBRow} from "mdbreact";
// import styles from "./SearchDescription.module.sass"

const SearchDescription = props => {

    return (
        <MDBContainer>
            <MDBRow >
                {/* <MDBCol size="2" className="pr-0">
                    <img className="img-fluid rounded" src="https://media.giphy.com/media/h2HTvVuKVS4daQaVuS/giphy.gif" alt=""/>
                </MDBCol> */}
                <MDBCol size="12" className="mb-4 mb-lg-2">
                    <MDBRow className="pl-3 p-lg-0"> 
                        <h4>
                            Search results for {props.obj}: <strong>{props.query}</strong>
                        </h4>
                    </MDBRow>
                    {/* <MDBRow  style={{margin: '5px' }}>
                        <p>
                            Hottest stuff here
                        </p>
                    </MDBRow> */}
                </MDBCol>                
            </MDBRow> 
        </MDBContainer>
    );
};


SearchDescription.propTypes = {};

export default SearchDescription;