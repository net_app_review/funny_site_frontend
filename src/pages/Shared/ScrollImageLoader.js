import ContentLoader from "react-content-loader";
import React from 'react';

const ScrollImageLoader = () => (
    
    <div style={{
        padding: '0 15px'
    }}>
    <ContentLoader
        height={475}
        width={500}
        speed={2}
        primaryColor="var(--color-ivory)"
        secondaryColor="#ecebeb"
    >
        <rect x="2" y="2" rx="4" ry="4" width="314" height="21" />
        <rect x="2" y="29" rx="4" ry="4" width="264" height="13" />
        <rect x="2" y="53" rx="5" ry="5" width="420" height="408" />
        <rect x="446" y="56" rx="4" ry="4" width="50" height="202" />
        <rect x="445" y="411" rx="4" ry="4" width="50" height="50" />
    </ContentLoader>
    </div>
)

export default ScrollImageLoader;