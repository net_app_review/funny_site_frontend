import React from 'react';
// import PropTypes from 'prop-types';
import './PopularNav.sass'
import {Link} from 'react-router-dom'
import { MDBRow } from "mdbreact";
import { useTranslation } from "react-i18next";

const PopularNav = props => {
    const { t, i18n } = useTranslation();
    return (
        <div className="popular-nav d-flex flex-column">
            <div className="popular_nav_title">
            {t("home.popular")}                
            </div>            
            <div className="frame">    
                <Link to={"/hot"} className="popular_link">
                    <span className="badge badge-pill  badge-dark">{t("home.hot")}</span>
                </Link>
                <Link to={"/fresh"} className="popular_link">
                    <span className="badge badge-pill  badge-dark">{t("home.fresh")}</span>
                </Link>
                <Link to={"/trending"} className="popular_link">
                    <span className="badge badge-pill  badge-dark">{t("home.trending")}</span>
                </Link>
                <Link to={"/about/feedback"} className="popular_link">
                    <span className="badge badge-pill  badge-dark">Feedback</span>
                </Link>
                <a href="https://status.funniq.com" aria-label="status_page" target="_blank" rel="noopener noreferrer" className="popular_link" >
                    <span className="badge badge-pill  badge-dark">Service status</span>
                </a>
            </div>
            <div className="get_the_app">
                <MDBRow className="slogan">
                    {/* Mobile App */}
                </MDBRow>
                <MDBRow className="application">
                    {/* <MDBCol size="6" className="gg_play">
                        <button className="btn">
                            <img src="https://cdn4.iconfinder.com/data/icons/free-colorful-icons/360/google_play.png"></img>
                            Google play
                            <div className="coming_soon">
                                Coming soon !
                            </div>                            
                        </button>                        
                    </MDBCol>
                    <MDBCol size="6" className="app_store">
                        <button className="btn">
                            <img src="https://cdn3.iconfinder.com/data/icons/picons-social/57/68-appstore-512.png"></img>
                            App store
                        </button>  
                    </MDBCol> */}
                    {/* Coming soon! */}
                </MDBRow>
            </div>
        </div>
    );
};

PopularNav.propTypes = {

};

export default PopularNav;