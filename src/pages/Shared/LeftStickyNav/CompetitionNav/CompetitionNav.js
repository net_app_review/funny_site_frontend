import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import './CompetitionNav.sass'
import { MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";
import {categoriesService} from "../../../../services/categoriesService";

const CompetitionNav = props => {

    const [StCompetitors, setStCompetitors] = useState([]);

    useEffect(() => {
        categoriesService.getCategories()
            .then(cates => {
                setStCompetitors(cates)
            })
    }, [])


    return (
        <div className="category-nav">
            <div className="category-nav__title">
                Competition
            </div>
            <MDBNav className="flex-column">
                {StCompetitors.map(cate => {
                    return (
                        <MDBNavItem key={cate.id}>
                            <MDBNavLink to={`/categories/${cate.id}`}>{cate.name}</MDBNavLink>
                        </MDBNavItem>
                    )
                })}

            </MDBNav>
        </div>
    );
};

CompetitionNav.propTypes = {

};

export default CompetitionNav;