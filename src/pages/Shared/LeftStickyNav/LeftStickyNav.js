import React from 'react';
// import PropTypes from 'prop-types';
import './LeftStickyNav.sass'
import { MDBRow } from "mdbreact";
import CategoryNav from '../CategoryNav/CategoryNav'
import CompanyInfoNav from './CompanyInfoNav/CompanyInfoNav'


const LeftStickyNav = props => {

    return (
        <div className="left-sticky-nav">            
            <MDBRow className="m-0">
                <CategoryNav/>
            </MDBRow>
            <MDBRow className="m-0 p-0 company_info_parent">
                <CompanyInfoNav/>                    
            </MDBRow>                        
        </div>
    );
};

LeftStickyNav.propTypes = {

};

export default LeftStickyNav;