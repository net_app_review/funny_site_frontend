import React from 'react';
// import PropTypes from 'prop-types';
import styles from './CompanyInfoNav.module.sass'
import {  MDBRow} from "mdbreact";
// import {categoriesService} from "../../../services/categoriesService";
// import {Link} from 'react-router-dom'
import CompanyInfo from '../../../../components/CompanyInfo/CompanyInfo'

const CompanyInfoNav = props => {
    
    return (
        <div className={`${styles.CompanyInfoNav}`}>             
            <MDBRow className={`${styles.CompanyInfo} m-0 p-0`}>
                <CompanyInfo />
            </MDBRow>                        
            <MDBRow className={`p-0 m-0 ${styles.copy_right}`}>
                <div className="pl-0">
                <span className='pr-1'>
                    &copy;
                </span>                    
                <span className='pr-1'>
                    {(new Date().getFullYear())}
                </span>                 
                <a href="https://funniq.com">
                    funniq.com
                </a>                
                </div>                                
            </MDBRow>
            {/* <MDBRow className={`p-0 m-0 ${styles.dmca}`}>
                <a href="//www.dmca.com/Protection/Status.aspx?ID=761db785-1148-4f2a-9b4d-e6f7d1d56d48" 
                   title="DMCA.com Protection Status" 
                   class="dmca-badge"> 
                   <img src ="https://images.dmca.com/Badges/dmca_protected_sml_120m.png?ID=761db785-1148-4f2a-9b4d-e6f7d1d56d48"  
                        alt="DMCA.com Protection Status"                         
                        />
                </a>  
                <script src="https://images.dmca.com/Badges/DMCABadgeHelper.min.js"> </script>    
            </MDBRow> */}
            
        </div>
    );
};

CompanyInfoNav.propTypes = {

};

export default CompanyInfoNav;