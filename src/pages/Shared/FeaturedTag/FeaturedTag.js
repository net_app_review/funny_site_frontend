import React from 'react';
import {Link} from 'react-router-dom'
import PropTypes from 'prop-types';
import './FeaturedTag.scss'

const FeaturedTag = props => {

    // const {featureTagsList} = props

    return (
        <div className="FeaturedTag d-block d-sm-none">
            <div className="scrolling-wrapper">
                {
                    props.featureTagsList.map(tag => {
                        return (
                            
                                <h3 key={tag.id}>
                                    <Link to={`/tags/${tag.id}-${tag.slug}`}>
                                        <span className="badge badge-pill badge-dark mr-2">
                                            {tag.name}
                                        </span>
                                    </Link>
                                </h3>                           
                            
                        )
                    })
                }        
            </div>
        </div>
    );
};

FeaturedTag.propTypes = {    
    featureTagsList: PropTypes.array
};

export default FeaturedTag;