import React, {useContext, useState} from 'react';
import { withRouter } from 'react-router'
import {CurrentUserContext, LoggedInContext, SignInModalContext} from '../../../Store'
import PropTypes from 'prop-types';
import './Fun.sass'
import {
    MDBCol,    
    MDBDropdown,
    MDBDropdownItem,
    MDBDropdownMenu,
    MDBDropdownToggle,
    MDBIcon,
    MDBRow,
    MDBTooltip,
    MDBBtn
} from 'mdbreact';
// import FunRightContent from './RightContent/FunRightContent';
import {Link} from 'react-router-dom'
import ModalReport from '../../../components/ModalReport/ModalReport';
import {confirmAlert} from 'react-confirm-alert'; // Import alert
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import alert css
import apiClient from '../../../config/apiClient';
import {LikeType} from '../../../model/LikeTypeEnum';
import {LikeService} from '../../../services/likeService';
import {notificationService} from "../../../services/notificationService"
import {calculateElapsedTime} from '../../../functions/calculateElapsedTime'
import {FacebookShareButton, TwitterShareButton} from 'react-share';
import FsLightbox from 'fslightbox-react';
// import ReactPlayer from 'react-player'
import '../../../../node_modules/video-react/dist/video-react.css'; // import css
import swal from 'sweetalert';
import TagDropdown from '../../../components/TagDropdown/TagDropdown'
import { useTranslation } from "react-i18next";
// import Cookies from 'js-cookie'
import LazyLoad from 'react-lazyload';
import ModalUpdatePost from "../../../components/ModalUpdatePost/ModalUpdatePost";
import VizSensor from 'react-visibility-sensor';
import ControlledVideo from './ControlledVideo/ControlledVideo'
import {getArticleImageUrl} from '../../../functions/getArticleImageUrl'

const Fun = props => {

    const [stCurrentUser, ] = useContext(CurrentUserContext)
    const [stIsModalReportOpen, setStIsModalReportOpen] = useState(false);
    const [stIsUpdatePostOpen, setstIsUpdatePostOpen] = useState(false);
    const [, setSignInModal] = useContext(SignInModalContext)
    const [stLoggedIn,] = useContext(LoggedInContext)
    // const [stArticle, setstArticle] = useState({});
    const [stLikeType, setStLikeType] = useState(props.article.like_status.liketype? props.article.like_status.liketype: props.article.like_status);
    const [stLikeCount, setStLikeCount] = useState(props.article.like_count);
    const [stDislikeCount, setStDislikeCount] = useState(props.article.dislike_count);
    const [stImgLightBox, setstImgLightBox] = useState(false);
    const [stVideoAutoPlay, setstVideoAutoPlay] = useState(false);
    const [stLikeId, setstLikeId] = useState(props.article.like_status.id ? props.article.like_status.id : 0);
    
    const [stArticleDisplay, setstArticleDisplay] = useState("d-flex");
    const { t, i18n } = useTranslation();
    
    const showDeletePostConfirmation = () => {
        confirmAlert({
            title: 'Delete this post',
            message: 'Are you sure to do this?',
            buttons: [
              {
                label: 'Yes',
                onClick: () => {
                    deletePost()
                }
              },
              {
                label: 'No',
                onClick: () => {}
              }
            ]
        });
    }

    const deletePost =() => {

        let data = {
            id: props.article.id,
        }

        apiClient.post(`${process.env.REACT_APP_API_URL}/api/v1/articles/destroy`, data
        ).then(response => {
            if (response.status === 200){
                swal('Post deleted', 'Your post have been deleted', 'success');
                setstArticleDisplay('d-none')
                // props.history.push('/')
                // window.location.reload();                
            }
        })
            .catch(error => {                
                swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
         })
    }

    const isLogin = () => {
        return stLoggedIn === 'LOGGED_IN'
    }

    const onOpenModalReport = () => {
        if (stLoggedIn === 'LOGGED_IN'){
            setStIsModalReportOpen(true)
        } else {
            setSignInModal(true)            
        }
    }

    const onOpenUpdatePost = () => {
        if (stLoggedIn === 'LOGGED_IN'){
            setstIsUpdatePostOpen(true)
        } else {
            setSignInModal(true)            
        }
    }

    const openLoginModal = () => {
        setSignInModal(true)
    }

    const toggleModalReport = () => {
        setStIsModalReportOpen(!stIsModalReportOpen)
    }

    const toggleUpdatePost = () => {
        setstIsUpdatePostOpen(!stIsUpdatePostOpen)
    }

    // const getNotiObjectImageUrl = () => {
    //     let url = ''
    //     if (props.article.image === null || props.article.image === 'null'){
    //         url = props.article.video.thumbnail
    //     } else {
    //         url = props.article.image.url
    //     }
    //     return url
    // }

    const onClickLike = () => {
        if (!isLogin()) {
            openLoginModal()
            return
        }

        LikeService.like('article_id', article.id, stLikeType, stLikeId).then(result => 
            {                
                setstLikeId(result.id)
            }
        )

        if (stLikeId === 0 && stCurrentUser.id !== props.article.user.id ){
            notificationService.createNotification(
                stCurrentUser.uuid,
                stCurrentUser.name,
                stCurrentUser.image.url,
                article.uuid,
                'article',
                article.uuid,
                '',
                getArticleImageUrl.getImageUrl(props.article),
                props.article.user.uuid,
                `${stCurrentUser.name} has liked your article`
            )
        }
        
        switch (stLikeType) {
            case LikeType.LIKE: // => change like to unlike
                setStLikeCount(stLikeCount-1)
                setStLikeType(LikeType.NO_VALUE);
                break
            case LikeType.DISLIKE:
                setStLikeType(LikeType.LIKE);
                setStLikeCount(stLikeCount+1)
                setStDislikeCount(stDislikeCount-1)
                break
            case LikeType.NO_VALUE:
                setStLikeCount(stLikeCount+1)
                setStLikeType(LikeType.LIKE);
                break
            case 'no_action':
                setStLikeType(LikeType.LIKE);
                setStLikeCount(stLikeCount+1)
                break
            default:
                console.log('default')
        }
    }
    
    const onClickDislike = () => {
        if (!isLogin()) {
            openLoginModal()
            return
        }
        LikeService.dislike('article_id', article.id, stLikeType, stLikeId).then(result => 
            {
                setstLikeId(result.id)
            }
        )
        switch (stLikeType) {
            case LikeType.LIKE:
                setStLikeCount(stLikeCount-1)
                setStDislikeCount(stDislikeCount+1)
                setStLikeType(LikeType.DISLIKE);
                break
            case LikeType.DISLIKE:
                setStLikeType(LikeType.NO_VALUE);
                setStDislikeCount(stDislikeCount-1)
                break
            case LikeType.NO_VALUE:
                setStDislikeCount(stDislikeCount+1)
                setStLikeType(LikeType.DISLIKE);
                break
            case 'no_action':
                setStLikeType(LikeType.DISLIKE);
                setStDislikeCount(stDislikeCount+1)
                break
            default:
                console.log('default')
                          
        }
    }

    const isLiked = () => {
        return stLikeType === LikeType.LIKE
    }

    const isDisliked = () => {
        return stLikeType === LikeType.DISLIKE
    }

    const renderReport = () => {        
            if (article.user.email === stCurrentUser.email ){
                return <MDBDropdownMenu right className='custom-dropdown-menu'>
                            <MDBDropdownItem onClick={showDeletePostConfirmation}>Delete</MDBDropdownItem>
                            <MDBDropdownItem onClick={onOpenUpdatePost}>Edit Post</MDBDropdownItem>
                        </MDBDropdownMenu>
            } else {
                return <MDBDropdownMenu right className='custom-dropdown-menu'>
                            <MDBDropdownItem onClick={onOpenModalReport}>
                                <MDBIcon icon='flag' />
                                {' '}
                                {t("home.report")}
                            </MDBDropdownItem>
                            <MDBDropdownItem divider />
                            <MDBDropdownItem onClick={hideArticle}>
                                <MDBIcon icon='minus-circle' />
                                {' '}
                                {t("home.hide")}
                            </MDBDropdownItem>
                        </MDBDropdownMenu>                                      
            }                                                                                      
                    
    }

    const ArticleElapsedTime = () => {
        return calculateElapsedTime.elapsedTime(props.article.elapsed_time)
    }

    const createYoutubeEmbedUrl = (url) => {
        // https://www.youtube.com/watch?v=MVxl0zXZBR4
        const urlObj = new URL(url)
        const id  = urlObj.searchParams.get('v');
        return 'https://www.youtube.com/embed/' + id
    }
    
    const resolveSharedLink = () => {
        const domain = window.location.protocol + '//' + window.location.hostname + '/articles/'
        const id = props.article.uuid
        const slug = props.article.slug
        return domain + id + '-' + slug
    }
    
    const MobileFooter = () => {
        return <>
             <MDBRow className='footer d-xs-flex flex-row p-0 m-0'>            
                <div className='flex-fill bd-highlight '>
                    
                    <button className={`${['btn-footer'].join(' ')}`}
                            onClick={onClickLike}>
                        {
                            isLiked()?<MDBIcon icon='arrow-alt-circle-up' className='liked'/>:<MDBIcon far icon='arrow-alt-circle-up' />
                        }
                        {' '}
                        {/* {stLikeCount} */}
                    </button>
                    
                </div>
                <div className='flex-fill bd-highlight'>
                    <div className={[`btn-footer point_count ${stLikeCount - stDislikeCount >= 0 ? 'positive' : 'negative'}`]}>
                        {stLikeCount - stDislikeCount}
                        {' '}                        
                    </div>
                </div>
                <div className='flex-fill bd-highlight'>
                    <button className={['btn-footer']}
                            onClick={onClickDislike}>
                        {
                            isDisliked()?<MDBIcon icon='arrow-alt-circle-down' className='disliked'/>:<MDBIcon far icon='arrow-alt-circle-down' />
                        }
                        {' '}                        
                        {/* {stDislikeCount} */}
                    </button>
                </div>            
                <div className='flex-fill bd-highlight'>
                    <Link to={{ pathname: `/articles/${article.uuid}`}} style={{ textDecoration: 'none' }}>                        
                        <button className='btn-footer'>
                            <MDBIcon far icon='comments' />
                            {' '}
                            
                            {article.comment_count}
                        </button>
                    </Link>
                </div>
                <div className='flex-fill bd-highlight'>
                    <MDBDropdown className='more-dropdown'  dropdown>
                        <MDBDropdownToggle className='btn-footer custom-dropdown-toggle'  color='primary'>
                            <MDBIcon icon='share-alt-square' />
                            Share
                        </MDBDropdownToggle>
                        <MDBDropdownMenu right className='custom-dropdown-menu'>                            
                            <MDBDropdownItem>                               
                                <MDBTooltip placement="top" >
                                    <MDBBtn className='share_btn'>
                                            <FacebookShareButton url={resolveSharedLink()}
                                                                children={<>
                                                                    <MDBIcon fab icon='facebook-square mdb-gallery-view-icon' />
                                                                    {' '}Facebook
                                                                </>}
                                            />
                                    </MDBBtn>
                                    <div>Please share the second time if the first time doesn't work correctly</div>                                    
                                    </MDBTooltip>
                            </MDBDropdownItem>
                            
                            {/* <MDBDropdownItem divider/> */}
                            <MDBDropdownItem>
                                <MDBTooltip placement="top" >
                                    <MDBBtn className='share_btn'>
                                        <TwitterShareButton url={resolveSharedLink()}
                                                            children={<>
                                                                <MDBIcon fab icon='twitter-square mdb-gallery-view-icon' />
                                                                {' '}Twitter
                                                            </>}
                                        />
                                        
                                    </MDBBtn>
                                    <div>Please share the second time if the first time doesn't work correctly</div>                                    
                                </MDBTooltip>
                            </MDBDropdownItem>
                        </MDBDropdownMenu>
                    </MDBDropdown>
                </div>
            </MDBRow>
        </>
    }

    const renderSensitiveOverlay = (is_sensitive, article_id) => {
        
        // return (
        //     <div className='sensitive_overlay'>
        //         Sensitive Content                
        //     </div>
        // )
        if (is_sensitive){                            
            if(!isLogin()){                
                return (
                    <div className='sensitive_overlay' onClick={
                        (e) => {
                        props.history.push({
                            pathname: `/articles/${article_id}`                            
                        });
                        openLoginModal();}
                     }>
                        Sensitive Content
                        <Link to={`/about/terms`} style={{ color: 'white'}}>
                            <div className='info_symbol'>
                                i
                            </div>
                        </Link>
                    </div>
                )
            } else {
                return <></>            
            }
        }        
    }

    const hideArticle = () => {
        setstArticleDisplay('d-none')    
        hideArticleOnServer()    
    }

    const hideArticleOnServer = () => {
        LikeService.hide(props.article.id, 'hide', stLikeId ? stLikeId : '' )
    }
    

    const renderArticleImage = (article) => {
        
            if (article.image)
                if (article.sensitive && !isLogin()){
                    return (
                        <div className='senstitive_replaced'>
                            {renderSensitiveOverlay(article.sensitive, article.uuid)}
                        </div>
                    )
                } else {
                return(
                    <div className='article_image'>
                        <FsLightbox toggler={stImgLightBox}
                            customSources={ [ 
                                <img className='image img-fluid'
                                    alt={article.header}
                                    src={article.image.url}
                                    onClick={ () => setstImgLightBox(!stImgLightBox) }/>
                                ] } 
                                customSourcesGlobalMaxDimensions={ 
                                    { width: 600, height: 700 } 
                                } 
                                customSourcesMaxDimensions={ 
                                    [null, null, { width: 300, height: 50 }] 
                                } 
                            />
                            <LazyLoad height={'100%'} 
                                        offset={300} 
                                        placeholder={<img src='https://firebasestorage.googleapis.com/v0/b/funniq-78dbd.appspot.com/o/MeekAdorableIaerismetalmark-size_restricted%5B1%5D.gif?alt=media&token=9f30e23c-282d-44ea-a3f2-e110ac11df9d'
                                                        className='image img-fluid' alt="img"></img>}>
                                <img className='image img-fluid'
                                    alt={article.header}
                                    src={article.image.url}
                                    onClick={ () => setstImgLightBox(!stImgLightBox) }/>
                            </LazyLoad>                                                                                   
                    </ div >        
            )
        }
    }

    const onVideoVisible = (isVisible) => {        
        setstVideoAutoPlay(isVisible)             
    }
    
    const renderArticleVideo = (article) => {

        // check sensitive first
        
            // Load normal videos not from youtube
            if (article.video && !article.video.url.includes('https://www.youtube.com/watch?v=')){
                if (article.sensitive && !isLogin()){
                    return (
                        <div className='senstitive_replaced'>
                            {renderSensitiveOverlay(article.sensitive, article.uuid)}
                        </div>
                    )
                } else {
                    return (                        
                        <LazyLoad height={'100%'} 
                            offset={300} 
                            placeholder={<img src='https://firebasestorage.googleapis.com/v0/b/funniq-78dbd.appspot.com/o/MeekAdorableIaerismetalmark-size_restricted%5B1%5D.gif?alt=media&token=9f30e23c-282d-44ea-a3f2-e110ac11df9d'
                            className='image img-fluid' alt="img"></img>}>
                            <VizSensor 
                                onChange={onVideoVisible}
                            >
                                <ControlledVideo 
                                    video_source={article.video.url}
                                    play_now={!stVideoAutoPlay}
                                    poster_url={article.video.thumbnail}
                                />
                            </VizSensor>           
                        </LazyLoad>  
                    )
                }
            } else if (article.video && article.video.url.includes('https://www.youtube.com/watch?v=')) {
                return (
                    <LazyLoad height={'100%'} 
                            offset={300} 
                            placeholder={<img src='https://firebasestorage.googleapis.com/v0/b/funniq-78dbd.appspot.com/o/MeekAdorableIaerismetalmark-size_restricted%5B1%5D.gif?alt=media&token=9f30e23c-282d-44ea-a3f2-e110ac11df9d'
                            className='image img-fluid' alt='placeholder'></img>}>
                                <iframe id='ytplayer'
                                    type='text/html'
                                    width='100%'
                                    height='360'
                                    src={createYoutubeEmbedUrl(article.video.url)}
                                    frameBorder='0'
                                    title='uniq'/>
                            </LazyLoad> 
                )
            }
    }


    const article = props.article

    return (
        <div className={`fun ${stArticleDisplay} flex-column`}>            
            <ModalReport    report_object_type='article'
                            report_object={article}
                            isOpen={stIsModalReportOpen}
                           toggle={toggleModalReport}/>
            <ModalUpdatePost    article={article}
                                isOpen={stIsUpdatePostOpen}
                                toggle={toggleUpdatePost}/>

            <div className='m-0 p-0 d-flex'>
                <div className='pt-0 pb-0 pl-0 pr-0 flex-grow-1 bd-highlight d-flex flex-column'>
                    <Link to={{pathname: `/articles/${article.uuid}-${article.slug}`}} style={{ textDecoration: 'none' , color: 'black'}}                          
                          >
                        <h3 className='articles-name'>
                            {article.header}
                        </h3>
                    </Link>
                    <h4 className='articles-author d-flex flex-row'>
                        {/* Uploaded by
                        &nbsp; */}
                        <Link to={`/users/${article.user.uuid}-${article.user.slug}`} style={{ textDecoration: 'none',                              
                             fontWeight: '400' ,
                             wordBreak: 'break-word'
                            }}>
                            {article.user.name}
                        </Link>
                        <div className='separated_dot'>
                            &#8231;
                        </div>
                        
                        <div className='elapsed_time' style={{ textDecoration: 'none', 'fontWeight': '400' }}>
                            {ArticleElapsedTime()}                             
                        </div>
                        <div className='separated_dot'>
                            &#8231;
                        </div>
                        <Link to={`/categories/${article.category.id}`} style={{ textDecoration: 'none', color: 'green', 'fontWeight': '400'  }}>
                            <MDBIcon icon={article.category.icon}/>
                                &nbsp;
                                {article.category.name}
                        </Link>
                        <div className='separated_dot'>
                            &#8231;
                        </div>
                        <div className='view_count'>
                            {article.view}
                        </div>                        
                        &nbsp;
                        <MDBIcon icon='eye' className='article_view_icon'/>
                        <div className='separated_dot'>
                            &#8231;
                        </div>
                        <TagDropdown tags={props.article.tags}/>
                        
                    </h4>
                    
                </div>
                <div className='flex-shrink-1'>
                    <MDBDropdown className='float-rights'>
                       <MDBDropdownToggle color='#FFFFF' className='settings-dropdown' aria-label='settings-dropdown'>
                           <MDBIcon icon='angle-down' style={{color: 'green'}} />
                       </MDBDropdownToggle>
                        {
                            renderReport()
                        }
                    </MDBDropdown>
                </div>
            </div>
            <MDBRow className='m-0 p-0 content_row' >                
                    <MDBCol sm={12} md={12} className='p-0 m-0 main_content'>                        
                        {renderArticleImage(article)}
                        {renderArticleVideo(article)}                                                                                           
                        {MobileFooter()}
                </MDBCol>
            </MDBRow>
            
                    

            <MDBRow className='p-0 m-0'>
                <MDBCol sm={12} md={12} className='p-0 m-0'>
                    <hr className='hr-bold p-0 mt-1 mb-1'/>
                </MDBCol>
            </MDBRow>
        </div>
    );

    
};

Fun.propTypes = {
    modifyFun: PropTypes.func,
    article: PropTypes.shape({
        // created_at: PropTypes.instanceOf(Date),
        id: PropTypes.number,
        header: PropTypes.string,
        content: PropTypes.string,
        view: PropTypes.number,
        comment_count: PropTypes.number,
        like_count: PropTypes.number,
        dislike_count: PropTypes.number,        
        like_status: PropTypes.any,
        user: PropTypes.shape({
            id: PropTypes.number,
            name: PropTypes.string
        }),
        category: PropTypes.shape({
            icon: PropTypes.string,
            id: PropTypes.number,
            name: PropTypes.string
        }),
        image: PropTypes.shape({
            id: PropTypes.number,
            url: PropTypes.string,
            height: PropTypes.number,
            width: PropTypes.number
        }),
        status: PropTypes.string,
        video: PropTypes.shape({
            id: PropTypes.number,
            url: PropTypes.string
        })
    })
};

export default withRouter(Fun);

