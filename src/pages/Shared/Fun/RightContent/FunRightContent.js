import React, {useState, useEffect, useContext} from 'react';
import PropTypes from 'prop-types';
import './FunRightContent.sass'
import {CurrentUserContext,SignInModalContext,LoggedInContext} from '../../../../Store'
import {MDBBtn,  MDBIcon} from 'mdbreact'
import {useSpring, animated, config} from 'react-spring'
// import { store } from 'react-notifications-component';
import {LikeService} from "../../../../services/likeService";
import {LikeType} from "../../../../model/LikeTypeEnum";
import {Link} from 'react-router-dom'
import {FacebookShareButton, TwitterShareButton} from "react-share";

const FunRightContent = props => {


    const [stCommentMode, setStCommentMode] = useState(false);
    const [stLikeStatus, setstLikeStatus] = useState();    
    const [SignInModal, setSignInModal] = useContext(SignInModalContext)
    const [stLoggedIn] = useContext(LoggedInContext)
    const [stLikeType, setStLikeType] = useState(props.article.like_status.liketype? props.article.like_status.liketype: props.article.like_status);
    const [stLikeCount, setStLikeCount] = useState(props.article.like_count);
    const [stDislikeCount, setStDislikeCount] = useState(props.article.dislike_count);
    // const [stCurrentUser, setstCurrentUser] = useContext(CurrentUserContext)

    const LikeButtonColor = () => {
        if (props.article.like_status === "like") {
            return "success"
        } else if (props.article.like_status === "dislike") {
            return "warning"
        } else {
            return "default"
        }               
    }

    const onClickLike = () => {
        if (!isLogin()) {
            openLoginModal()
            return
        }
        LikeService.like(article.id, stLikeType, article.like_status.id)
        switch (stLikeType) {
            case LikeType.LIKE: // => change like to unlike
                setStLikeCount(stLikeCount-1)
                setStLikeType(LikeType.NO_VALUE);
                break
            case LikeType.DISLIKE:
                setStLikeType(LikeType.LIKE);
                setStLikeCount(stLikeCount+1)
                setStDislikeCount(stDislikeCount-1)
                break
            case LikeType.NO_VALUE:
            case 'no_action':
                setStLikeType(LikeType.LIKE);
                setStLikeCount(stLikeCount+1)
        }
    }

    const isLogin = () => {
        return stLoggedIn === "LOGGED_IN"
    }

    const openLoginModal = () => {
        setSignInModal(true)
    }

    const onClickDislike = () => {
        if (!isLogin()) {
            openLoginModal()
            return
        }
        LikeService.dislike(article.id, stLikeType, article.like_status.id)
        switch (stLikeType) {
            case LikeType.LIKE:
                setStLikeCount(stLikeCount-1)
                setStDislikeCount(stDislikeCount+1)
                setStLikeType(LikeType.DISLIKE);
                break
            case LikeType.DISLIKE:
                setStLikeType(LikeType.NO_VALUE);
                setStDislikeCount(stDislikeCount-1)
                break
            case LikeType.NO_VALUE:
                setStLikeType(LikeType.DISLIKE);
                setStDislikeCount(stDislikeCount+1)
                break
            case 'no_action':
                setStLikeType(LikeType.DISLIKE);
                setStDislikeCount(stDislikeCount+1)
                break
            default:
                console.log('default')
        }
    }

    const isLiked = () => {
        return stLikeType === LikeType.LIKE
    }

    const isDisliked = () => {
        return stLikeType === LikeType.DISLIKE
    }
    
    const changeLikeThumbsColor = () => {
        if (isLiked()) {
            return "thumbs-green"
        }
        return "thumbs-black"
    }

    const changeDisLikeThumbsColor = () => {        
        if (isDisliked()){
            return "thumbs-green"
        } else {
            return "thumbs-black"
        }
    }

    useEffect(() => {
        // const tOut = setTimeout(() => {
        //     setStCommentMode(!stCommentMode)
        // }, 3000)
        // return () => {
        //     clearTimeout(tOut)
        // };
        setStCommentMode(false)
    }, []);

    useEffect(()=>{
        if (props.article.like_status === "not_authenticated"){
            setstLikeStatus("not_authenticated")
        } else if (props.article.like_status === "no_action") {
            setstLikeStatus(props.article.like_status)
        } else {
            setstLikeStatus(props.article.like_status.liketype)
        }        
    },[])   

    const spgCmtCountRef = useSpring({
        config: config.stiff,
        from: {
            transform: `rotateZ(${stCommentMode? 0:180}deg) scale(${stCommentMode? 1:0})`,
            opacity: stCommentMode? 1:0,
            display: stCommentMode? 'flex':'none'
        },
        to: {
            transform: `rotateZ(${stCommentMode? 180:0}deg)  scale(${stCommentMode? 0:1})`,
            opacity: stCommentMode? 0:1,
            display: stCommentMode? 'none':'flex'
        }
    })
    const spgCmtWriteRef = useSpring({
        config: config.stiff,
        to: {
            transform: `rotateZ(${stCommentMode? 0:180}deg) scale(${stCommentMode? 1:0})`,
            opacity: stCommentMode? 1:0,
            display: stCommentMode? 'flex':'none'

        },
        from: {
            transform: `rotateZ(${stCommentMode? 180:0}deg)  scale(${stCommentMode? 0:1})`,
            opacity: stCommentMode? 0:1,
            display: stCommentMode? 'none':'flex'

        }
    })


    const article = props.article

    return (
        <div className="fun-right-content">
            <div className="fun-right-content--desktop">
                <MDBBtn color={LikeButtonColor} onClick={onClickLike} className="fun-button fun-button--like">
                    <span className="like-count">
                        {stLikeCount}
                    </span>
                    <MDBIcon far icon="arrow-alt-circle-up" className={changeLikeThumbsColor()} />
                </MDBBtn>
                <MDBBtn color={LikeButtonColor} onClick={onClickDislike} className="fun-button fun-button--like">
                    <span className="like-count">
                        {stDislikeCount}
                    </span>
                    <MDBIcon far icon="arrow-alt-circle-down" className={changeDisLikeThumbsColor()} />
                </MDBBtn>
                <Link to={`/articles/${props.article.uuid}`} style={{ textDecoration: 'none' }}>
                    <MDBBtn className="fun-button fun-button--comment"  onClick={
                        () => setStCommentMode(!stCommentMode)
                    }>
                        <animated.div className="cmt-count-wrapper" style={spgCmtCountRef}>
                            <span className="cmt-count">{
                                article.comment_count
                            }</span>
                            <MDBIcon far icon="comment-dots" />
                        </animated.div>
                        <animated.div className="cmt-count-wrapper" style={spgCmtWriteRef}>
                            <MDBIcon icon="pencil-alt" />
                        </animated.div>
                    </MDBBtn>
                </Link>
                &nbsp;
                <FacebookShareButton url={props.sharedLink}
                                            children={
                                                <MDBBtn color="primary" className="fun-button">                    
                                                <MDBIcon fab icon="facebook-f" />                    
                                            </MDBBtn>
                                            }>                    
                </FacebookShareButton>
                <TwitterShareButton url={props.sharedLink}
                    title={article.header}
                    via="Funniq"
                    // hashtags={["Hashtags", "123213","asd"]}
                    children={<MDBBtn color="info" className="fun-button">
                    <MDBIcon fab icon="twitter" />
                </MDBBtn>                    
                }>                    
                </TwitterShareButton>
                {/* <a href="https://www.facebook.com/sharer/sharer.php?u=funniq.com/articles/664-liverpool-champions-premier-league-2019-2020-after-30-years-ynwa" target="_blank">
                    Share on Facebook
                </a> */}
                {/* <MDBBtn color="danger" className="fun-button">
                    <MDBIcon fab icon="pinterest-p" />
                </MDBBtn> */}
            </div>

            {/*<div className="fun-right-content--mobile">*/}
            {/*    <div className="btn-bottom" onClick={handleLikeClick}>*/}
            {/*        <span className="like-count">*/}
            {/*            {article.like_count}*/}
            {/*        </span>*/}
            {/*        &nbsp;*/}
            {/*        &nbsp;*/}
            {/*        <MDBIcon far icon="arrow-alt-circle-up" className={changeDisLikeThumbsColor()}/>*/}
            {/*    </div>*/}
            {/*    <div className="btn-bottom" onClick={handleDislikeClick}>*/}
            {/*        <span className="dislike-count">*/}
            {/*            {article.dislike_count}*/}
            {/*        </span>*/}
            {/*        &nbsp;*/}
            {/*        &nbsp;*/}
            {/*        <MDBIcon far icon="arrow-alt-circle-down" className={changeDisLikeThumbsColor()} />*/}
            {/*    </div>*/}
            {/*    <div className="btn-bottom">*/}
            {/*        <span className="dislike-count">*/}
            {/*            {article.comment_count}*/}
            {/*        </span>*/}
            {/*        &nbsp;*/}
            {/*        &nbsp;*/}
            {/*        <MDBIcon far icon="comment-dots" />*/}
            {/*    </div>*/}
            {/*    <div className="btn-bottom">*/}
            {/*        <MDBIcon icon="share-alt" />*/}
            {/*    </div>                */}
            {/*</div>*/}
        </div>
    );
};

FunRightContent.propTypes = {
    // article: Fun.propTypes.article
    sharedLink: PropTypes.string,
    article: PropTypes.any
};

export default FunRightContent;