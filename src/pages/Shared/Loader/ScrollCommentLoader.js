import ContentLoader from "react-content-loader";
import React from 'react';

const ScrollCommentLoader = () => (
    
    <div style={{
        padding: '0 15px'
    }}>
    <ContentLoader
        height={5}
        width={10}
        speed={2}
        primaryColor="var(--color-ivory)"
        secondaryColor="#ecebeb"
    >
        <rect x="2" y="2" rx="4" ry="4" width="50" height="21" />
        <rect x="2" y="29" rx="4" ry="4" width="50" height="13" />
        <rect x="2" y="53" rx="5" ry="5" width="50" height="25" />
        <rect x="2" y="56" rx="4" ry="4" width="50" height="20" />
        <rect x="2" y="53" rx="4" ry="4" width="50" height="50" />
    </ContentLoader>
    </div>
)

export default ScrollCommentLoader;