import React from 'react';
// import PropTypes from 'prop-types';
import './RightStickyNav.sass'
// import { MDBRow,MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";
import PopularNav from '../PopularNav/PopularNav'
import SocialButtons from '../../../components/SocialButtons/SocialButtons'
import UserRankings from '../../../components/UserRankings/UserRankings'
import MobileComingSoon from '../../../components/MobileComingSoon/MobileComingSoon'

const RightStickyNav = props => {

    const renderConnectWithUs = () => {
        return (<div className='connect_us'>
            <div className='conn_text'>
                Connect with us
            </div>
        </div>)
    }

    return (
        <div className="right-sticky-nav">            
            <PopularNav/>
            <div className="user_rankings_parent">
                <UserRankings />    
            </div>
            <div className="mobile_coming_parent">
                <MobileComingSoon />
            </div>            
            {renderConnectWithUs()}
            <div className="social_nav">
                <SocialButtons />
            </div>
                     
        </div>
    );
};

RightStickyNav.propTypes = {

};

export default RightStickyNav;