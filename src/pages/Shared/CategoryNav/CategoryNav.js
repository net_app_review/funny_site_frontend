import React, {useEffect, useState} from 'react';
import './CategoryNav.sass'
import {  MDBNavLink,MDBIcon } from "mdbreact";
import {categoriesService} from "../../../services/categoriesService";
import { useTranslation } from "react-i18next";
import MultiLanguageBtn from '../../../components/MultiLanguageBtn/MultiLanguageBtn';

const CategoryNav = props => {

    const [stCate, setStCate] = useState([]);
    const [stCollapse, setstCollapse] = useState('');
    const { t,  } = useTranslation();
    useEffect(() => {
        categoriesService.getCategories()
            .then(cates => {
                setStCate(cates)
            })
    }, [])

    const collapseCategory = () => {
        if (stCollapse === 'collapse_d') {
            setstCollapse('')
        } else {
            setstCollapse('collapse_d')
        }
    }

    return (
        <div className={`category-nav ${stCollapse}`}>
            <div className={`d-flex flex-column category_list`}>
                <div className="category_title">
                    {t("home.categories")}
                </div>                             
                {stCate.map(cate => {
                    return (
                        <MDBNavLink to={`/categories/${cate.id}-${cate.slug}`} key={cate.id} className="m-0 p-0">                                    
                            <div className="d-flex flex-row category_stats" key={cate.id}>                                    
                                <div className="flex-shrink-1 bd-highlight category_icon" >                                        
                                    <MDBIcon icon={cate.icon}/>                                        
                                </div>
                                <div className="category_name">
                                    {cate.name}                                    
                                </div>                                    
                            </div>
                        </MDBNavLink>
                    )
                })}                               
            </div>
            <div className='MultiLanguage_Btn'>
                <MultiLanguageBtn collapseCategory={collapseCategory}/>
            </div>
        </div>
    );
};

CategoryNav.propTypes = {

};

export default CategoryNav;