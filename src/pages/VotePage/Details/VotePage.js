import React, {useState, useEffect, useContext} from 'react';
import {  MDBCol, MDBRow  } from "mdbreact";
import "./VotePage.sass";
import {getVoteFightService} from '../../../services/getVoteFightService'
import { LoggedInContext, SignInModalContext} from '../../../Store'
// import {LikeType} from "../../../model/LikeTypeEnum";
import {voteService} from '../../../services/voteService'
import CommentForm from '../../../components/Comments/CommentForm'
import CommentList from '../../../components/Comments/CommentList'
// import TimeCountdown from '../../../components/TimeCountdown/TimeCountdown';
import ModalVoteCandidateStats from '../../../components/ModalVoteCandidateStats/ModalVoteCandidateStats'
// import swal from 'sweetalert'
import { MoonLoader } from 'react-spinners';
import LoginMoonLoaderCss from '../../../config/ReactLoader/LoginMoonLoaderCss'
import {fetchCommentService} from '../../../services/fetchCommentService'

// const ReactMarkdown = require('react-markdown')


const VotePage = (props) => {

    const [stVoteFight, setstVoteFight] = useState({});    
    // const [stVoteStatus, setstVoteStatus] = useState("");
    const [stLoggedIn, ] = useContext(LoggedInContext)
    const [, setSignInModal] = useContext(SignInModalContext)
    // const [stRemainingTime, setstRemainingTime] = useState(0)
    const [stIsModalCandidateInfoOpen, setstIsModalCandidateInfoOpen] = useState(false);
    const [stCandidateInfo, setstCandidateInfo] = useState("");
    // const [stUpvoteLoading, setstUpvoteLoading] = useState(false);
    const [stLoadMoreComment, setstLoadMoreComment] = useState(false);

    // const [stCurrentUser, setstCurrentUser] = useContext(CurrentUserContext);
    const [stComments, setstComments] = useState([])
    const [stPage, setStPage] = useState(1);
    const [stMaxPage, setstMaxPage] = useState(1);
    const [stFirstThreePlace, setstFirstThreePlace] = useState([]);    
    const [stRemainingVoteCandidates, setstRemainingVoteCandidates] = useState([]);

    useEffect(() => {                
        fetchVotefight()
        window.scrollTo(0, 0)
    }, []);

    useEffect(() => {                
        fetchVotefight()        
    }, [stLoggedIn]);

    useEffect(() => {        
        fetchComment()
    }, [stPage]);

    const fetchVotefight = () => {
        getVoteFightService.getVoteFight(props.match.params.id)
        .then(result => {             
            setstVoteFight (result.data)

            setstFirstThreePlace (result.data.vote_candidates.slice(0,3))

            setstRemainingVoteCandidates (result.data.vote_candidates.slice(3, result.data.vote_candidates.length))
                        
        })
    }    

    const VoteButtonColor = (candidate) => {
        if (candidate.vote_status !== "not_authenticated"){
            if (candidate.vote_status.liketype !== "no_action"){
                if (candidate.vote_status.liketype === "upvote"){
                    return (candidate.color)
                } else if (candidate.vote_status.liketype === "no_value") {
                    return "grey"
                } else {
                    return "grey"
                }           
            } else {
                return (candidate.color)
            }
        } else {
            return (candidate.color)
        }
    }

    const VoteColor = (candidate) => {
        if (candidate.vote_status !== "not_authenticated"){
            if (candidate.vote_status.liketype !== "no_action"){
                if (candidate.vote_status.liketype === "upvote"){
                    return (candidate.color)
                } else if (candidate.vote_status.liketype === "no_value") {
                    return "grey"
                } else {
                    return "grey"
                }           
            } else {
                return (candidate.color)
            }
        } else {
            return (candidate.color)
        }
        
    }

    const CandidateGrayscale = (candidate) => {
        if (candidate.vote_status !== "not_authenticated"){
            if (candidate.vote_status.liketype !== "no_action"){
                if (candidate.vote_status.liketype !== "upvote") {
                    return "grayscale(100%)"
                } else {
                    return "grayscale(0%)"
                }            
            }     
        } else {
            return "grayscale(0%)"
        }
    }

    // function calculateTotalVote () {
    //     return stLeftOpponentVoteCount + stRightOpponentVoteCount
    // }
    

    // const calCandidatePercent = (candidate_vote_count) => {
    //     return (candidate_vote_count * 100 / stVoteFight.total_vote_count).toFixed(2)         
    // }

    // function calrightPercent () {        
    //     return (100 - calLeftPercent()).toFixed(2)
    // }        

    const isLogin = () => {
        return stLoggedIn === "LOGGED_IN"
    }

    const openLoginModal = () => {
        setSignInModal(true)        
    }

    function removeElementFromArrayMatchName(arr, what) {
        var what, a = arguments, L = a.length, ax;
        while (L > 1 && arr.length) {
            what = a[--L];
            while ((ax= arr.indexOf(what)) !== -1) {
                arr.splice(ax, 1);
            }
        }        
        return arr;
    }
    
    const decideVoteAction = (action) => {
        if (action === "no_value") {
            return "devoted"
        } else if (action === "upvote"){
            return "voted"
        }
    }

    const handleVoteServer = (candidate) => {                
        if (candidate.vote_status.liketype === "no_action") {                            
            voteService.handle_vote(candidate.id, "no_action", "")            
            .then(result =>  {
                    let RemainingVoteCandidates = removeElementFromArrayMatchName(stRemainingVoteCandidates, candidate);                    

                    for (let i = 0; i < RemainingVoteCandidates.length; i++) {
                        voteService.handle_vote(RemainingVoteCandidates[i].id, 'dis_vote', RemainingVoteCandidates[i].vote_status.id).then(result => {fetchVotefight()}) ;
                    }                
                    // swal(decideVoteAction(result.liketype), "You have " + decideVoteAction(result.liketype) + " for " + `${candidate.name}`, "success");                    
                }

            )
        } else {
            voteService.handle_vote(candidate.id, candidate.vote_status.liketype, candidate.vote_status.id).then(result => {
                fetchVotefight()                
                // swal(decideVoteAction(result.liketype), "You have " + decideVoteAction(result.liketype) + " for " + `${candidate.name}`, "success");                
            })                 
        }        
    }

    const onClickUpVote = (candidate) => {        
        if (!isLogin()) {
            openLoginModal()
            return
        }
        // var ary = ['three', 'seven', 'eleven'];        
        // setstUpvoteLoading(true)
        handleVoteServer(candidate)
        // setstUpvoteLoading(true)
        
    }

    // const remainingTime = () => {
    //     if (stRemainingTime > 0 && stRemainingTime !== undefined) {
    //         return (<TimeCountdown remainingTime={stRemainingTime}/>)
    //     }
    // }

    const userVotedCandidates = () => {

        let string = ""

        let new_arr =[]

        for (var i = 0; i < stRemainingVoteCandidates.length ; i ++){
            if (stRemainingVoteCandidates[i].vote_status){
                if (stRemainingVoteCandidates[i].vote_status.liketype === "upvote"){
                    new_arr.push(stRemainingVoteCandidates[i].name )                    
                }
            }            
        }
        
        if (new_arr.length === 0) {
            return (
                <div>  
                    <strong>
                        You have not voted yet
                    </strong>
                       &nbsp;                                      
                </div>
            )
        } else if (new_arr.length === 1) {
            return (
                <div >    
                    <strong>
                        You voted for: 
                    </strong>
                    &nbsp;
                    {new_arr[0]}
                </div>
            )
        } else {            

            for ( i = 0; i < new_arr.length ; i ++){
                string = string + " and " +new_arr[i]
            }
            return (
                <div >
                    <strong>
                        You voted for: 
                    </strong>                    
                    &nbsp;
                    {remove_first_occurrence(string, "and")}
                </div>
            )
        }        
        
    }

    const remove_first_occurrence = (str, searchstr) => {
        var index = str.indexOf(searchstr);
        if (index === -1) {
            return str;
        }
        return str.slice(0, index) + str.slice(index + searchstr.length);
    }
    
    const toggleModalCandidateInfo = (candidate_info) => {
        // e.preventde
        setstIsModalCandidateInfoOpen(!stIsModalCandidateInfoOpen)
        setstCandidateInfo(candidate_info)
    }
    
    const fetchComment = () => {
        setstLoadMoreComment(true)
        fetchCommentService.fetchComment("votefight", props.match.params.id,stPage)
            .then(result => {
                setstMaxPage(parseInt(result.totalPage))                
                setstComments([...stComments, ...result.data])
                setstLoadMoreComment(false)
            })
    }

    const fetchNewComment = (new_comment) => {                
        setstComments([new_comment, ...stComments])
    }

    const buttonVoteStatus = (liketype) => {
        if (liketype === "upvote"){
            return "VOTED"
        } else if (liketype === "no_value") {
            return "UPVOTE"
        } else {
            return "UPVOTE"
        }
    }

    const renderLoadMoreButton = () => {
        if (stPage < stMaxPage){
            return <button onClick={_handleLoadMoreClick} className="load_more_btn btn">
                        Load more comments
                        <MoonLoader          
                            css={LoginMoonLoaderCss}            
                            size={20}
                            color={'black'}
                            loading={stLoadMoreComment}                        
                            />
                    </button>
        }
    }
    

    const _handleLoadMoreClick = () => {                
        setStPage(stPage + 1)        
    }

    const deleteComment = (comment_id) => {
        let i = 0
        let array = []
        array = stComments
        for ( i =0; i< array.length; i ++){        
            if (array[i]["id"] === comment_id) {                                
                array.splice(i, 1);                
                setstComments([...array])
            }
        }                
        // return array
    }

    const renderPositionBadge = (position) => {
        if (position === 1){
            return <div className="badge first_position">1<sub className="pl-1">st</sub></div>
        } else if (position === 2){
            return <div className="badge second_position">2<sub className="pl-1">nd</sub></div>
        } else if (position === 3){
            return <div className="badge third_position"> 3<sub className="pl-1">rd</sub></div>
        }
    }

    return (
       <div className="vote_page_container">                        
            <ModalVoteCandidateStats isOpen={stIsModalCandidateInfoOpen}
            toggle={toggleModalCandidateInfo} candidate_info={stCandidateInfo}/>
           <div className="d-flex pt-2 vote_slogan justify-content-center">
               {stVoteFight.slogan}
           </div>
           <div className="pc_vote_fight_component">    
                <div className="d-flex pt-2 flex-row vote_fight_component">
                    {stFirstThreePlace.map((candidate, index) => {
                        return (
                        <div className="flex-fill d-flex flex-column opponent_component" key={index}>                                    
                            <div className="p-0 flex-fill d-flex flex-column opponent_avatar" style={{ backgroundColor: `white`}}>                                        
                                <div className="opponent_image">
                                    <img src={candidate.image_url} style={{WebkitFilter: `${CandidateGrayscale(candidate)}`}} 
                                                                    alt="Hillary Clinton" className="img-fluid"/>
                                    <div className="opponent_image_overlay">
                                        <button type="button" className="btn candidate_profile_btn"  onClick={(e) => {toggleModalCandidateInfo(candidate.profile)}}>
                                            View stats
                                        </button>
                                    </div>
                                    <div className="rank_overlay">
                                        {
                                            renderPositionBadge(index + 1)
                                        }
                                    </div>
                                </div>
                                

                                    <div className="opponent_name"> 
                                        {candidate.name}
                                    </div>                                        
                                    <button className="btn upvote_button" onClick={() => onClickUpVote(candidate)}>                                            
                                        {
                                            buttonVoteStatus(candidate.vote_status.liketype)
                                        }
                                        &nbsp;                                            
                                    </button>
                                    <div className="p-0 flex-fill opponent_stat">
                                        Vote count: {candidate.vote_count}
                                    </div>
                                </div>                                                                        
                            </div>
                        )
                    })}  
                </div>                                                 
           </div>     
                <div className="d-flex pt-2 flex-column remaining_candidates">
                    {stRemainingVoteCandidates.map((candidate, index) => {
                        return (                            
                            <MDBRow className="candidate">
                                <MDBCol size="1" className="rank">                                                           
                                    {index + 4}           
                                </MDBCol>
                                <MDBCol size="1" className="vote_btn ">
                                    <button className="btn" onClick={() => onClickUpVote(candidate)}>
                                        Upvote    
                                    </button>                                        
                                </MDBCol>
                                <MDBCol size="2" className="image">
                                    <img src={candidate.image_url} className="img ">

                                    </img>                                        
                                </MDBCol>  
                                <MDBCol size="6" className="name">
                                    {candidate.name}
                                </MDBCol>   
                                <MDBCol size="2" className="vote_count">
                                    Vote count:
                                    &nbsp;
                                    {candidate.vote_count}
                                </MDBCol>              
                            </MDBRow>                                                 
                        )
                    })}                                
                </div>    
                       
           {/* {
               votedCandidate()
           } */}
            

           {/* <div className="d-flex pt-2 flex-row vote_percentage_component">                
                <div className="progress">
                    {stVoteCandidates.map((candidate, index) => {
                    return (
                        <div className="progress-bar" role="progressbar"  style={{width: `${calCandidatePercent(candidate.vote_count)}%`, backgroundColor: `${candidate.color}`}} aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" key={index}> 
                            {calCandidatePercent(candidate.vote_count)}%

                        </div>
                        
                        )
                    })}
                </div>
           </div> */}
           {/* <div className="voted_candidates">
                {userVotedCandidates ()}
            </div> */}
           {/* <p className="count_down" >
               <strong>Remaining time:</strong>
                &nbsp;
                {remainingTime()}
           </p> */}

           {/* <div className="d-flex flex-row justify-content-center stats_info">               
                <MDBTable responsive >
                    <ReactMarkdown source={stVoteFight.criteria} />    
                </MDBTable>            
           </div> */}
           
            <div className="comment_section">
                    <CommentForm obj = "votefight"
                                id={props.match.params.id}
                                onSubmit={fetchNewComment}													                
                        />
                    <CommentList 
                        comments = {stComments}                        
                        deletecomment={deleteComment}
                        />
                    {renderLoadMoreButton()}
            </div>           
       </div>
    );
};


export default VotePage;