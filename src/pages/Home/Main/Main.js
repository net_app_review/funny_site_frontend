import React from 'react';

import './Main.sass'
import { MDBContainer} from "mdbreact";
import HomeDescription from '../HomeDescription/HomeDescription'
import FunZone from "../FunZone/FunZone";


const Main = props => {

    // const [stFeaturedTags, setstFeaturedTags] = useState([]);
    

    return (
        <MDBContainer className="main_home">                                                                                                                              
            <HomeDescription />                                        
            <FunZone/>                                                                    
        </MDBContainer>
    );
};

Main.propTypes = {

};

export default Main;
