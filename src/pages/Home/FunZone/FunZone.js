import React, {useEffect, useState, useContext} from 'react';
// import PropTypes from 'prop-types';
import './FunZone.sass'
import {LoggedInContext, CurrentUserContext} from '../../../Store'
import {articlesService} from "../../../services/articlesService";
import Fun from "../../Shared/Fun/Fun"
import ContentLoader from "react-content-loader";
import {removeHidingArticles} from "../../../functions/removeHidingArticles"

const SCROLL_OFFSET = 1250

const FunZone = props => {

    const [stPage, setStPage] = useState(1);
    const [stArticles, setstArticles] = useState([]);    
    const [stIsBottom, setStIsBottom] = useState(false);
    const [LoggedIn, ] = useContext(LoggedInContext);    

    useEffect(() => {     
        fetchArticles()
    }, [stPage]);

    useEffect(() => {        
        if (LoggedIn === "LOGGED_IN"){
            setstArticles([])            
        }        
    }, [LoggedIn]);

    useEffect(() => {        
        if (LoggedIn === "LOGGED_IN" && stArticles.length === 0){            
            fetchArticles()            
        }        
    }, [LoggedIn, stArticles]);

    const fetchArticles = () => {
        articlesService.getArticles(stPage)
            .then(result => {                            
                setstArticles([...stArticles, ...removeHidingArticles.removeHidedArticle(result.data,"hide")])
            })
    }

    useEffect(() => {        
        const onScroll = () => {
            if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - SCROLL_OFFSET) {
                if (!stIsBottom) {
                    setStIsBottom(true)                    
                    setStPage(stPage+1)                    
                }
            } else {
                setStIsBottom(false)
            }
        }

        window.addEventListener('scroll', onScroll)

        return () => {
            window.removeEventListener('scroll', onScroll)
        };
    }, [stIsBottom, stPage])



    return (
        <div className="fun-zone">
            {stArticles.length > 0 && stArticles.map((article, index) => {
                return (
                    <Fun key={index} article={article}/>
                )
            })}
            {stArticles.length >= 5  && <DefaultFun/>}

        </div>
    );
};


FunZone.propTypes = {};

const DefaultFun = () => (
    <div style={{
        padding: '0 15px'
    }}>
    <ContentLoader
        height={475}
        width={500}
        speed={2}
        primaryColor="var(--color-ivory)"
        secondaryColor="#ecebeb"
    >
        <rect x="2" y="2" rx="4" ry="4" width="314" height="21" />
        <rect x="2" y="29" rx="4" ry="4" width="264" height="13" />
        <rect x="2" y="53" rx="5" ry="5" width="420" height="408" />
        <rect x="446" y="56" rx="4" ry="4" width="50" height="202" />
        <rect x="445" y="411" rx="4" ry="4" width="50" height="50" />
    </ContentLoader>
    </div>
)


export default FunZone;