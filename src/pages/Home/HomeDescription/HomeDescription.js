import React, { useState} from 'react';

import {MDBCol, MDBContainer, MDBRow} from "mdbreact";
import FsLightbox from 'fslightbox-react';
import { useTranslation } from "react-i18next";

const HomeDescription = props => {
    
    const [stImgLightBox, setstImgLightBox] = useState(false);
    const { t, } = useTranslation();

    return (
        <MDBContainer className="d-none d-lg-block p-0 pt-lg-2">
            <MDBRow >
                <MDBCol size="2" className="pr-0">
                    <FsLightbox toggler={stImgLightBox}
                        customSources={ [ 
                            <img className="image img-fluid"
                                alt=""
                                src="https://firebasestorage.googleapis.com/v0/b/funniq-78dbd.appspot.com/o/ezgif-6-dd8a8d44d590%5B1%5D.gif?alt=media&token=dedc28fe-c107-4b25-b883-d2c60e7deec6"
                                onClick={ () => setstImgLightBox(!stImgLightBox) }/>
                            ] } 
                            customSourcesGlobalMaxDimensions={ 
                                { width: 600, height: 700 } 
                            } 
                            customSourcesMaxDimensions={ 
                                [null, null, { width: 300, height: 50 }] 
                            } 
                        />
                    <img className="img-fluid rounded" 
                        src="https://firebasestorage.googleapis.com/v0/b/funniq-78dbd.appspot.com/o/ezgif-6-dd8a8d44d590%5B1%5D.gif?alt=media&token=dedc28fe-c107-4b25-b883-d2c60e7deec6" 
                        alt=""
                        onClick={ () => setstImgLightBox(!stImgLightBox)} />
                </MDBCol>
                <MDBCol size="10" >
                    <MDBRow className="m-0"> 
                        <h3>
                            <strong>
                            {t("home.title")}
                            </strong>                            
                        </h3>                        
                    </MDBRow>
                    <MDBRow  className="m-0">
                        <h4>
                        {t("home.slogan")}
                        </h4>
                    </MDBRow>
                </MDBCol>                
            </MDBRow> 
        </MDBContainer>
    );
};


HomeDescription.propTypes = {};

export default HomeDescription;