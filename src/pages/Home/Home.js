import React, {useState, useEffect} from 'react';
// import {Link} from 'react-router-dom'
import './Home.sass'
// import FunZone from "./FunZone/FunZone";
import { MDBContainer, MDBCol } from "mdbreact";
import LeftStickyNav from '../Shared/LeftStickyNav/LeftStickyNav'
import RightStickyNav from '../Shared/RightStickyNav/RightStickyNav'
// import HomeDescription from './HomeDescription/HomeDescription'
import FeaturedTag from '../Shared/FeaturedTag/FeaturedTag'
import {featuredTagsService} from '../../services/featuredTagsService'
import SEO from '../../services/seoService'
// import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
// import { useTranslation } from "react-i18next";
// import Dropdown from "react-bootstrap/Dropdown";
import Main from './Main/Main'
// import RankingPage from '../RankingPage/RankingPage'

const Home = props => {

    const [stFeaturedTags, setstFeaturedTags] = useState([]);
    // const { t, i18n } = useTranslation();
    useEffect(() => {        
        featuredTagsService.getFeaturedTags()
            .then(tags => {
                setstFeaturedTags(tags)
            })
    }, []);

    return (
        <MDBContainer className="home">
            <SEO pageProps={{ 
						title: "Funniq is your source for the best & latest cosplay content",
						type: "website",
						description: "Funniq is your source for the best & latest cosplay content",
						image: "https://firebasestorage.googleapis.com/v0/b/funniq-78dbd.appspot.com/o/funniq_production%2Fu29-16-1597830086578?alt=media&token=ba8c6b70-2509-4332-ba36-35e2e9198df4", 
						url:"https://funniq.com"
					}} />            
            {/* <p class="h-card">
                <img class="u-photo" src="https://i.ibb.co/VtQ15PN/image.png" alt="" />
                <a class="p-name u-url" href="https://funniq.com">Funniq cosplay content</a>
                <a class="u-email" href="mailto:funniqpage@gmail.com">funniqpage@gmail.com</a>, 
                <span class="p-street-address">17 Austerstræti</span>
                <span class="p-locality">Reykjavík</span>
                <span class="p-country-name">USA</span>
            </p> */}
            <div className="d-none">
                funny cosplay contents, funny videos, mama, animals funny videos, 
                funny cosplay contents dirty, funny cosplay contents pictures, new cosplay contents, cosplay contents funny love,
                funny adult cosplay contents, funny video baby, funny website, funny friendship cosplay contents,
                american funny videos, video cosplay contents, list of cosplay contents, apps for video calling
                ifunny cosplay contents, funny new year cosplay contents, free cosplay contents, cosplay contents website, funny video cosplay contents
                world funny videos, new funny cosplay contents, latest cosplay contents
            </div>
            <a className="h-card d-none" href="https://funniq.com">Funniq cosplay content</a>
           
           
            <FeaturedTag featureTagsList = {stFeaturedTags} />
            <div className="d-flex flex-row pt-2">
                <MDBCol xs={4} sm={0} md={3} lg={3} xl={3}  className="d-none d-lg-block pl-0">
                    <LeftStickyNav/>
                </MDBCol>
                <div  className="pl-2 pr-2 p-lg-0 pt-2 mr-lg-1 flex-grow-1 bd-highlight">                    
                    {/* <Route path='/' exact component={Main}/> */}
                    <main>
                        <Main />
                    </main>                    
                    {/* <HomeDescription />                                        
                    <FunZone/> */}                    
                </div>                
                <div>
                    
                </div>
                <MDBCol xs={4} sm={0} md={3} lg={3} xl={3}  className="d-none d-lg-block pl-0 pr-0">
                    <RightStickyNav/>                    
                </MDBCol>
            </div>
        </MDBContainer>
    );
};

Home.propTypes = {

};

export default Home;
