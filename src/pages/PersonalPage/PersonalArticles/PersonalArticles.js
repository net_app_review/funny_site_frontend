import React, {useEffect, useState} from 'react';
import './PersonalArticles.sass'
import Fun from "../../Shared/Fun/Fun"
import ScrollImageLoader from "../../Shared/ScrollImageLoader";
import {personalPageService} from "../../../services/personalPageService"

const SCROLL_OFFSET = 50

const PersonalArticles = props => {

    const [stPage, setStPage] = useState(1);
    const [stArticles, setStArticles] = useState([]);    
    const [stIsBottom, setStIsBottom] = useState(false);
    // const [LoggedIn, setLoggedIn] = useContext(LoggedInContext);

    useEffect(() => {        
        personalPageService.getArticles(props.user_id,stPage)
            .then(result => {                
                setStArticles([...stArticles, ...result.data])
            })
    }, [stPage]);


    useEffect(() => {        
        const onScroll = () => {
            if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - SCROLL_OFFSET) {
                if (!stIsBottom) {
                    setStIsBottom(true)                    
                    setStPage(stPage+1)                    
                }
            } else {
                setStIsBottom(false)
            }
        }

        window.addEventListener('scroll', onScroll)

        return () => {
            window.removeEventListener('scroll', onScroll)
        };
    }, [stIsBottom, stPage])


    return (
        <div className="fun-zone">
            {stArticles.length > 0 && stArticles.map(article => {
                return (
                    <Fun key={article.id} article={article}/>
                )
            })}
            {stArticles >=5 && <ScrollImageLoader/>}

        </div>
    );
};


PersonalArticles.propTypes = {};


export default PersonalArticles;