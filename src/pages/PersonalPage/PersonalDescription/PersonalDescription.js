import React, {useEffect, useState, useContext } from 'react';
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import "./PersonalDescription.module.sass"
import FsLightbox from 'fslightbox-react';
import styles from './PersonalDescription.module.sass'
import Table from 'react-bootstrap/Table'
import UserLevelBadge from '../../../components/UserLevelBadge/UserLevelBadge'
import UserNationalFlag from '../../../components/UserNationalFlag/UserNationalFlag'
import {subscriptionService} from '../../../services/subscriptionService'
import {personalPageService} from '../../../services/personalPageService'
import { MoonLoader } from 'react-spinners'; 
import LoginMoonLoaderCss from '../../../config/ReactLoader/LoginMoonLoaderCss'
import {LoggedInContext, SignInModalContext, CurrentUserContext} from '../../../Store'
import swal from 'sweetalert'
import {notificationService} from "../../../services/notificationService"

const PersonalDescription = props => {

    const [stUserInfo, setstUserInfo] = useState({});
    const [stCurrentUser, ] = useContext(CurrentUserContext)
    const [stMoonLoading, setstMoonLoading] = useState(false);
    const [stUserStatDropdown, setstUserStatDropdown] = useState("0px")
    const [stImgLightBox, setstImgLightBox] = useState(false);
    const [stSubscribed, setstSubscribed] = useState('');
    const [stLoggedIn, ] = useContext(LoggedInContext)
    const [, setSignInModal] = useContext(SignInModalContext)

    useEffect(() => {		
        fetchUserInfo()	        
    },[])

    const isLogin = () => {
        return stLoggedIn === 'LOGGED_IN'
    }

    const switchDropdownStatus = () => {
        if (stUserStatDropdown === "0px"){
            setstUserStatDropdown("auto")
        } else {
            setstUserStatDropdown("0px")
        }
        // setstUserStatDropdown(!stUserStatDropdown)
    }

	const fetchUserInfo = () => {
        setstMoonLoading(true)
        personalPageService.getUserInfo(props.user_id)
        .then(response => {
            setstUserInfo(response.data.user)
            setstSubscribed(response.data.subscribed)
            setstMoonLoading(false)
        })
        .catch(error => {
            setstMoonLoading(false)
            swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
        }
        
        )
    }    
    
    const UserImage = () =>  {
        if (stUserInfo.image) {
            return stUserInfo.image.url
        }
        return null
    }

    const renderSubscription = (user_info) => {        
        return (
            <div className={`${styles.subscriptions}`}>
                <div className={`${styles.subscribers}`}>
                    Subscribers
                    <span>
                        {user_info.subscribers_count}
                    </span>                    
                </div>
                <div className={`${styles.subscribe_to}`}>
                    Subscribing
                    <span>
                        {user_info.subscribeto_count}
                    </span>                    
                </div>
                <div className='d-none d-lg-block'>
                    {getSubscriptionStatus(user_info.id)}
                </div>                
            </div>
        )
    }

    const getSubscriptionStatus = (user_id) => {
        if (stSubscribed === 'active') {
            return (
                <div className={`${styles.subscribe_action} ${styles.subscribed}`} onClick={(e) => updateSubscription(e, user_id, stSubscribed)}>
                    <button className='btn'>
                        Subscribed
                        <MoonLoader          
                            css={LoginMoonLoaderCss}            
                            size={16}
                            color={'#fafafa'}
                            loading={stMoonLoading}                        
                        />
                    </button>                    
                </div>
            )
        } else if (stSubscribed === 'inactive') {
            return (
                <div className={`${styles.subscribe_action} ${styles.unsubscribed}`} onClick={(e) => updateSubscription(e, user_id, stSubscribed)}>
                    <button className='btn'>
                        Subscribe now
                        <MoonLoader          
                            css={LoginMoonLoaderCss}            
                            size={16}
                            color={'#318f31'}
                            loading={stMoonLoading}                        
                        />
                    </button>                    
                </div>
            )
        } else if (stSubscribed === 'no_action') {
            return (
                <div className={`${styles.subscribe_action} ${styles.unsubscribed}`} onClick={(e) => createSubscription(e, user_id)}>
                    <button className='btn'>
                        Subscribe now
                        <MoonLoader          
                            css={LoginMoonLoaderCss}            
                            size={16}
                            color={'#318f31'}
                            loading={stMoonLoading}                        
                        />
                    </button>                    
                </div>
            )
        } else if (stSubscribed === 'self') {
            return (
                <></>
            )
        }
    }

    const openLoginModal = () => {
        setSignInModal(true)
    }

    const createSubscription = (e, subscribe_to) => {
        e.preventDefault()
        if (!isLogin()) {
            openLoginModal()
            return
        }
        subscriptionService.createSubscription(subscribe_to)
            .then(result => {
                fetchUserInfo()
                createNotiAfterSubscribe()                
            })
            .catch(error => {                
                swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");                
            })        

    }

    // const deleteSubscription = (e, subscribe_to) => {
    //     e.preventDefault()
    //     subscriptionService.deleteSubscription(subscribe_to)
    //         .then(result => {                
    //             fetchUserInfo()                
    //         })
    //         .catch(error => {                
                
    //         })        

    // }

    const updateSubscription = (e, subscribe_to, status) => {
        e.preventDefault()
        subscriptionService.updateSubscription(subscribe_to, status)
            .then(result => {                
                fetchUserInfo()
            })
            .catch(error => {                
                swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
        })        

    }

    const createNotiAfterSubscribe = () => {
        notificationService.createNotification(
            stCurrentUser.uuid,
            stCurrentUser.name,
            stCurrentUser.image.url,
            '',
            'subscription',
            '',
            '',
            '',
            stUserInfo.uuid,
            `${stCurrentUser.name} has subscribed to your content`
        )      
    }

    return (        
        <MDBContainer className={`align-top ${styles.personal_description}`} >
            <MDBRow className={styles.user_info}>
                <MDBCol size="2" className={`pl-lg-0 ${styles.user_avatar}`}>
                    <FsLightbox toggler={stImgLightBox}
                        customSources={ [ 
                            <img className="image img-fluid"                                
                                src={UserImage()}
                                onClick={ () => setstImgLightBox(!stImgLightBox) }
                                alt={stUserInfo.name}/>
                            ] } 
                            customSourcesGlobalMaxDimensions={ 
                                { width: 600, height: 700 } 
                            } 
                            customSourcesMaxDimensions={ 
                                [null, null, { width: 300, height: 50 }] 
                            } 
                        />
                    <div className={`${styles.avatar_box}`}>
                        <img className={`img-fluid rounded ${styles.avatar_img}`} src={UserImage()} alt="" onClick={ () => setstImgLightBox(!stImgLightBox) }/>                        
                    </div>
                    
                </MDBCol>                
                <MDBCol size="10" className={`${styles.stats} text-right pl-2 pt-2 pl-sm-0`}>
                    <MDBRow className="pl-3 mb-1 d-flex flex-row mr-0">
                        <div className="ml-2 ">
                            <UserNationalFlag country_code={stUserInfo.country_code} height="19px"/>
                        </div>                        
                        <div className="ml-0 pt-1">
                            <UserLevelBadge level={stUserInfo.level} />
                        </div>
                    </MDBRow>
                    <MDBRow className="pl-3 mr-0">
                        <h3 className='pt-1 pr-2' style={{wordBreak: 'break-word'}}>
                            {stUserInfo.name}
                        </h3>
                        <div className={`${styles.mobile_subscribe} d-block d-lg-none pl-5 pb-2`}>
                            {getSubscriptionStatus(stUserInfo.id)}
                        </div>
                    </MDBRow>
                    <div className={``}>
                        {renderSubscription(stUserInfo)}
                    </div>                  
                </MDBCol>                
            </MDBRow>
            <MDBRow className={styles.user_stats}>
                <div className={`${styles.points_and_ranks} d-flex flex-row`}>
                    <div className={`${styles.ranks} flex-grow-1`}>
                        <strong>
                            Ranks:
                        </strong>
                        &nbsp;
                        #{stUserInfo.rank}
                    </div>
                    <div className={`${styles.points} flex-grow-1`}>
                        <strong>
                            Points: 
                        </strong>
                        &nbsp;
                        {stUserInfo.points}
                    </div>
                </div>
                <button className={styles.dropdown_button} onClick={switchDropdownStatus}>User stats</button>
                <div className={styles.dropdown_content} style={{ height: `${stUserStatDropdown}`}}>                    
                    <Table striped bordered hover responsive className={`${styles.stat_table} p-2`}>
                        {/* <thead>
                            <tr>                                
                                <th colSpan="2" className="stats_item" >Item</th>
                                <th>Stats</th>                            
                            </tr>
                        </thead> */}
                        <tbody>                            
                            <tr>                                
                                <td colSpan="2">Post views count</td>
                                <td>{stUserInfo.post_view_count}</td>                        
                            </tr>
                            <tr>                                
                                <td colSpan="2">Level</td>
                                <td>{stUserInfo.level}</td>                        
                            </tr>
                            {/* <tr>                                
                                <td colSpan="2">Comment count</td>
                                <td>{stUserInfo.comment_count}</td>                            
                            </tr> */}
                            {/* <tr>
                                <td>3</td>
                                <td colSpan="2">Larry the Bird</td>                            
                            </tr> */}
                        </tbody>
                    </Table>                    
                </div> 
            </MDBRow>
        </MDBContainer>
    );
};


PersonalDescription.propTypes = {};

export default PersonalDescription;