import React, {useEffect, useState} from 'react';
// import PropTypes from 'prop-types';
import './PersonalPage.sass'
import PersonalArticles from "./PersonalArticles/PersonalArticles";
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
// import CategoryNav from "../Shared/CategoryNav/CategoryNav";
// import PopularNav from "../Shared/PopularNav/PopularNav"
import LeftStickyNav from '../Shared/LeftStickyNav/LeftStickyNav'
import RightStickyNav from '../Shared/RightStickyNav/RightStickyNav'
import PersonalDescription from './PersonalDescription/PersonalDescription'
import SEO from "../../services/seoService";

const PersonalPage = props => {

    const [stSeo, setstSeo] = useState({})

    useEffect(() => {
        standardizeTitle()
        window.scrollTo(0, 0)
    }, []);    

    const standardizeTitle = () => {
        let final_string = props.location.pathname.replace("/users/", "")
        final_string = final_string.replace("-", "")
        final_string = final_string.replace(/[0-9]/g, '')              
        setstSeo({ title: final_string})
	}

    return (
        <MDBContainer className="home">
            <SEO pageProps={stSeo} />           
            <MDBRow className="content">
                <MDBCol sm={0} lg={3} md={3} xl={3} xs={4} className="hidden-xs pl-0">
                    <LeftStickyNav/>
                </MDBCol>
                <MDBCol sm={12} lg={6} md={6} xl={6} xs={7} key = {props.match.params.id} >
                    <PersonalDescription 
                        user_id = {props.match.params.id}/>
                    &nbsp;
                    &nbsp;
                    <PersonalArticles 
                    user_id={props.match.params.id}/>
                </MDBCol>
                <MDBCol sm={0} lg={3} md={3} xl={3} xs={4} className="hidden-xs pl-0 pr-0">
                    <RightStickyNav/>                    
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    );
};

PersonalPage.propTypes = {

};

export default PersonalPage;
