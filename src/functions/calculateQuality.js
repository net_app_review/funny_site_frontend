const calcQuality = (percent) => {    
    if (0 <= percent && percent <= 30) {
        return 'bad'
    } else if ( 30 < percent && percent <= 70 ) {
        return 'normal'
    } else if ( 70 < percent && percent <= 100 ) {
        return 'good'
    }    
}

export const calculateQuality = {
    calcQuality
}