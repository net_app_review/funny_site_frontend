const getTheDate = (offset) => {
    var today = new Date();
    today.setDate(1);
    today.setMonth(today.getMonth()-1);
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = dd + '/' + mm + '/' + yyyy;
    return today
}

export const getDate = {
    getTheDate
}