const calcPercent = (like_count, total_like_count) => {
    let percent = 0

    if (like_count > 0){
      percent = like_count/total_like_count * 100
      percent = percent.toFixed(0)
    }

    return percent
}

export const calculateLikePercent = {
    calcPercent
}