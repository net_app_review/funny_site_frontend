const handleLike = (input) => {

    let output = ''

    if (input === 'like') {
        output = 'no_value'
    } else if (input === 'dislike') {
        output = 'like'
    } else if (input === 'no_value'){
        output = 'like'
    } else if (input === 'no_action'){
        output = 'like'
    }

    return output
}

const handleDislike = (input) => {

    let output = ''

    if (input === 'like') {
        output = 'dislike'
    } else if (input === 'dislike') {
        output = 'no_value'
    } else if (input === 'no_value'){
        output = 'dislike'
    } else if (input === 'no_action'){
        output = 'dislike'
    }

    return output
}

export const handleLikeFunction = {
    handleLike,
    handleDislike
}