const elapsedTime = (time) => {
    var elapsed_time = time
        if (elapsed_time <= 60) {
            return 'just now'
        } else if ( 60 < elapsed_time && elapsed_time <= 3600 ) {
            return `${(elapsed_time / 60).toFixed(0)} minutes`
        } else if ( 3600 < elapsed_time && elapsed_time <= 86400 ) {
            return `${(elapsed_time / 3600).toFixed(0)} hours`
        } else if ( 86400 < elapsed_time && elapsed_time <= 604800) {
            return `${(elapsed_time / 86400).toFixed(0)} days`
        } else if ( 604800 < elapsed_time && elapsed_time <= 2592000) {
            return `${(elapsed_time / 604800).toFixed(0)} weeks`
        } else if ( 2592000 < elapsed_time && elapsed_time <= 31536000) {
            return `${(elapsed_time / 2592000).toFixed(0)} months`
        } else if ( 31536000 < elapsed_time && elapsed_time <= 63072000) {
            return `${(elapsed_time / 31536000).toFixed(0)} year`
        } else {
            return (elapsed_time/31536000).toFixed(0) + ' years'
        }    
}

export const calculateElapsedTime = {
    elapsedTime
}