const removeHidedArticle = (array, status) => {
    let i
    for ( i = 0; i< array.length; i ++){        
        if (array[i]["like_status"]["liketype"] === status) {            
            array.splice(i, 1);            
        }
    }    
    
    if (array[0] !== undefined && array[0]["like_status"]["liketype"] === status) {                                
        array.shift();
    }
    
    return array
}

export const removeHidingArticles = {
    removeHidedArticle
}