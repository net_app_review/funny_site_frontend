const getImageUrl = (article) => {    
    let url = ''    
    if (article.image === null || article.image === 'null'){
        url = article.video.thumbnail
    } else {
        url = article.image.url
    }        
    return url
}

export const getArticleImageUrl = {
    getImageUrl
}