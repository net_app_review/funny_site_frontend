import React, {useContext, useEffect, useState} from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {NavPath} from "./config/NavPath";
import Home from "./pages/Home/Home";
// import Upload from "./pages/Upload/Upload";
import Detail from "./pages/Detail/Detail";
import Header from "./components/layouts/Header/Header";
import StickyUploadButton from "./components/layouts/StickyUploadButton/StickyUploadButton"
import Body from "./components/layouts/Body/Body";
import {CurrentUserContext, LoggedInContext} from './Store'
import Privacy from './pages/About/Privacy/Privacy'
import PageRules from './pages/About/PageRules/PageRules'
import Donation from './pages/About/Donation/Donation'
import Feedback from './pages/About/Feedback/Feedback'
import FAQ from './pages/About/FAQ/FAQ'
import TermOfService from './pages/About/TermOfService/TermOfService'
import PersonalPage from './pages/PersonalPage/PersonalPage'
import CategoryPage from './pages/CategoryPage/CategoryPage'
import RankingPage from './pages/RankingPage/RankingPage'
import TagPage from './pages/TagPage/TagPage'
import HotPage from './pages/HotPage/HotPage'
import {NavRoute} from "./config/NavRoute";
import PasswordForgot from "./pages/Password/PasswordForgot"
import PasswordReset from "./pages/Password/PasswordReset"
import FreshPage from './pages/FreshPage/FreshPage'
import TrendingPage from './pages/TrendingPage/TrendingPage'
import UserSettings from './pages/UserSettings/UserSettings'
import apiClient from './config/apiClient'
import ReactGA from 'react-ga';
import SearchPage from './pages/SearchPage/SearchPage'
import NotFoundPage from './pages/NotFoundPage/NotFoundPage'
// import VotePage from './pages/VotePage/Details/VotePage'
import Cookies from 'js-cookie';
import './AppRouters.sass'
import uuid from 'react-uuid'
import swal from 'sweetalert'

const PrivateRoute = (props) => {
    const {path, component, ...other} = props
    document.title = NavRoute[path]? NavRoute[path].title:path
    return (
        <Route path={path} component={component} {...other}/>
    )
}

const AppRouters = props => {
    
    const [stCurrentUser, setstCurrentUser] = useContext(CurrentUserContext)
    const [LoggedIn, setLoggedIn] = useContext(LoggedInContext)
    const [stIsMouseMoving, setstIsMouseMoving] = useState(false)

    const generate_uniq_random_token = (length) => {        
        let result           = '';
        let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        // return result;
        
        result = result + Date.now()

        return result        
        
    }

    const setMouseMove = (e) => {
        e.preventDefault();        
        setstIsMouseMoving(true)
      
        let timeout;
        (() => {
          clearTimeout(timeout);
          timeout = setTimeout(() => setstIsMouseMoving(false), 50);
        })();        
    }

    const setGuestToken = () => {         
        if (LoggedIn === "NOT_LOGGED_IN") {
            if(Cookies.get("GuestTkn") === "undefined" || Cookies.get("GuestTkn") === undefined) {
                Cookies.set('GuestTkn', uuid())
            }
        } else if (LoggedIn === "LOGGED_IN") {            
            Cookies.remove('GuestTkn')
        }        
    }    

    const initGA = () => {
        // console.log("GA",`${process.env.REACT_GOOGLE_ANALYTIC_ID}`)        
        // ReactGA.initialize(`${process.env.REACT_GOOGLE_ANALYTIC_ID}`);
        ReactGA.initialize("UA-148510553-1");
    }

    const logPageView = () => {
        ReactGA.set({page: window.location.pathname})
        ReactGA.pageview(window.location.pathname)
    }

    const checkLoginStatus = () => {
        apiClient.get(`${process.env.REACT_APP_API_URL}/api/v1/users/current_user`).then(response => {
        
          if(response.status === 200){            
            setstCurrentUser(response.data.current_user)
            Cookies.get('AuthToken')
            setLoggedIn("LOGGED_IN")
          } else if (response.status === 204){
                // Cookies.remove('AuthToken');                
          }          
      }).catch(error=>{        
        Cookies.remove('AuthToken');
        setstCurrentUser({})
        swal("Error", `Something is wrong ${error}, please contact us at https://facebook.com/FunniqPage`, "error");
      })

    }

    useEffect(()=>{        
        let client_cookies_token = Cookies.get('AuthToken')
        if ((client_cookies_token === undefined && LoggedIn === 'LOGGED_IN') || (client_cookies_token === 'undefined' && LoggedIn === 'LOGGED_IN')){
            swal("Error", `You have logged out from other tabs, we will refresh you now, please log in again, any issues please contact us at https://facebook.com/FunniqPage`, "error")
            .then((value) => {
                window.location.reload();
            });
        }
    },[stIsMouseMoving]);

    useEffect(()=>{
        checkLoginStatus()
        setGuestToken();
    },[LoggedIn]);
    
    useEffect(()=>{
        initGA();
        logPageView();
        setGuestToken();
    },[]);

    return (
        <Router >            
            <Header current_user={stCurrentUser} onMouseMove={setMouseMove}/>
            <StickyUploadButton />
            <Body >
                <Switch>
                    <PrivateRoute path={NavPath.root} exact component={Home}/>
                    <PrivateRoute path={NavPath.home} component={Home}/>
                    <PrivateRoute path={NavPath.user} component={PersonalPage}/>
                    <PrivateRoute path={NavPath.articles} component={Detail}/>
                    <PrivateRoute path={NavPath.hot} component={HotPage}/>
                    <PrivateRoute path={NavPath.fresh} component={FreshPage}/>
                    <PrivateRoute path={NavPath.trending} component={TrendingPage}/>
                    <PrivateRoute path={NavPath.categories} component={CategoryPage}/>
                    <PrivateRoute path={NavPath.rankings} component={RankingPage}/>
                    <PrivateRoute path={NavPath.privacy} component={Privacy}/>
                    <PrivateRoute path={NavPath.pagerules} component={PageRules}/>
                    <PrivateRoute path={NavPath.feedback} component={Feedback}/>
                    <PrivateRoute path={NavPath.donate} component={Donation}/>                    
                    <PrivateRoute path={NavPath.faqs} component={FAQ}/>
                    <PrivateRoute path={NavPath.termofservice} component={TermOfService}/>
                    <PrivateRoute path={NavPath.password_forgot} component={PasswordForgot}/>
                    <PrivateRoute path={NavPath.password_reset} component={PasswordReset}/>
                    <PrivateRoute path={NavPath.settings} component={UserSettings}/>
                    <PrivateRoute path={NavPath.search} component={SearchPage}/>
                    <PrivateRoute path={NavPath.tags} component={TagPage}/>
                    {/* <PrivateRoute path={NavPath.vote} component={VotePage}/> */}
                    {/* <Route path="/404" component={NotFoundPage} status={404}/> */}
                    <Route path="*" component={NotFoundPage}> 
                        {/* <Redirect to="/404" /> */}
                    </Route>
                </Switch>
            </Body>
        </Router>
    );
};

AppRouters.propTypes = {

};

export default AppRouters;
