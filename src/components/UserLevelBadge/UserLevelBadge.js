import React from "react";
import PropTypes from 'prop-types'
import "./UserLevelBadge.sass"


const UserLevelBadge = (props) => {
    
    const levelBackgroundColor = (level) => {
        if (level >= 1 && level < 3) {
            return "#474692"
        } else if (level >= 3 && level < 5) {
            return "#4B78FF"
        } else if (level >= 5 && level < 7) {
            return "#4B8CFF"
        } else if (level >= 7 && level < 9) {
            return "#2D6EB7"
        } else if (level >= 9 && level < 11) {
            return "#2D82B7"
        } else if (level >= 11 && level < 13) {
            return "#27829E"
        } else if (level >= 13 && level < 15) {
            return "#228C85"
        } else if (level >= 15 && level < 17) {
            return "#1C7A66"
        } else if (level >= 17 && level < 19) {
            return "#1C8E66"
        } else if (level >= 19 && level < 21) {
            return "#1CA266"
        } else if (level >= 21 && level < 23) {
            return "#178050"
        } else if (level >= 23 && level < 25) {
            return "#9A6D45"
        } else if (level >= 25 && level < 27) {
            return "#966030"
        } else if (level >= 27 && level < 29) {
            return "#A6542E"
        } else if (level >= 29 && level < 31) {
            return "#A64B2E"
        } else if (level >= 31 && level < 33) {
            return "#C14720"
        } else if (level >= 33 && level < 35) {
            return "#AD1A09"
        } else if (level >= 35 && level < 37) {
            return "#BF2614"
        } else if (level >= 37 && level < 39) {
            return "#C83D2D"
        } else if (level >= 39 && level < 41) {
            return "#C65042"
        } else if (level >= 41 && level < 43) {
            return "#83433C"
        } else {
            return "#815854"
        }
    }

    return (
        <div className="user_level_badge" style={{color: "white", backgroundColor: `${levelBackgroundColor(props.level)}`, height: `${props.height}`}}>
            Lv. {props.level}
        </div>
    );
};

UserLevelBadge.propTypes = {
    level:  PropTypes.number
};

export default UserLevelBadge;
