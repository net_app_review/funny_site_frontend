import React from 'react';
import './LinedText.sass'

const LinedText = props => {    
    return (
        <div className='lined_text'>            
            <div className='conn_text'>
                {props.passedText}            
            </div>        
        </div>
    );
};

LinedText.propTypes = {

};

export default LinedText;