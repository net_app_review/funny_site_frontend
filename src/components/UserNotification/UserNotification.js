import React from "react";
import "./UserNotification.sass"
import {MDBDropdownItem, MDBContainer} from 'mdbreact'
import {Link} from 'react-router-dom';
import {replaceString} from '../../functions/replaceString'

const UserNotification = (props) => {
    
    const renderCreatorImage = (notification) => {        
        if (notification !== undefined && notification !== 'undefined'){
            if(notification.creator !== null){
                return  <Link to={`/users/${replaceString._replaceString("user", "", notification.creator)}`} style={{padding: "0px"}}>
                            <img src={notification.creatorImageUrl} alt="creator" className='creator_img'></img>
                        </Link>
            } else {
                return <></>
            }
        }        
    }
    
    const renderReferredObjectImage =(notification) => {        
        if (notification !== undefined && notification !== 'undefined'){
            if(notification.article !== null && notification.objImageUrl !== ""){
                return  <a  href={`https://funniq.com/articles/${notification.notiObjId}`} 
                            style={{padding: "0px"}}
                            target='_blank'
                            rel="noopener noreferrer">                            
                            <img src={notification.objImageUrl} alt="noti_img" className='article_img'></img>
                            &nbsp;
                        </a>
            } else {
                return <></>
            }
        }
    }

    const renderNotiContent =(notification) => {
        if (notification !== undefined && notification !== 'undefined'){
            if(notification.content !== null && notification.article !== null){
                return <div className='noti_text text-center'>
                    <a href={`https://funniq.com/articles/${notification.notiObjId}`}
                        style={{ padding: '0px', fontSize: '1em'}}
                        target='_blank'
                        rel="noopener noreferrer"
                        >
                            {notification.creator.name}
                            &nbsp;
                            {notification.content}
                            &nbsp;
                    </a>
                </div>
            } else if(notification.content !== null && notification.article === null){
                return <div className='noti_text text-center' style={{ padding: '0px', fontSize: '1em'}}>
                    <Link to={`/users/${notification.creator.id}`} style={{ padding: '0px', fontSize: '1em'}}>
                        {notification.creator.name}
                        &nbsp;
                        {notification.content}
                        &nbsp;
                    </Link>
                </div>
            }
            else {
                return <></>
            }
        }
    }
    
    return (
        <MDBDropdownItem className={`user_notification`} onClick={(e) => props.onClick(e,props.notification.id)}>
            <MDBContainer className={`noti_container ${props.notification.status}`}>
                {renderCreatorImage(props.notification)}
                {renderNotiContent(props.notification)}
                {renderReferredObjectImage(props.notification)}
            </MDBContainer>
            {/* {props.notification.id} */}
        </MDBDropdownItem>        
    );
};

UserNotification.propTypes = {

};

export default UserNotification;
