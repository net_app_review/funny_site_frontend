import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import style from './ModalReport.module.sass'
import {MDBModal, MDBBtn, MDBModalHeader, MDBModalBody, MDBModalFooter} from "mdbreact";
// import {StorageService} from "../../services/storageService";
// import axios from 'axios'   
import useUserAuthenForm from '../authentication/useUserAuthenForm'
import apiClient from "../../config/apiClient";
import swal from 'sweetalert'
import { useTranslation } from "react-i18next";

const ModalReport = (props) => {
    // const [stFormTitle, setStFormTitle] = useState('');
    // const [stFile, setStFile] = useState(null);
    // const [stDownloadLink, setStDownloadLink] = useState('');    
    const [stReportType, setstReportType] = useState([]);
    const { values, handleChange, handleSubmit } = useUserAuthenForm(report);
    const { t, i18n } = useTranslation();

    useEffect(() => {
        fetchCategory()
    }, []);

    const fetchCategory = () => {
        apiClient.get(`${process.env.REACT_APP_API_URL}/api/v1/reportcategories`)
        .then(response => {            
            if (response.status === 200){
                setstReportType(response.data)                 
            }
          })
          .catch(error => {
            swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");                    
          })
    }

    function report (){  
        let data = {}

        if (props.report_object_type === "article"){
            data = {
                report: {
                    article_id: props.report_object.id,
                    reportcategory_id: values.reportcategory_id                
                }
            }
        } else if (props.report_object_type === "comment") {
            data = {
                report: {
                    comment_id: props.report_object.id,
                    reportcategory_id: values.reportcategory_id                
                }
            }
        } else if (props.report_object_type === "sub_comment") {
            data = {
                report: {
                    subcomment_id: props.report_object.id,
                    reportcategory_id: values.reportcategory_id                
                }
            }
        }
        apiClient.post(`${process.env.REACT_APP_API_URL}/api/v1/reports/create`, data                            							
          ).then(response => {            
            if (response.status === 200){            
            //   window.location.reload();    
            props.toggle()
            swal("Report received", "Thanks for supporting Funniq community, we will look after this, if you need further action asap, please contact us at https://facebook.com/FunniqPage", "success");                            
            }
          })
          .catch(error => {            
            if (error.response.data && error.response.data.user_id[0]==="has already been taken"){
                swal("Something seems not right", "It looks like you have reported this section for the same reasons, please choose another reason or contact us at funniqpage@gmail.com", "error");                
            } else {
                swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
            }
          })
    }

    // const onChangeTitle = (e) => {
    //     const value = e.currentTarget.value
    //     setStFormTitle(value)
    // }

    // const onUploaded = (file) => {
    //     setStFile(file)
    // }    

    const {isOpen, onOk, toggle, ...other} = props
    return (
        <MDBModal isOpen={isOpen} toggle={toggle} {...other}>
            <MDBModalHeader toggle={toggle}>Create report</MDBModalHeader>
            <form onSubmit={handleSubmit} className="mt-4">
                <MDBModalBody className={style.mdBody}>                            
                    <div>{t("modalReport.reason")}</div>
                    <div>
                        <select className="browser-default custom-select" 
                                value={values.reportcategory_id} name="reportcategory_id" onChange={handleChange} required>
                            <option value=""></option>
                            {stReportType.map(category => {
                            return (
                                <option key={category.id}
                                        value={category.id}>
                                    {category.name}
                                </option>
                                )
                            })}                            
                        </select>
                    </div>                    
                </MDBModalBody>                
                <MDBModalFooter className={style.mdFooter}>
                    <MDBBtn color="red" type="submit">{t("modalReport.report")}</MDBBtn>
                </MDBModalFooter>
            </form>        
        </MDBModal>
    );
};

ModalReport.propTypes = {
    isOpen: PropTypes.bool,
    onOk: PropTypes.func,
    toggle: PropTypes.func,
};

export default ModalReport;
