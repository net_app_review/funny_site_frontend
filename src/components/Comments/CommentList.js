import React from 'react';
// import PropTypes from 'prop-types';
import {  MDBRow } from "mdbreact";
import Comment from "./Comment";

const CommentList = (props) => {

    return (
                
            <MDBRow style={{margin: "0px", padding: "0px"}}>                        
                      {
                        props.comments.map((comment, index) => {
                            return (
                                <Comment deletecomment={props.deletecomment} 
                                        onSubmit={props.onSubmit} 
                                        key={comment.id} 
                                        comment={comment}
                                        relatedArticle={props.relatedArticle}/>  
                            )
                        })
                    }                                                            
            </MDBRow>                    
    );
};


export default CommentList;
