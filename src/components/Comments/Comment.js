import React, {useState, useEffect, useContext} from "react";
import SubCommentForm from "./SubComments/SubCommentForm";
import SubCommentList from "./SubComments/SubCommentList";
import {MDBBtn, MDBContainer, MDBIcon, MDBRow, MDBCol,MDBDropdownMenu, MDBDropdownItem,MDBDropdown,MDBDropdownToggle} from "mdbreact";
import PropTypes from 'prop-types'
import {SignInModalContext, LoggedInContext, CurrentUserContext} from '../../Store'
import './Comment.sass'
import {Link} from 'react-router-dom'
import { confirmAlert } from 'react-confirm-alert'; // Import alert
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import alert css
import apiClient from "../../config/apiClient";
import ModalReport from "../../components/ModalReport/ModalReport";
import FsLightbox from 'fslightbox-react';
import { Twemoji } from 'react-emoji-render';
import swal from 'sweetalert'
import { useTranslation } from "react-i18next";
import UserLevelBadge from '../../components/UserLevelBadge/UserLevelBadge'
import UserNationalFlag from '../../components/UserNationalFlag/UserNationalFlag'
import {calculateElapsedTime} from '../../functions/calculateElapsedTime'
import {notificationService} from "../../services/notificationService"
import {getArticleImageUrl} from '../../functions/getArticleImageUrl'

const Comment = (props) => {
    const [stIsShowSubComments, setStIsShowSubComments] = useState(false);
    const [stIsShowSubCommentForm, setStIsShowSubCommentForm] = useState(false);
    const [stSubCommentsList, setstSubCommentsList] = useState([]);
    const [stLoggedIn, ] = useContext(LoggedInContext)
    const [, setSignInModal] = useContext(SignInModalContext)
    const [stCurrentUser,] = useContext(CurrentUserContext)
    const [stIsModalReportOpen, setStIsModalReportOpen] = useState(false); 
    const [stLikeStatus, setstLikeStatus] = useState("");   
    const [stImgLightBox, setstImgLightBox] = useState(false);
    const [stLikeCount, setStLikeCount] = useState(props.comment.like_count);
    const [stDislikeCount, setStDislikeCount] = useState(props.comment.dislike_count);
    const [stLikeId, setstLikeId] = useState(props.comment.like_status.id ? props.comment.like_status.id : 0);
    const { t, i18n } = useTranslation();
    const [stRepliedToUser, setstRepliedToUser] = useState({});

    useEffect(() => {		
        fetchSubcomments()                
        },[])

    useEffect(() => {
        if (props.comment.like_status === "not_authenticated"){
            setstLikeStatus("not_authenticated")
        } else if (props.comment.like_status === "no_action") {
            setstLikeStatus("no_action")            
        } else {                        
            setstLikeStatus(props.comment.like_status.liketype)
        }        
    },[props.comment])
  
    const changeLikeThumbsColor = () => {    
        if (stLikeStatus === "like"){
            return "thumbs-green"
        } else {
            return "thumbs-no-color"
        }
    }

    const changeDisLikeThumbsColor = () => {        
        if (stLikeStatus === "dislike"){
            return "thumbs-red"
        } else {
            return "thumbs-no-color"
        }
    }

    const toggleModalReport = () => {
        setStIsModalReportOpen(!stIsModalReportOpen)
    }
    

    const fetchSubcomments = () => {
        apiClient.get(`${process.env.REACT_APP_API_URL}/api/v1/subcomments/comment_subcomments?comment_id=${props.comment.id}&page=1`)
            .then(response => {
            setstSubCommentsList([...response.data])
            })
            .catch(
                error => swal("Something is wrong", `${error}, please contact us at https://facebook.com/FunniqPage`, "error")
        )
    }

    const toggleSubComments = () => {
        setStIsShowSubComments(!stIsShowSubComments)
    }

    const toggle_sub_comment_form = (replied_to_user) => {
        if (stLoggedIn === "LOGGED_IN"){
            setStIsShowSubCommentForm(!stIsShowSubCommentForm)

            if (replied_to_user.id !== undefined){
                setstRepliedToUser(replied_to_user)
            } else {
                setstRepliedToUser( {name: props.comment.user.name, id: props.comment.user.id})
            }        
        } else {
            setSignInModal(true)            
        }
    }


    const renderSubCommentForm = (comment) => {
        if (stIsShowSubCommentForm) {
            return <SubCommentForm 
                    setStIsShowSubComments = {setStIsShowSubComments}
                    onSubmit={fetchSubcomments}
                    parent_comment={comment}
                    repliedToUser={stRepliedToUser}
                    toggle_sub_comment_form = {toggle_sub_comment_form}
                    />
        }
        return null
    }
    const renderSubComments = (comment) => {
        if (stIsShowSubComments) {
            return <SubCommentList
            toggle_sub_comment_form = {toggle_sub_comment_form}
            onSubmit={fetchSubcomments}    
            subcomments={stSubCommentsList}            
            />
        }
        return null
    }

    const renderButtonTitle = (subcommentsAmount) => {
        if (stIsShowSubComments) {
            return "Hide replies"
        } else {
            return "Show " + subcommentsAmount + " replies"
        }
    }    

    const renderReport = () => {        
        if (props.comment.user.id === stCurrentUser.id ){
            return <MDBDropdownMenu right>
                        <MDBDropdownItem onClick={showDeleteCommentConfirmation}>{t("comment.delete")}</MDBDropdownItem>                            
                    </MDBDropdownMenu>
        } else {
            return <MDBDropdownMenu right>
                        <MDBDropdownItem onClick={onOpenModalReport}>{t("comment.report")}</MDBDropdownItem>
                        {/* <MDBDropdownItem divider />
                        <MDBDropdownItem>Hide this comment</MDBDropdownItem> */}
                    </MDBDropdownMenu>                                      
        }                                                                                      
                
    }

    const onOpenModalReport = () => {
        if (stLoggedIn === "LOGGED_IN"){
            setStIsModalReportOpen(true)
        } else {
            setSignInModal(true)            
        }
    }

    const showDeleteCommentConfirmation = () => {
        confirmAlert({
            title: 'Delete this comment',
            message: 'Are you sure to do this?',
            buttons: [
              {
                label: 'Yes',
                onClick: () => {
                    deleteComment()
                }
              },
              {
                label: 'No',
                onClick: () => {}
              }
            ]
        });
    }

    const deleteComment = () => {

        let data = {
            id: props.comment.id,
        }

        apiClient.post(`${process.env.REACT_APP_API_URL}/api/v1/comments/destroy`, data
        ).then(response => {            
            if (response.status === 200){
                swal("Comment deleted", "Your comment have been deleted", "success")                
                if (props.onSubmit) props.onSubmit()
                if (props.deletecomment) props.deletecomment(props.comment.id)        
            }
        })
            .catch(error => {
                swal("Internal Server Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
         })
    }

    // this is to handle like, unlike buttons

    const handleLikeClick = () => {

        if (stLoggedIn === "LOGGED_IN"){ 

            let data ={}

            if (stLikeStatus === "no_action") {
                data = {
                    id : "",
                    like: {      	
                        comment_id: props.comment.id,
                        liketype: "like"      
                    }
                }
            } else if (stLikeStatus === "no_value"){
                data = {
                    id : stLikeId,
                    like: {      	
                        comment_id: props.comment.id,
                        liketype: "like"      
                    }
                }
            } else if (stLikeStatus === "dislike"){
                data = {
                    id : stLikeId,
                    like: {      	
                        comment_id: props.comment.id,
                        liketype: "like"      
                    }
                }
            } else if (stLikeStatus === "like"){
                data = {
                    id : stLikeId,
                    like: {      	
                        comment_id: props.comment.id,
                        liketype: "no_value"      
                    }
                }
            }         

            apiClient.post(`${process.env.REACT_APP_API_URL}/api/v1/likes/handle_like`, data            
            ).then(response => {
                setstLikeId(response.data.id)                       
            })
            .catch(error => {      
                swal("Internal Server Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");          
            })

            if (stLikeId === 0 && stCurrentUser.id !== props.comment.user.id ){
                notificationService.createNotification(
                    stCurrentUser.uuid,
                    stCurrentUser.name,
                    stCurrentUser.image.url,
                    props.relatedArticle.uuid,
                    'comment',
                    props.comment.id,
                    '',
                    getArticleImageUrl.getImageUrl(props.relatedArticle),
                    props.relatedArticle.user.uuid,
                    `${stCurrentUser.name} has liked your coment`
                )
            }

            switch (stLikeStatus) {
                case "no_action":
                    setStLikeCount(stLikeCount + 1)                    
                    setstLikeStatus("like");
                    break
                case "like":
                    setstLikeStatus("no_value")
                    setStLikeCount(stLikeCount-1)
                    break
                case "dislike":
                    setstLikeStatus("like");
                    setStDislikeCount(stDislikeCount - 1)
                    setStLikeCount(stLikeCount + 1)
                    break
                case 'no_value':
                    setStLikeCount(stLikeCount + 1)                    
                    setstLikeStatus("like");
                    break
                default:
                    console.log("defauly")
            }
        } else {
            setSignInModal(true)            
        }

    }

    const handleDislikeClick = () => {
        if (stLoggedIn === "LOGGED_IN"){                  
            let data ={}

            if (stLikeStatus === "no_action") {
                data = {
                    id : "",
                    like: {      	
                        comment_id: props.comment.id,
                        liketype: "dislike"      
                    }
                }
            } else if (stLikeStatus === "no_value"){
                data = {
                    id : stLikeId,
                    like: {      	
                        comment_id: props.comment.id,
                        liketype: "dislike"      
                }
                }
            } else if (stLikeStatus === "dislike"){
                data = {
                    id : stLikeId,
                    like: {      	
                        comment_id: props.comment.id,
                        liketype: "no_value"      
                    }
                }
            } else if (stLikeStatus === "like"){
                data = {
                    id : stLikeId,
                    like: {      	
                        comment_id: props.comment.id,
                        liketype: "dislike"      
                }
                }
            }         

            apiClient.post(`${process.env.REACT_APP_API_URL}/api/v1/likes/handle_like`, data
                ).then(response => {
                               
                })
                .catch(error => {
                    swal("Internal Server Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
                })
                switch (stLikeStatus) {                    
                    case "like":
                        setstLikeStatus("dislike");
                        setStDislikeCount(stDislikeCount + 1)
                        setStLikeCount(stLikeCount - 1)
                        break
                    case "dislike":
                        setstLikeStatus("no_value");
                        setStDislikeCount(stDislikeCount - 1)
                        break
                    case 'no_value':
                        setStDislikeCount(stDislikeCount + 1)                    
                        setstLikeStatus("dislike");
                        break
                    case "no_action":
                        setstLikeStatus("dislike");
                        setStDislikeCount(stDislikeCount + 1)                        
                        break
                    default:
                        console.log("defauly")
                }
        } else {
            setSignInModal(true)            
        }
    }
    
    const CommentImage = () =>  {
        if (props.comment.image_url) {
            return (
                <>
                    <MDBRow className="pt-2 pb-0 pl-0 pr-0 mt-0 ml-0 mr-0 mb-1">
                        <FsLightbox toggler={stImgLightBox}
                            customSources={ [ 
                                <img className="image img-fluid"
                                    alt="ima"
                                    src={props.comment.image_url}
                                    onClick={ () => setstImgLightBox(!stImgLightBox) }/>
                                ] } 
                                customSourcesGlobalMaxDimensions={ 
                                    { width: 600, height: 400 } 
                                } 
                                customSourcesMaxDimensions={ 
                                    [null, null, { width: 300, height: 50 }] 
                                } 
                                
                            /> 
                        <img src={props.comment.image_url} className="comment_image" alt="" onClick={ () => setstImgLightBox(!stImgLightBox)}></img>
                    </MDBRow>
                </>
            )            
        } else {
            return (<></>)
        }        
    }    

    const renderCommentText = () => {
        if (props.comment.content) {
            return <Twemoji text={comment.content} className="comment_content" />
        } else {
            return <></>
        }

    }

    const {comment, ...other} = props    

    return (
        <MDBContainer {...other} className="Commend_sass">
        <ModalReport    report_object_type="comment"
                            report_object={comment}
                            isOpen={stIsModalReportOpen}
                           toggle={toggleModalReport}/>        
        <MDBRow className="p-0 m-0 comment_section" >
            <MDBCol size="2" className="image-col">
                <Link to={`/users/${comment.user.uuid}`} style={{ textDecoration: 'none', color: "black" }}>
                    <img className="rounded-circle image" src={comment.user.image.url} alt="" />
                </Link>
            </MDBCol>
            <MDBCol size="10" className="content-col pl-0 pr-0">          
                <MDBRow className="username-row">
                    <MDBCol size="10" className="m-0 p-0 pt-0">
                        <div className="d-flex flex-row">
                            <div className="p-0">
                                <Link to={`/users/${comment.user.uuid}`} style={{ textDecoration: 'none', color: "black" }}>
                                    <h6 className="font-weight-bold mt-0 pt-0">                      
                                        {comment.user.name}
                                    </h6>                                            
                                </Link>
                            </div>
                            <div className="ml-2 pb-1">
                                <UserNationalFlag country_code={comment.user.country_code} height="14px"/>
                            </div>                        
                                                    
                            <div className="ml-0">
                                <UserLevelBadge level={comment.user.level} height="1.2em"/>
                            </div>
                            {/* &#8231; */}
                            <div className='ml-2' style={{ fontSize: '0.8rem', color: '#B3B3B3'}}>
                                {calculateElapsedTime.elapsedTime(comment.elapsed_time)}
                            </div>                            
                        </div>                                                
                    </MDBCol>
                    <MDBCol size="2" className="text-center p-0 pr-0">
                        <MDBDropdown dropleft className="align-top pb-2" style={{height: "1em", margin: "0px"}} >
                                <MDBDropdownToggle color="primary" id="settings-dropdown-comment">
                                    <MDBIcon icon="ellipsis-v" id="comment-settings-icon"/>
                                </MDBDropdownToggle>
                                {
                                    renderReport()
                                }
                            </MDBDropdown>
                        <MDBBtn className="float-right m-0 p-0 rounded-circle" id="button-comment-settings">
                            
                        </MDBBtn>
                    </MDBCol>
                </MDBRow>
                <MDBRow className={`p-0 m-0 comment_text_container`}>
                    {renderCommentText()}
                </MDBRow>
                {/* &nbsp; */}                
                    {CommentImage()}                                                                                    
                <MDBRow className={`comment_btn_row`}>                                    
                    <MDBBtn id="button-thumbs" onClick={handleLikeClick}>
                        <span>
                            {stLikeCount}
                        </span>
                        &nbsp;                    
                        &nbsp;                    
                        <MDBIcon icon="arrow-alt-circle-up" id="icon-arrow-alt-circle-up" className={changeLikeThumbsColor()}/>
                    </MDBBtn>
                    <MDBBtn id="button-thumbs" onClick={handleDislikeClick}>
                        <span> 
                            {stDislikeCount}
                        </span>
                        &nbsp;
                        &nbsp;                    
                        <MDBIcon icon="arrow-alt-circle-down" id="icon-arrow-alt-circle-down" className={changeDisLikeThumbsColor()}/>
                    </MDBBtn>
                    {/* <h5> {comment.points} points</h5>                         */}                    
                    <MDBBtn onClick={toggle_sub_comment_form} id="button-reply">
                        Reply
                    </MDBBtn>
                </MDBRow>
                <div className={`show_replies text-left`}>
                    {    stSubCommentsList.length >0 ?
                        <MDBBtn id="button-show-replies" onClick={toggleSubComments}>
                                {
                                    renderButtonTitle(stSubCommentsList.length)
                                }
                        </MDBBtn>                    
                        
                        :
                        <></>
                    }
                </div>                     
                    {
                        renderSubComments(comment)
                    }
                
                <div>
                    {
                        renderSubCommentForm(comment)
                    }
                </div>          
          </MDBCol>
        </MDBRow>
        </MDBContainer>
    )
}


Comment.propTypes = {
  comment: PropTypes.any
}

export default Comment
