import React, {useState, useEffect} from "react";
import {MDBBadge, MDBIcon} from "mdbreact";
import PropTypes from 'prop-types'
import './ReplyTag.sass'
import {Link} from 'react-router-dom'

const ReplyTag = (props) => {    
    // const [stIsShowSubCommentForm, setStIsShowSubCommentForm] = useState(false);    
    const [stTagState, setstTagState] = useState(props.stTagState ? props.stTagState : '' )
  
    useEffect(()=> {
        setstTagState(props.stTagState)
    },[props.stTagState])

    const removeTag = (e) => {
        // e.preventDefault()        
        setstTagState('removed')
    }

    return (        
        <div className={`ReplyTag ${stTagState}`} >
            <MDBBadge color="default" className="d-flex flex-row reply_badge">                
                @{props.replyToUser.name}
                <button type="button" className="close_button" onClick={removeTag}>
                    <MDBIcon icon="times" />
                </button>
            </MDBBadge>            
        </div>       
    )
}


ReplyTag.propTypes = {
//   sub_comment: PropTypes.any
    replyToUser: PropTypes.string
}

export default ReplyTag
