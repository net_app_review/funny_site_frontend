import React, {useState} from 'react';
// import PropTypes from 'prop-types';
import {  MDBRow,MDBCol} from "mdbreact";
// import {SignUpModalContext,SignInModalContext,LoggedInContext} from '../../../../Store'
// import axios from 'axios'
// import {CommentsContext} from '../../Detail'
import SubComment from './SubComment'
import './SubCommentList.sass'

const SubCommentList = props => {
    // const [stPersonalSubComment, setstPersonalSubComment] = useState(false);

    // const togglePersonalSubComment = () => {
    //     setstPersonalSubComment(!stPersonalSubComment)
    // }

    return (            
        <MDBRow>
            <MDBCol sm={12} lg={12} md={12} xl={12} xs={12} className="p-0" >                    
                    {
                    props.subcomments.map(sub_comment => 
                    < div key={sub_comment.id} className="sub-comment-container">
                        
                        <SubComment  
                            toggle_sub_comment_form = {props.toggle_sub_comment_form}   
                            onSubmit={props.onSubmit}                            
                            sub_comment={sub_comment}/>                            
                    </div>)
                    }                                              
            </MDBCol>
        </MDBRow>            
    );
};

export default SubCommentList;
