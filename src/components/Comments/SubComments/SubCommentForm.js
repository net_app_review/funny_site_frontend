import React, {useState, useRef,useEffect, useContext} from 'react';
import PropTypes from 'prop-types';
import {
    MDBContainer
} from "mdbreact";
// import {CurrentUserContext} from '../../../../Store'
// import {CommentsContext} from '../../Detail'
// import useUserAuthenForm from '../../../../components/authentication/useUserAuthenForm'
import styles from './SubCommentForm.module.sass'
import {postSubCommentService} from '../../../services/postSubCommentService'
import {SignInModalContext, LoggedInContext} from '../../../Store'
import swal from 'sweetalert'
// import ReplyTag from './ReplyTag/ReplyTag'
import { useTranslation } from "react-i18next";

const SubCommentForm = (props) => {    
    // const [Subcomments, setSubcomments] = useState(CommentsContext)
    // const { values, handleChange, handleSubmit } = useUserAuthenForm(new_sub_comment);    
    // const [stCurrentUser, setstCurrentUser] = useContext(CurrentUserContext)
    const inputRef = useRef(null);
    const [isEditing, ] = useState(true);
    const [stSubComment, setstSubComment] = useState('');    
    const [stLoggedIn,] = useContext(LoggedInContext)
    const [, setstSignInModal] = useContext(SignInModalContext)
    // const [stSubCommentLoading, setstSubCommentLoading] = useState('');
    const { t, i18n } = useTranslation();    

    useEffect(() => {
      if (isEditing) {
        inputRef.current.focus();
        inputRef.current.value="@" + props.repliedToUser.name + " "
        // inputRef.current.value.style.backgroundColor="green"
      }
    }, [isEditing, props.repliedToUser.name]);

    const new_sub_comment = () => {
      if (stLoggedIn === "LOGGED_IN"){        
        postSubCommentService.postSubComment(stSubComment, props.parent_comment.id, stSubComment.includes("@" + props.repliedToUser.name) ? props.repliedToUser.id : "")
          .then(result => {            
            if (result.data.status === 201) {              
              if (props.setStIsShowSubComments) props.setStIsShowSubComments(true)      
              if (props.onSubmit) props.onSubmit()
            }            
            
          }).catch(error => {   
            swal("Error", "Something is wrong, please contact us at https://facebook.com/FunniqPage", "error");                        
            
          })

        } else {
          setstSignInModal(true)            
      }       
    }

    const handleSubCommentSubmit = (e) => {
      if (e) {
        e.preventDefault();      
        new_sub_comment ()          
        setstSubComment("")
      }
    }

    const handleSubCommentChange = (event) => {
      event.preventDefault();        
      setstSubComment(event.target.value)            
    };

    const _handleOnKeyDown = (event) =>{
      if (event.ctrlKey && event.keyCode === 13) {
        handleSubCommentSubmit(event)
      }
    }
    return (        
        <MDBContainer className={styles.sub_comment_form}>
          <React.Fragment>            
          <form onSubmit={(e) => handleSubCommentSubmit(e)} className={styles.input_form}>
            <div className="form-group">
              <div className={styles.reply_text_area}>                
                  {isEditing && <textarea
                  maxLength="200"
                  ref={inputRef} 
                  onChange={(e) => handleSubCommentChange(e)}
                  value={stSubComment}
                  className={styles.textArea}
                  placeholder="Your reply..."
                  name="message"
                  rows="2"
                  onKeyDown={(e) =>_handleOnKeyDown(e)}
                  required                  
                />}
              </div>              
                <div className="d-flex flex-row justify-content-end">
                  <button onClick={props.toggle_sub_comment_form} className={`btn ${styles.cancel_button}`}>
                    Cancel                    
                  </button>
                  <button disabled={false} className={`btn btn-primary ${styles.reply_button}`}>
                  {t("comment.reply")}
                  </button>          
                </div>
              </div>
          </form>
        
        </React.Fragment>
        </MDBContainer>
    );
};


SubCommentForm.propTypes = {
    article_id: PropTypes.any,
    onSubmit: PropTypes.func,
    toggleSubComments: PropTypes.func,
}

export default SubCommentForm;
