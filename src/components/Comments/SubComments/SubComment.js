import React, {useState, useEffect, useContext} from "react";
import {MDBBtn,  MDBIcon, MDBRow, 
        MDBCol,MDBDropdown,MDBDropdownToggle,
        MDBDropdownMenu, MDBDropdownItem} from "mdbreact";
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'
import {SignInModalContext,LoggedInContext,CurrentUserContext} from '../../../Store'
import { confirmAlert } from 'react-confirm-alert'; // Import alert
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import alert css
import apiClient from "../../../config/apiClient";
import ModalReport from "../../../components/ModalReport/ModalReport";
import { Twemoji } from 'react-emoji-render';
import styles from './SubComment.module.sass'
import swal from 'sweetalert'
import { useTranslation } from "react-i18next";
import UserLevelBadge from '../../../components/UserLevelBadge/UserLevelBadge'
// import {thumbs_green} from './SubComment.module.sass'
import UserNationalFlag from '../../../components/UserNationalFlag/UserNationalFlag'
import {replaceString} from '../../../functions/replaceString'
import {calculateElapsedTime} from '../../../functions/calculateElapsedTime'

const SubComment = (props) => {
    const [stCurrentUser, ] = useContext(CurrentUserContext)
    const [stLoggedIn, ] = useContext(LoggedInContext)
    const [, setSignInModal] = useContext(SignInModalContext)
    const [stIsModalReportOpen, setStIsModalReportOpen] = useState(false); 
    const [stLikeStatus, setstLikeStatus] = useState("");
    const { t, i18n } = useTranslation();

    useEffect(() => {		        

        if (props.sub_comment.like_status === "not_authenticated"){
            setstLikeStatus("not_authenticated")
        } else if (props.sub_comment.like_status === "no_action") {
            setstLikeStatus("no_action")            
        } else {                        
            setstLikeStatus(props.sub_comment.like_status.liketype)
        }        
    },[props.sub_comment])

    const {sub_comment, ...other} = props

    const showSubCommentForm = () => {
        props.toggle_sub_comment_form({id: props.sub_comment.user.id, name: props.sub_comment.user.name })
        // if (props.setstIsShowSubCommentForm) props.setstIsShowSubCommentForm(true)
    }

    const toggleModalReport = () => {
        setStIsModalReportOpen(!stIsModalReportOpen)
    }

    const onOpenModalReport = () => {
        if (stLoggedIn === "LOGGED_IN"){
            setStIsModalReportOpen(true)
        } else {
            setSignInModal(true)            
        }
    }


    const showDeleteSubCommentConfirmation = () => {
        confirmAlert({
            title: 'Delete this comment',
            message: 'Are you sure to do this?',
            buttons: [
              {
                label: 'Yes',
                onClick: () => {
                    deleteSubComment()
                }
              },
              {
                label: 'No',
                onClick: () => {}
              }
            ]
        });
    }


    const renderReport = () => {        
        if (props.sub_comment.user.id === stCurrentUser.id ){
            return <MDBDropdownMenu right className="custom-dropdown-menu">
                        <MDBDropdownItem onClick={showDeleteSubCommentConfirmation} style={{ display: "inline-block"}}>{t("comment.delete")}</MDBDropdownItem>                            
                    </MDBDropdownMenu>
        } else {
            return <MDBDropdownMenu right className="custom-dropdown-menu">
                        <MDBDropdownItem onClick={onOpenModalReport}>{t("comment.report")}</MDBDropdownItem>
                        {/* <MDBDropdownItem divider />
                        <MDBDropdownItem>Hide this comment</MDBDropdownItem> */}
                    </MDBDropdownMenu>                                      
        }                                                                                      
                
    }    

    const deleteSubComment =() => {

        let data = {
            id: props.sub_comment.id,
        }

        apiClient.post(`${process.env.REACT_APP_API_URL}/api/v1/subcomments/destroy`, data
        ).then(response => {
            if (response.status === 200){
                swal("Comment deleted", "Your comment have been deleted", "success")                                
                if (props.onSubmit) props.onSubmit()               
            }
        })
            .catch(error => {
                swal("Internal Server Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
         })
    }

    const handleLikeClick = () => {

        if (stLoggedIn === "LOGGED_IN"){ 


            let data ={}

            if (stLikeStatus === "no_action") {
                data = {
                    id : "",
                    like: {      	
                        subcomment_id: props.sub_comment.id,
                        liketype: "like"      
                    }
                }
            } else if (stLikeStatus === "no_value"){
                data = {
                    id : props.sub_comment.like_status.id,
                    like: {      	
                        subcomment_id: props.sub_comment.id,
                        liketype: "like"      
                }
                }
            } else if (stLikeStatus === "dislike"){
                data = {
                    id : props.sub_comment.like_status.id,
                    like: {      	
                        subcomment_id: props.sub_comment.id,
                        liketype: "like"      
                    }
                }
            } else if (stLikeStatus === "like"){
                data = {
                    id : props.sub_comment.like_status.id,
                    like: {      	
                        subcomment_id: props.sub_comment.id,
                        liketype: "no_value"      
                }
                }
            }         

            apiClient.post(`${process.env.REACT_APP_API_URL}/api/v1/likes/handle_like`, data            
            ).then(response => {
                if (props.onSubmit) props.onSubmit()
            })
            .catch(error => {
                swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
            })         
        } else {
            setSignInModal(true)            
        }

    }

    const handleDislikeClick = () => {
        if (stLoggedIn === "LOGGED_IN"){            
            let data ={}

            if (stLikeStatus === "no_action") {
                data = {
                    id : "",
                    like: {      	
                        subcomment_id: props.sub_comment.id,
                        liketype: "dislike"      
                    }
                }
            } else if (stLikeStatus === "no_value"){
                data = {
                    id : props.sub_comment.like_status.id,
                    like: {      	
                        subcomment_id: props.sub_comment.id,
                        liketype: "dislike"      
                }
                }
            } else if (stLikeStatus === "dislike"){
                data = {
                    id : props.sub_comment.like_status.id,
                    like: {      	
                        subcomment_id: props.sub_comment.id,
                        liketype: "no_value"      
                    }
                }
            } else if (stLikeStatus === "like"){
                data = {
                    id : props.sub_comment.like_status.id,
                    like: {      	
                        subcomment_id: props.sub_comment.id,
                        liketype: "dislike"      
                }
                }
            }         

            apiClient.post( `${process.env.REACT_APP_API_URL}/api/v1/likes/handle_like`, data
            ).then(response => {                
                if (props.onSubmit) props.onSubmit()
            })
            .catch(error => {                
                swal("Login error", `${error}, please contact us at https://facebook.com/FunniqPage`, "error")
            })         
        } else {
            setSignInModal(true)        
        }
    }

    const renderRepliedUser = () => {        
        // let splittedText = text.split('');
        // for ( let i = 0; i < splittedText.length -1; i++) {
        //     if (isRepliedUser(splittedText[i])) {

        //     }

        // }

        if (props.sub_comment.replied_to !== null){
            return <>
                    <Link to={`/users/${props.sub_comment.replied_to.uuid}`} className="replied_to_user">                                    
                        @{props.sub_comment.replied_to.name}
                    </Link>
                </>
        } else {
            return <></>
        }
    }    

    const tailorSubCommentContent = () =>{
        if (props.sub_comment.replied_to != null && props.sub_comment.content.includes("@" + props.sub_comment.replied_to.name)){
            return replaceString._replaceString("@" + props.sub_comment.replied_to.name, "", props.sub_comment.content)
        } else {
            return props.sub_comment.content
        }
    }

    return (        
        <MDBRow   {...other} className={styles.sub_comment}>
            <ModalReport    report_object_type="sub_comment"
                report_object={sub_comment}
                isOpen={stIsModalReportOpen}
                toggle={toggleModalReport}/>
            <MDBCol size="1" className={`p-0 ${styles.avatar_column}`}>
                <Link to={`/users/${sub_comment.user.uuid}`} style={{ textDecoration: 'none', color: "black" }}>
                    <img className={`rounded-circle ${styles.sub_comment_avatar}`}
                        src={sub_comment.user.image.url} 
                        alt="" id={styles.sub_comment_img} />
                </Link>
            </MDBCol>        
            <MDBCol size="11" className={`${styles.sub_comment_column}`}> 
                <MDBRow className="p-0">
                    <MDBCol size="11" className="m-0 p-0">
                        <div className="d-flex flex-row">
                            <Link to={`/users/${sub_comment.user.uuid}`} style={{ textDecoration: 'none', color: "black" }}>
                                <h6 className="font-weight-bold mt-0 text-left">                    
                                    {sub_comment.user.name}
                                </h6>                            
                            </Link>
                            <div className="ml-2 ">
                                <UserNationalFlag country_code={sub_comment.user.country_code} height="12px"/>
                            </div>
                            <div className="ml-0">
                                <UserLevelBadge level={sub_comment.user.level} height="1.2em"/>
                            </div>
                            <div className='ml-2' style={{ fontSize: '0.8rem', color: '#B3B3B3'}}>
                                {calculateElapsedTime.elapsedTime(sub_comment.elapsed_time)}
                            </div>
                        </div>
                        
                        <div className={styles.sub_comment_container}>
                            <h6 className={styles.sub_comment_content}>
                                {renderRepliedUser()}                                
                                <Twemoji text={tailorSubCommentContent()} className={styles.comment_content} />                                
                            </h6>
                        </div>
                    </MDBCol>
                    <MDBCol size="1" className="text-right p-0">
                        <MDBDropdown className="float-rights">
                            <MDBDropdownToggle id={styles.settings_dropdown_button}>
                                <MDBIcon icon="ellipsis-v" id={styles.settings_dropdown_icon}/>
                            </MDBDropdownToggle>
                            {
                                renderReport()
                            }
                        </MDBDropdown>
                        <MDBBtn className="float-right m-0 p-0 rounded-circle" id="button-comment-settings">                            
                        </MDBBtn>
                    </MDBCol>
                </MDBRow>
                <MDBRow className={styles.sub_comment_buttons}>
                    <MDBBtn id="button-thumbs" onClick={handleLikeClick} >
                        <span >
                            {sub_comment.like_count}
                        </span>
                        &nbsp;                    
                        &nbsp;                    
                        <MDBIcon icon="arrow-alt-circle-up" id="icon-arrow-alt-circle-up" style={stLikeStatus === "like" ? {color: "#006400"}: {}}/>
                    </MDBBtn>
                    <MDBBtn id="button-thumbs" onClick={handleDislikeClick}>
                        <span> 
                            {sub_comment.dislike_count}
                        </span>
                        &nbsp;
                        &nbsp;                    
                        <MDBIcon icon="arrow-alt-circle-down" id="icon-arrow-alt-circle-down" style={stLikeStatus === "dislike" ? {color: "#bb3838"}: {}}/>
                    </MDBBtn>
                    {/* <h5> {comment.points} points</h5>                         */}
                    <MDBBtn id="button-reply" onClick={showSubCommentForm}>
                        Reply
                    </MDBBtn>                
                </MDBRow>
            </MDBCol>
        </MDBRow>            
    )
}


SubComment.propTypes = {
  sub_comment: PropTypes.any
}

export default SubComment
