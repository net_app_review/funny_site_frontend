import React, { useState,
                useEffect,
                useContext,
                useRef
              } from 'react';
import PropTypes from 'prop-types';
import {
     MDBContainer,MDBRow, MDBCol, MDBIcon, MDBBtn
} from "mdbreact";
import {SignInModalContext,LoggedInContext,CurrentUserContext} from '../../Store'
import './CommentForm.sass'
import {postCommentService} from '../../services/postCommentService'
import swal from 'sweetalert'
import { MoonLoader } from 'react-spinners'; 
import LoginMoonLoaderCss from '../../config/ReactLoader/LoginMoonLoaderCss'
import { useTranslation } from "react-i18next";
import Resizer from 'react-image-file-resizer';

const CommentForm = (props) => {    
    // const [Comments, setComments] = useContext(CommentsContext)
    // const { values, handleChange, handleSubmit } = useUserAuthenForm(new_comment);
    const [stLoggedIn, ] = useContext(LoggedInContext)
    const [, setstSignInModal] = useContext(SignInModalContext)
    const [stCurrentUser,] = useContext(CurrentUserContext)
    const [stPreviewCommentImage, setstPreviewCommentImage] = useState(null)
    const [stPreviewCommentImageUrl, setstPreviewCommentImageUrl] = useState("")
    // const [stDownloadLink, setStDownloadLink] = useState('');
    const [stComment, setstComment] = useState('');
    const [stCommentLoading, setstCommentLoading] = useState(false);
    const { t, } = useTranslation();

    const commentFormRef = useRef(null);

    useEffect(() => {
      if (commentFormRef.current) {
        // let topOffSet = commentFormRef.current.getBoundingClientRect().y

        let topOffSet = 300

        const timer = setTimeout(() => {
          window.scrollTo({
            behavior: "smooth",
            top: topOffSet
          });
          commentFormRef.current.focus()
        }, 1000);
        return () => clearTimeout(timer);
      }      
      
    }, []);

    const handleCommentSubmit = (event) => {
      if (event) {
        event.preventDefault();      
        new_comment ()          
        setstComment("")
      }               
    };
   
    const handleCommentChange = (event) => {
      event.preventDefault();        
      setstComment(event.target.value)            
    };

    const new_comment = () => {      
      if (stLoggedIn === "LOGGED_IN"){
        setstCommentLoading(true)
        postCommentService.postComment(stPreviewCommentImage, props.obj, props.id, stComment)
          .then(result => {         
            if (result.data.status === 201) {
              if (props.onSubmit) props.onSubmit(result.data.data)
              setstPreviewCommentImage(null)
              setstPreviewCommentImageUrl(null)
              setstCommentLoading(false)
            }                                    
          }).catch(error => {   
            swal("Internal Server Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
            setstCommentLoading(false)
          })

        } else {
          setstSignInModal(true)            
      }
    }
  

    const fetchCurrentUserAvatar = () => {
      if (stCurrentUser.image){
        return stCurrentUser.image.url
      } else {
        return "https://firebasestorage.googleapis.com/v0/b/funniq-78dbd.appspot.com/o/funniq_internal%2Fbasic_icon.jpeg?alt=media&token=844b83dc-bb8a-423e-a526-517e438fde20"
      }
    }
    

    const _handleImageChange = (e) => {
      e.preventDefault();
      if (e.target.files[0]) {
        if(e.target.files[0].size > 10000000 ){
          alert("Sorry bro, your image is too big! Max image size at the moment is 10 MB");
          e.target.value = "";
        } else {        
          let reader = new FileReader();
          let file = e.target.files[0];
      
          reader.onloadend = () => {
            setstPreviewCommentImageUrl(reader.result)               
            Resizer.imageFileResizer(
              file,
              550,
              50000,
              'JPEG',
              90,
              0,
              uri => {                                     
                setstPreviewCommentImage(uri)                
              },
              'blob'                    
            );
          }
          reader.readAsDataURL(file)
        }
      }
    }

    const handleRemoveImage = (e) => {
      setstPreviewCommentImageUrl(null)
      setstPreviewCommentImage(null)
    }

    const _handleCommentOnKeyDown = (event) => {
      if (event.ctrlKey && event.keyCode === 13) {
        handleCommentSubmit(event)
      }
    }

    return (        
      <React.Fragment>
        <MDBContainer className="comment-form">
          <MDBRow className="comment-form-row">            
            <MDBCol size="2" className="user_avatar text-right pr-1 pl-lg-0 pl-0">
              <img className="img-fluid rounded-circle comment-avatar" src={fetchCurrentUserAvatar()} alt=''/>
            </MDBCol>
            <MDBCol size="10" className="form-col pl-0 pl-lg-1">
              <form onSubmit={(e) => handleCommentSubmit(e)}>
                <div className="form-group comment-text">
                  <textarea
                    ref={commentFormRef}
                    maxLength="200"                 
                    onChange={(e) => handleCommentChange(e)}
                    value={stComment}
                    className="form-control comment-textarea"
                    placeholder={t("comment.comment")}
                    name="message"
                    rows="2"
                    onKeyDown={(e) =>_handleCommentOnKeyDown(e)}
                    required
                  />
                  <div className="image_icon">
                    <img src={stPreviewCommentImageUrl} className="comment_img" alt=''></img>
                    <MDBBtn className={`remove_image_btn ${stPreviewCommentImage === null || stPreviewCommentImage === 'null' ? 'd-none': ''}`}>
                        <MDBIcon icon="times" onClick={(e)=>handleRemoveImage(e)}/>                      
                    </MDBBtn>
                  </div>
                </div>
                <div className="d-flex justify-content-end button-list">
                  
                    <input className="comment_fileinput"
                      type="file" 
                      accept={"image/png, image/jpeg"}
                      onChange={(e)=>_handleImageChange(e)}                      
                    />
                  <div className="p-2 col-example text-left" id="comment-button-div">                    
                    <button disabled={false} className="btn btn-primary comment-button"  type="submit">
                      Comment
                      &nbsp;
                      <MoonLoader          
                        css={LoginMoonLoaderCss}            
                        size={20}
                        color={'black'}
                        loading={stCommentLoading}                        
                      />
                    </button>
                  </div>
                </div>                
            </form>
            </MDBCol>                    
         </MDBRow>         
        </MDBContainer>
      </React.Fragment>
    );
};


CommentForm.propTypes = {
    obj: PropTypes.string,
    id: PropTypes.any,
    onSubmit: PropTypes.func,    
}

export default CommentForm;
