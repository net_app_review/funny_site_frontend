import React, {useState} from 'react'
import './AgeConfirmOverlay.sass'
import swal from 'sweetalert';

const AgeConfirmOverlay = (props) => {

  const [stAgeConfirmOverlay, setstAgeConfirmOverlay] = useState('d-none')
  
    return (      
        <div className={`age_confirm_overlay ${props.isShown}`}
          onClick={(e) => {
          swal("Error!", props.message , "error")
          return
          }}
        >               
        </div>
    );

}

export default AgeConfirmOverlay
