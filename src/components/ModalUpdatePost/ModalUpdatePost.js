import React, {useEffect, useState} from 'react';
import { withRouter } from 'react-router'
import PropTypes from 'prop-types';
import style from './ModalUpdatePost.module.sass'
import {        
    MDBModal,
    MDBModalHeader,    
    MDBModalBody
} from "mdbreact";
// import { useTranslation } from "react-i18next";
import {categoriesService} from "../../services/categoriesService";
import {articlesService} from "../../services/articlesService";
import MultipleTag from '../ModalUploader/MultipleTag/MultipleTag';
import swal from 'sweetalert';

const ModalUpdatePost = props => {

    // const { t,  } = useTranslation();
    const [stIsSensitiveArticle, setstIsSensitiveArticle] = useState(props.article.sensitive);
    const [stCategorySelect, setstCategorySelect] = useState(props.article.category.name);
    const [stCategories, setStCategories] = useState([]);
    const [stTagLists, setstTagLists] = useState(props.article.tags.map (tag => tag.name));
    const [stPostDescription, setstPostDescription] = useState(props.article.header);
    const [stIsModalOpen, setstIsModalOpen] = useState()

    useEffect(() => {        
        categoriesService.getCategories()
            .then(result => {
                setStCategories(result)                
            })        
    }, []);

    useEffect(() => {        
        setstIsModalOpen(props.isOpen)
    }, [props.isOpen]);

    const getTagList = (tagList) => {
        setstTagLists(tagList);      
    }

    const updatePostInfo = (e) => {
        e.preventDefault()        
        let data = {}
        if (stPostDescription === "" || stCategorySelect === "" || stTagLists === null ){            
            swal("Missing information", "please fill in the post description, category and at least one tag", "error");                            
        } else {
            data = {
                id: props.article.uuid,
                article: {                                            
                    header: stPostDescription,                    
                    category: stCategorySelect,                    
                    tags: stTagLists.toString(),
                    sensitive: stIsSensitiveArticle
                }
            }

            articlesService.updateArticles(data)
                .then(result => {                    
                    if (result.status === 200) {
                        swal("Update successful", "Your post has been updated, please click ok to reload", "success")
                        .then((value) => {
                            if (value === true){
                              window.location.reload();              
                            }                  
                        });                  
                        // setstIsModalOpen(!stIsModalOpen);
                        // props.toggle()                        
                    }
                })
                .catch(error => {
                    swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
                })
        }
    }

    const {isOpen, onOk, toggle, article, ...other} = props

    return (
        <MDBModal isOpen={stIsModalOpen} toggle={toggle} {...other} className={style.modal_updatepost}>
            <div className={style.cmp_column}>            
                <MDBModalHeader className={style.modal_header} toggle={toggle}>
                    {/* {t("modalUpdatePost.update")} */}
                    Update post
                </MDBModalHeader>
                <MDBModalBody className={style.modal_body}>
                    <form onSubmit={(e) => updatePostInfo(e)}>
                        <div className={style.update_form}>                                                                                        
                            <textarea className={style.article_description}
                                      defaultValue={article.header}
                                      placeholder='post description ...'
                                      onChange={(e) => setstPostDescription(e.target.value)}
                                      required
                                      >
                            </textarea>                            
                            <div className={style.other_stats}>
                                <div className={style.cate_select}>
                                    <select className={["browser-default mb-2", 'custom-select', style.selectBox].join(' ')}
                                        // defaultValue={stCategories[0]}
                                        id="category_select"
                                        value={stCategorySelect}
                                        onChange={(e) => setstCategorySelect(e.target.value)}
                                        required
                                    >
                                        <option value="" defaultValue>Fun</option>
                                        {stCategories.map(category => {
                                            return (
                                                <option key={category.id}
                                                        value={category.name}>
                                                    {category.name}
                                                </option>
                                            )
                                        })}
                                    </select>
                                </div> 

                                <div className={`custom-control custom-checkbox ${style.sensitive_check}`}>
                                    <input  type="checkbox" 
                                            className="custom-control-input"
                                            id="defaultChecked"
                                            checked={stIsSensitiveArticle}
                                            onChange={(event) => {setstIsSensitiveArticle(event.target.checked)}} />
                                    <label className="custom-control-label" htmlFor="defaultChecked">sensitive content</label>
                                </div>

                                <div className={style.tag_list}>
                                    <MultipleTag tagList={getTagList} defaultList={stTagLists} />
                                </div>                                
                            </div>
                        </div>
                        <div className={style.buttons_panel}>
                            <input type="submit" value="Update post" />
                            {/* <button className={`btn ${style.cancel_button}`} >
                                Cancel
                            </button> */}
                        </div>             
                        
                    </form>
                    
                </MDBModalBody>
                                   
            </div>
        </MDBModal>
    );
};

ModalUpdatePost.propTypes = {
    isOpen: PropTypes.bool,    
    toggle: PropTypes.func,
    article: PropTypes.any
};

export default withRouter(ModalUpdatePost);
