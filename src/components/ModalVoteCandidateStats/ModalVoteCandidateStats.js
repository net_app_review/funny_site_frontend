import React, {useEffect, useState} from 'react';
import { withRouter } from 'react-router'
import PropTypes from 'prop-types';
import style from './ModalVoteCandidateStats.sass'
import {MDBModal, MDBModalHeader } from 'mdbreact'
const ReactMarkdown = require('react-markdown')

const ModalVoteCandidateStats = props => {
   
    useEffect(() => {        
              
    }, []);

    // const onFormChange = (e, name) => {
    //     const value = e.currentTarget.value
    //     setStFormModel({
    //         ...stFormModel,
    //         [name]: value
    //     })
    // }
    

    // const onUploaded = (file) => {
    //     setStFile(file) 
    // }

    // const getRandomInt = (min, max) => {
    //     min = Math.ceil(min);
    //     max = Math.floor(max);
    //     return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
    //   }

    // function checkAssociatedTags () {

    // }

    // const tabToggle = (currentTab) => {
        
    // }    

    const {isOpen, onOk, toggle,candidate_info, ...other} = props
    return (
        <MDBModal isOpen={isOpen} toggle={toggle} {...other} className={style.modal_uploader}>
            <div className="d-flex flex-column pl-2 pr-2">
                <MDBModalHeader className="pt-2 pb-0 pl-0 pr-0 col-example text-center" toggle={toggle}>Candidate Info</MDBModalHeader>
                <ReactMarkdown source={candidate_info} />
                <button type="button" className="btn btn-dark" onClick={toggle}>Back</button>
            </div>                                           
        </MDBModal>
    );
};

ModalVoteCandidateStats.propTypes = {
    candidate_info: PropTypes.string,
    isOpen: PropTypes.bool,
    onOk: PropTypes.func,
    toggle: PropTypes.func,
};

export default withRouter(ModalVoteCandidateStats);
