import React, {useState}from "react";
import {   MDBContainer, MDBTabPane, MDBTabContent, MDBNav, MDBNavItem, MDBNavLink, MDBIcon } from "mdbreact";
import { useTranslation } from "react-i18next";

const MobilePopNav = () => {
  
  const [stActiveTab, setstLeftNavWidth] = useState("1");  
  const { t, i18n } = useTranslation();
  const toggleClassicTabs1 = tab => () => {
    if (stActiveTab !== tab) {
      setstLeftNavWidth({
        stActiveTab: tab
      });
    }
  };
  
    return (
      <MDBContainer className="classic-tabs">
          <MDBNav classicTabs color="orange" className="mt-5">
            <MDBNavItem>
              <MDBNavLink link to="/hot" active={ stActiveTab === "1"} onClick={toggleClassicTabs1("1")} >
                <MDBIcon icon="user" size="2x" />
                <br />
                {t("mobile.hot")}
              </MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink link to="/fresh" active={ stActiveTab === "2"} onClick={toggleClassicTabs1("2")} >
                <MDBIcon icon="heart" size="2x" />
                <br />
                {t("mobile.fresh")}
              </MDBNavLink>
            </MDBNavItem>
            <MDBNavItem>
              <MDBNavLink link to="/trending" active={ stActiveTab === "3"} onClick={toggleClassicTabs1("3")} >
                <MDBIcon icon="envelope" size="2x" />
                <br />
                {t("mobile.trending")}
              </MDBNavLink>
            </MDBNavItem>            
          </MDBNav>
          
        </MDBContainer>
    );
  }

export default MobilePopNav;