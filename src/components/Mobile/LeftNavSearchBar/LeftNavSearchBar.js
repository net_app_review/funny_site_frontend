import React, {useState,useRef,useEffect}from "react";
import { MDBIcon,
        MDBDropdown,
        MDBDropdownToggle,
        MDBDropdownMenu,
        MDBDropdownItem
} from "mdbreact";
import './LeftNavSearchBar.sass'
import { Redirect } from 'react-router-dom';

const LeftNavSearchBar = (props) => {
  
    const [stSearch, setstSearch] = useState('');
    const [stRedirect, setstRedirect] = useState(false);    
    const [stSearchObject, setstSearchObject] = useState('post');
    const searchInputRef = useRef(null);


    useEffect(() => {		
        setstSearch("")
    },[])


    const renderRedirect = () => {
        if (stRedirect) {
          return <Redirect to={{
                pathname: `/search/${stSearch}`,
                state: { 
                    detail: {
                        search_object: stSearchObject,
                        search_string: stSearch
                    } 
                }                
            }}
            key={stSearch}          
            />          
        }        
    } 
    

    const _handleSearchChange = (event) => {
      event.preventDefault();     
      setstSearch(event.target.value)         
    };

    const handleOnKeyDown=(event)=> {        
        // event.preventDefault();        
        if (event.keyCode === 13) {
            // event.preventDefault();                                  
            props.onClick()
            setstRedirect(true)
            // setstSearch("")      
        }
    }

    const handleOnSearchButton = (event) => {
        event.preventDefault();                                  
        props.onClick()
        setstRedirect(true)        
    }

    const searchForObject = (obj) => {
        setstSearch("")
        setstSearchObject(obj)
        searchInputRef.current.focus()
    }
  
    return (
        <div className="LeftNavSearchBar">
            {renderRedirect()}
            <div className="input-group search_form form-1">
                <MDBDropdown className='search_type_dropdown' aria-label='search_type'>
                    <MDBDropdownToggle className='_toggle' color="primary" aria-label='search_type'>
                        <MDBIcon icon="angle-down" />
                    </MDBDropdownToggle>
                    <MDBDropdownMenu className='search_dropdown_menu' basic>
                        {/* <TriangleDiv /> */}
                        <MDBDropdownItem className='search_for'>                            
                            Search for:                                                    
                        </MDBDropdownItem>
                        <MDBDropdownItem className='search_dropdown_item' onClick={(e) => searchForObject('post')}>
                            Posts
                        </MDBDropdownItem>
                        <MDBDropdownItem className='search_dropdown_item' onClick={(e) => searchForObject('tag')}>
                            Tags
                        </MDBDropdownItem>
                        <MDBDropdownItem className='search_dropdown_item' onClick={(e) => searchForObject('user')}>
                            Users
                        </MDBDropdownItem>
                    </MDBDropdownMenu>
                </MDBDropdown>
                <div className="input-group-prepend " onClick={(e) => {handleOnSearchButton(e)}} >
                    <span className="input-group-text search_button" id="basic-text1">
                        <MDBIcon className="text-white" icon="search" />
                    </span>
                </div>
                <input className="form-control search_input" 
                    type="text" 
                    placeholder={`Search for ${stSearchObject}...`}
                    aria-label="Search" 
                    value={stSearch} 
                    onChange={(e) => _handleSearchChange(e)}
                    onKeyDown={(e) => handleOnKeyDown(e)}
                    ref={searchInputRef}
                    />
            </div>
        </div>
    );
  }

export default LeftNavSearchBar;