import React, {useState, useEffect, Fragment } from "react";
// import ReactDOM from "react-dom";
import { MDBDropdownItem} from "mdbreact";
import "./UnseenNotification.sass"
// import { ActionCableConsumer, ActionCable } from 'react-actioncable-provider';
import {Link} from 'react-router-dom';
import {crudUserNotification} from '../../../services/crudUserNotification'
import PropTypes from 'prop-types'

const UnseenNotification = (props) => {

    const [stNotifications, setstNotifications] = useState([]);
    const [stNotiPage, setstNotiPage] = useState(1);        

    useEffect(() => {

        let run_count = 0;
        var counter = 100;

        const myFunction = () => {
            clearInterval(timer);
            counter += 100;
            run_count += 1;            
            if (run_count < 200){
                fetchUnseenNoti();                
            }        
            timer = setInterval(myFunction, counter);
        }

        let timer = setInterval(myFunction, counter)                

        return () => clearInterval(timer);

    }, [stNotiPage]);

    const loadNextPage = () => {
        setstNotiPage(stNotiPage + 1)
    }

    const fetchUnseenNoti = () => {
        crudUserNotification.getUnseenNoti(stNotiPage)
            .then(                
                response => {                    
                    if (response.data.status === 200 && response.data.data.length >= 0){
                        setstNotifications([...stNotifications, ...response.data.data])
                    }
                    checkUnseenNoti(response.data.data)
                    if (props.getUnseenNoti) props.getUnseenNoti(response.data.total_unseen)
                }
            )   
    }    

    const seeTheNotification = (e,id) => {        
        crudUserNotification.updateUnseenNoti(id, 'true').then(
            response => {
                if (response.data.id !== undefined && response.data.id !== 'undefined') {
                    // setstShowNoti('d-none')
                    fetchUnseenNoti()                    
                    if (props.fetchSeenArticle) props.fetchSeenArticle()
                }                
            }
        )
    }

    const checkUnseenNoti = (unSeenNoties) => {
        let containUnseen = false
        for (let i = 0; i < unSeenNoties.length ; i ++){
            if (unSeenNoties[i].iswatched === false ){
                containUnseen = true
            }
        }
        if (containUnseen === true) {
            if (props.showNewNoti)  {props.showNewNoti('inline')}
        } else {
            if (props.showNewNoti)  {props.showNewNoti('none')}
        }
    }

    const renderCreatorImage = (notification) => {        
        if (notification !== undefined && notification !== 'undefined'){
            if(notification.created_user !== null){
                return  <Link to={`/users/${notification.created_user.id}`} style={{padding: "0px"}} onClick={(e) => {seeTheNotification(e, notification.id)}}>
                            <img src={notification.created_user.image.url} alt="creator" className='creator_img'></img>
                        </Link>
            } else {
                return <></>
            }
        }        
    }

    const renderNotiContent =(notification) => {
        if (notification !== undefined && notification !== 'undefined'){
            if(notification.content !== null){
                return <div className='noti_text text-center flex-fill'>
                    <Link   to={{
                                    pathname:`/articles/${notification.article.uuid}`,
                                    state: {
                                        article_id: notification.articleID
                                    }                    
                                }}
                            style={{ padding: '0px', fontSize: '1em'}} 
                            onClick={(e) => {seeTheNotification(e, notification.id)}}>
                        {notification.created_user.name}
                        &nbsp;
                        {notification.content}
                        &nbsp;
                    </Link>
                </div>
            } else {
                return <></>
            }
        }
    }    

    const renderReferredObjectImage =(notification) => {
        if (notification !== undefined && notification !== 'undefined'){
            if(notification.article !== null){
                return  <Link   to={{
                                    pathname:`/articles/${notification.article.uuid}`,
                                        state: {
                                            article_id: notification.articleID
                                        }                    
                                    }}
                                style={{padding: "0px"}} key={notification.article.id}
                                onClick={(e) => {seeTheNotification(e, notification.id)}}>
                            <img src={notification.article.url} alt="noti_img" className='article_img'></img>
                            &nbsp;
                        </Link>
                    
            } else {
                return <></>
            }
        }
    }
    
    const renderSeeMoreButton = () => {
        if(stNotifications.length > 0) {
            return <div className='see_more_cmp' onClick={loadNextPage}>
                        <button className='btn see_more_btn'>
                            See more
                        </button>                
                    </div>
        }
    }

    return (
        <div className={`unseen_notification`} >                            
            {/* <ActionCableConsumer
                channel={{ channel: 'NotificationChannel' }}
                onReceived={handleReceivedNotification}
                // onReceived={handleReceivedNotification}
            />     */}
                {stNotifications.length > 0 && stNotifications.map((notification, index) => {
                    return (
                        <MDBDropdownItem key={index} className={`noti_content_button`}>
                            <div className='d-flex flex-row noti_content' key={index}>
                                {renderCreatorImage(notification)}
                                {renderNotiContent(notification)}
                                {renderReferredObjectImage(notification)}                            
                            </div>                        
                        </MDBDropdownItem>
                    )
                })}
            {renderSeeMoreButton()}
            
        </div>
    );
};

UnseenNotification.propTypes = {
    stNotifications: PropTypes.array
};

export default UnseenNotification;
