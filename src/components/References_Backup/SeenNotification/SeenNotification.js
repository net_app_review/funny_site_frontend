import React from "react";
// import ReactDOM from "react-dom";
import { MDBDropdownItem} from "mdbreact";
import "./SeenNotification.sass"
// import { ActionCableConsumer, ActionCable } from 'react-actioncable-provider';
import {Link} from 'react-router-dom';

import PropTypes from 'prop-types'

const SeenNotification = (props) => {    

    const renderCreatorImage = (notification) => {        
        if (notification !== undefined && notification !== 'undefined'){
            if(notification.created_user !== null){
                return  <Link to={`/users/${notification.created_user.id}`} style={{padding: "0px"}}>
                            <img src={notification.created_user.image.url} alt="creator" className='creator_img'></img>
                        </Link>
            } else {
                return <></>
            }
        }        
    }

    const renderNotiContent =(notification) => {
        if (notification !== undefined && notification !== 'undefined'){
            if(notification.content !== null){
                return <div className='noti_text text-center flex-fill'>
                    <Link to={{
                                pathname:`/articles/${notification.article.uuid}`,
                                state: {
                                    article_id: notification.articleID
                                }                    
                            }}
                          style={{ padding: '0px', fontSize: '1em'}}>
                        {notification.created_user.name}
                        &nbsp;
                        {notification.content} in a post you followed
                        &nbsp;
                    </Link>
                </div>
            } else {
                return <></>
            }
        }
    }
    
    const renderReferredObjectImage =(notification) => {
        if (notification !== undefined && notification !== 'undefined'){
            if(notification.article !== null){
                return  <Link to={{
                                    pathname:`/articles/${notification.article.uuid}`,
                                    state: {
                                        article_id: notification.articleID
                                    }                    
                                }} style={{padding: "0px"}} key={notification.article.id}>
                            <img src={notification.article.url} alt="noti_img" className='article_img'></img>
                            &nbsp;
                        </Link>
                    
            } else {
                return <></>
            }
        }
    }
    

    return (
        <div className={`Seen_notification`} >                            
            {/* <ActionCableConsumer
                channel={{ channel: 'NotificationChannel' }}
                onReceived={handleReceivedNotification}
                // onReceived={handleReceivedNotification}
            />     */}
                {props.stSeenNotifications.length > 0 && props.stSeenNotifications.map((notification, index) => {
                    return (
                        <MDBDropdownItem key={index} className='noti_content_button'>
                            <div className='d-flex flex-row noti_content'>
                                {renderCreatorImage(notification)}
                                {renderNotiContent(notification)}                                
                                {renderReferredObjectImage(notification)}                            
                            </div>
                        </MDBDropdownItem>
                    )
                })}
            <div className='see_more_cmp' onClick={props.loadNextPage}>
                <button className='btn see_more_btn'>
                    See more
                </button>                
            </div>
        </div>
    );
};

SeenNotification.propTypes = {
    stSeenNotifications: PropTypes.array
};

export default SeenNotification;
