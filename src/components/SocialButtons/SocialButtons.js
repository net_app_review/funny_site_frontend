import React from 'react';
import {MDBIcon, Container} from 'mdbreact'
import styles from './SocialButtons.module.sass'

const SocialButtons = props => {    
        
    return (        
        <Container className={styles.social_buttons} >            
            <div className={`d-flex flex-row pt-2 justify-content-center ${styles.button_group}`} >
                <a href="https://www.facebook.com/FunniqPage/" aria-label="facebook_page" target="_blank" rel="noopener noreferrer" className="flex-grow-1" >
                    <div className={`${styles.social_button} ${styles.facebook}`} >
                        <MDBIcon fab icon="facebook-f" />                            
                    </div>
                </a>
                
                <a href="https://twitter.com/FunniqPage" aria-label="twitter_page" target="_blank" rel="noopener noreferrer" className="flex-grow-1">
                    <div className={`${styles.social_button} ${styles.twitter}`}>                    
                        <MDBIcon fab icon="twitter" />                        
                    </div>
                </a>

                <a href="https://www.instagram.com/funniqpage/" aria-label="insta_page" target="_blank" rel="noopener noreferrer" className="flex-grow-1">
                    <div className={`${styles.social_button} ${styles.instagram}`}>                        
                        <MDBIcon fab icon="instagram" />                
                    </div>
                </a>

                <a href="https://www.linkedin.com/company/funniq" aria-label="linkedin_page" target="_blank" rel="noopener noreferrer" className="flex-grow-1">
                    <div className={`${styles.social_button} ${styles.linkedin}`}>                        
                        <MDBIcon fab icon="linkedin-in" />                        
                    </div>
                </a>
            </div>                                                     
        </Container>        
    );  
};
export default SocialButtons
