import React, {useState} from 'react';
import { MoonLoader } from 'react-spinners'; 
import LoginMoonLoaderCss from '../../config/ReactLoader/LoginMoonLoaderCss'

export default function LazyLoadPlaceholder() {

    const[stPlaceHolderLoading, setstPlaceHolderLoading] = useState(true)

  return (
    <div className="placeholder">
        <MoonLoader          
            css={LoginMoonLoaderCss}            
            size={20}
            color={'#fafafa'}
            loading={stPlaceHolderLoading}                        
        />
    </div>
  );
}