import React, {useState} from 'react';
import './SquareSearchBar.sass'
// import useUserAuthenForm from '../../authentication/useUserAuthenForm'
import {  withRouter} from 'react-router-dom'
import {    MDBIcon,
            MDBDropdown,
            MDBDropdownToggle,
            MDBDropdownMenu,
            MDBDropdownItem
        } from "mdbreact";
import TriangleDiv from '../../components/TriangleDiv/TriangleDiv'
import swal from 'sweetalert';
// import OutsideClicked from '../../components/OutsideClicked/OutsideClicked'

const SquareSearchBar = props => {    

    const [stSearch, setstSearch] = useState('');
    const [stSearchObject, setstSearchObject] = useState('post');    
    const [stIconOnSearch, setstIconOnSearch] = useState('');    

    // http://localhost:5054/api/v1/searchs/user?search[search_string]=a&search[page]=1
   
    const _handleSearchChange = (event) => {
      event.preventDefault();        
      setstSearch(event.target.value)            
    };

    const handleOnKeyDown=(event)=> {
        
        if (event.keyCode === 13) {
            event.preventDefault();
            redirectSearch()             
                    
        }
    }

    const handleOnSearchButton = (event) => {
        event.preventDefault();        
        redirectSearch()
    }

    const redirectSearch = () => {
        if (stSearch === '' || stSearch === undefined || stSearch=== 'undefined'){
            swal("Missing Info!","Please fill in search bar", "error")
        } else {
            props.history.push({
                pathname: `/search/${stSearchObject}`,
                state: { 
                    detail: {
                        search_object: stSearchObject,
                        search_string: stSearch
                    } 
                }
            })    
        }        
        setstSearch("")
    }

    const searchForObject = (obj) => {
        setstSearchObject(obj)
        switchIconOnSearch()
    }
    
    const switchIconOnSearch =() => {
        if (stIconOnSearch === ''){
            setstIconOnSearch('onSearch')
        } else {
            setstIconOnSearch('')
        }
        
    }

    // renderDropDownToggle = ()=> {

    // }

    return (
        <div className="SquareSearchBar">        
            <div className="input-group search_form form-1">                
                <input className="form-control search_input" 
                        type="text" 
                        placeholder={`Search for ${stSearchObject}...`}
                        aria-label="Search" 
                        value={stSearch} 
                        onChange={(e) => _handleSearchChange(e)}
                        onKeyDown={(e) => handleOnKeyDown(e)}
                        />
                 <button className="input_group_prepend" onClick={(e) => {handleOnSearchButton(e)}} aria-label='on_search_btn'>
                    <MDBIcon className="text-white" icon="search" />
                </button>
                
                    <MDBDropdown className='search_type_dropdown'>
                        
                            <MDBDropdownToggle color="primary" onClick={switchIconOnSearch} aria-label='search_type'>
                                <MDBIcon icon="angle-down" className={stIconOnSearch}/>
                            </MDBDropdownToggle>
                        
                        
                            <MDBDropdownMenu className='search_dropdown_menu' right basic>
                                <TriangleDiv />
                                <MDBDropdownItem className='search_for'>                            
                                    Search for:                                                    
                                </MDBDropdownItem>                        
                                <MDBDropdownItem className='search_dropdown_item' onClick={(e) => searchForObject('post')}>
                                    Posts
                                </MDBDropdownItem>
                                <MDBDropdownItem className='search_dropdown_item' onClick={(e) => searchForObject('tag')}>
                                    Tags
                                </MDBDropdownItem>
                                <MDBDropdownItem className='search_dropdown_item' onClick={(e) => searchForObject('user')}>
                                    Users
                                </MDBDropdownItem>
                            </MDBDropdownMenu>                        
                    </MDBDropdown>
                
            </div>
        </div>
    );  
};

SquareSearchBar.propTypes = {

};

export default withRouter( SquareSearchBar);
