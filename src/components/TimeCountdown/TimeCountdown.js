import React from "react";
// import ReactDOM from "react-dom";
import Countdown from "react-countdown";

const Completionist = () => <span className="count_down">Time is up!, the result is as below</span>;

const renderer = ({ hours, minutes, seconds, completed }) => {
if (completed) {
    // Render a completed state
    return <Completionist />;
} else {
    // Render a countdown
    return <span className="count_down">{hours}:{minutes}:{seconds}</span>;
}
};

const TimeCountdown = (props) => {

    return (
        <>
            <Countdown
            date={ props.remainingTime}
            // date={Date.now() + 3600}
            renderer={renderer}
            />
        </>
    );
};

Countdown.propTypes = {

};

export default TimeCountdown;
