import React from "react";
// import ReactDOM from "react-dom";
// import {Link} from 'react-router-dom'
// import {userRankService} from "../../services/userRankService";
// import { MDBContainer } from "mdbreact";
import PropTypes from 'prop-types'
import "./UserNationalFlag.sass"
import Flag from 'react-world-flags'
import {countries} from 'country-data';

const UserNationalFlag = (props) => {

    // const [stLevel, setstLevel] = useState(props.level);
    
    const UserNationalFlag = () =>  {
        // props.comment.user.country_code        
        if (props.country_code) {            
            let link_to_wiki = "https://en.wikipedia.org/wiki/" + countries[props.country_code].name.replace(/ /g, '_')
            return (
                <div className="user_national_flag d-flex flex-column" >
                    <a href={`${link_to_wiki}`} target="_blank" rel="noopener noreferrer" className='mt-auto'>
                        <Flag code={props.country_code} height={props.height} />                                                                
                    </a>                    
                </div>
            )            
        } else {
            return (<></>)
        }        
    }


    return (<>
    {
        UserNationalFlag()
    }
        
    </>
    
    );
};

UserNationalFlag.propTypes = {
    country_code:  PropTypes.string,
    height: PropTypes.string
};

export default UserNationalFlag;
