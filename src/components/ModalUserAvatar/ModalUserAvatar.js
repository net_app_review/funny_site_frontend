import React, { useState, useContext} from 'react';
import PropTypes from 'prop-types';
import style from './ModalUserAvatar.module.sass'
import {
    MDBModal,
    MDBModalHeader,
    
    MDBBtn
} from "mdbreact";
import { css } from '@emotion/core';
import {CurrentUserContext} from '../../Store'
import { BarLoader } from 'react-spinners'; 
import Avatar from 'react-avatar-edit'
import {StorageService} from '../../services/storageService'
import apiClient from "../../config/apiClient";
import styles from './ModalUserAvatar.module.sass'
import { useTranslation } from "react-i18next";
import swal from 'sweetalert'
// const FORM_FIELDS = {
//     TITLE: 'title',
//     FROM_URL: 'from_url',
//     CATEGORY: 'category'
// }

const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

const ModalUserAvatar = props => {    

    const [stLoadingBar, setstLoadingBar] = useState(false);
    const [stAvatarFile, setstAvatarFile] = useState();
    const [stPreview, setstPreview] = useState(null);
    const [stSrc, setstSrc] = useState('');
    const { t, i18n } = useTranslation();

    const onBeforeFileLoad = (elem) => {
        if(elem.target.files[0].size > 10000000 ){
          alert("File is too big! Max image size is 10 MB");
          elem.target.value = "";
        };
      }

      const handleUpdateAvatar = (e) => {
        setstLoadingBar(true)
        e.preventDefault();            
        // TODO: do something with -> this.state.file  
        StorageService.upload(stAvatarFile, 'user' + stCurrentUser.id + '-' + Date.now())
                      .then(fileSnapshot => {
                          fileSnapshot.ref.getDownloadURL()
                              .then(downloadLink => {
                                  // setStDownloadLink(downloadLink)                            
                                  if (downloadLink) {
      
                                      const img = new Image()
                                      img.onload = (e) => {
                                        const width = e.path[0].width
                                        const height = e.path[0].height
                                        
                                        let data = {
                                          userprofile: {
                                            image_url: downloadLink,
                                            image_height: width,
                                            image_width: height,                                        
                                          }
                                      }
      
                                        apiClient.post(`${process.env.REACT_APP_API_URL}/api/v1/userprofiles/update_image`, data)
                                          .then(response => {                                        
                                            if (response.status === 200){                                          
                                            setstLoadingBar(false)
                                            window.location.reload()
                                            }
                                          })
                                          .catch(error => swal("Error", `${error}, please contact us at https://facebook.com/FunniqPage`, "error"))                                
                                        }
                                      img.src = URL.createObjectURL(stAvatarFile)
                                  }
                              })
                      })
      }

      const dataURItoBlob = (dataURI) => {
        // convert base64 to raw binary data held in a string
        // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
        var byteString = atob(dataURI.split(',')[1]);
      
        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
      
        // write the bytes of the string to an ArrayBuffer
        var ab = new ArrayBuffer(byteString.length);
      
        // create a view into the buffer
        var ia = new Uint8Array(ab);
      
        // set the bytes of the buffer to the correct values
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
      
        // write the ArrayBuffer to a blob, and you're done
        var blob = new Blob([ab], {type: mimeString});
        return blob;
      
      } 

    const onClose = () => {
        // this.setState({preview: null})
        setstPreview(null)
        setstAvatarFile(null)
      }
      
      const onCrop = (preview) => {    
        setstPreview(preview)        
        setstAvatarFile(dataURItoBlob(preview))
      }
    
    const {isOpen, onOk, toggle, ...other} = props

    const [stCurrentUser, ] = useContext(CurrentUserContext)

    return (
    <MDBModal isOpen={isOpen} toggle={toggle} {...other} className={style.modal_uploader}>
        <div className="d-flex flex-column pl-2 pr-2 justify-content-center">            
            <MDBModalHeader className="pt-2 pb-0 pl-0 pr-0 col-example text-center" toggle={toggle}>{t("userSetting.avatar")}</MDBModalHeader>

                <div className={style.upload_avatar}>
                  <Avatar
                    width={300}
                    height={250}
                    onCrop={onCrop}
                    onClose={onClose}
                    onBeforeFileLoad={onBeforeFileLoad}
                    src={stSrc}
                    className={styles.AvatarFix}
                  />   
                </div>                                                                                                       
            
                <img src={stPreview} alt="Preview" className={`${styles.AvatarPreview} ${stPreview === null || stPreview === 'null' ? 'd-none' : ''}`} />

          <BarLoader
            css={override}
            sizeUnit={"px"}
            width={"200"}
            color={'#6495ED'}
            loading={stLoadingBar}
          /> 
          <br></br>                                    
          <h6 className="text-center">
            {t("userSetting.maximum")}
          </h6>                                      
          <MDBBtn color="dark-green" className="mb-4" onClick={(e)=>handleUpdateAvatar(e)}>
            {t("userSetting.upload")}
          </MDBBtn>                    
        </div>                                           
    </MDBModal>
    );
};

ModalUserAvatar.propTypes = {
    isOpen: PropTypes.bool,
    onOk: PropTypes.func,
    toggle: PropTypes.func,
};

export default ModalUserAvatar;
