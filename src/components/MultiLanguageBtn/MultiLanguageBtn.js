import React, { useState } from "react";
// import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import {MDBIcon} from 'mdbreact'
import styles from './MultiLanguageBtn.module.sass'
// import Dropdown from "react-bootstrap/Dropdown";
// import Select from 'react-select'
// import Table from 'react-bootstrap/Table'

const options = [
    { value: 'bra', label: 'Brazilian', flag_url: 'https://firebasestorage.googleapis.com/v0/b/funniq-78dbd.appspot.com/o/funniq_development%2Flongname-circle_brazil-256.webp?alt=media&token=c464aa9c-77f2-44f4-aa3a-32884b92a922'},
    { value: 'en', label: 'English', flag_url: 'https://firebasestorage.googleapis.com/v0/b/funniq-78dbd.appspot.com/o/funniq_development%2FlongnameFlag_of_United_Kingdom_-_Circle-512.webp?alt=media&token=550e9a85-fe72-4c66-ad42-a74dc0161f17'},
    { value: 'ge', label: 'German', flag_url: 'https://firebasestorage.googleapis.com/v0/b/funniq-78dbd.appspot.com/o/funniq_development%2Flongname-Germany-Flag.png?alt=media&token=92f1dbab-e691-4fd5-98fc-d53ad8f3cb3a'},
    { value: 'sp', label: 'Spanish', flag_url: 'https://firebasestorage.googleapis.com/v0/b/funniq-78dbd.appspot.com/o/funniq_production%2Fu29-84-1597748049901?alt=media&token=8b8e071c-e783-40dd-8f00-5bd065f08043'}
]

const MultiLanguageBtn = props => {

    const { t, i18n } = useTranslation();
    const [stUserStatDropdown, setstUserStatDropdown] = useState("0vh")    
    const [stLanguage, setstLanguage] = useState(options[1].label)    
    const [stFlag, setstFlag] = useState(options[1].flag_url)
    const [stRotation, setstRotation] = useState('')

    function handleClick(lang) {        
        if (lang === 'ge') {
            setstLanguage(options[2].label)
            setstFlag(options[2].flag_url)
        } else if (lang === 'sp') {
            setstLanguage(options[3].label)
            setstFlag(options[3].flag_url)
        } else if (lang === 'bra') {
            setstLanguage(options[0].label)
            setstFlag(options[0].flag_url)
        } else {
            setstLanguage(options[1].label)
            setstFlag(options[1].flag_url)
        }
        setstUserStatDropdown('0vh')
        if (props.collapseCategory) props.collapseCategory()
        setstRotation('')
        i18n.changeLanguage(lang);
    }
    const switchDropdownStatus = () => {
        if (stUserStatDropdown === "0vh"){
            setstUserStatDropdown("20vh")
        } else {
            setstUserStatDropdown("0vh")
        }

        if (stRotation === ''){
            setstRotation(`${styles._onRotate_}`)
        } else {
            setstRotation('')
        }

        if (props.collapseCategory) props.collapseCategory()
    }

    return (
        <div className={styles.TagDropdown}>
            {/* <Select options={options} color='green'  onChange={handleClick} /> */}            
            <div className={`${styles.dropdown_button}`} onClick={switchDropdownStatus}>                
                <img src={stFlag}
                    className={styles.country_flag}
                    alt='_flag_img'></img>
                <div className={styles.country_lang}>
                    {stLanguage}
                </div>
                <div className={styles.navigation} >
                    <MDBIcon icon="angle-right" className={`${styles.nav_icon_} ${stRotation}`} />
                </div>
            </div>
            <div className={styles.dropdown_content} style={{height: `${stUserStatDropdown}`}}>
                <div onClick={() => handleClick('en')} className={`${styles.language_btn}`}>
                    <p>English</p>
                    <img src={`${options[1].flag_url}`}
                        className={styles.flag}
                        alt='_flag_img'
                        ></img>
                </div>
                <div onClick={() => handleClick('bra')} className={`${styles.language_btn}`}>
                    <p>Brazilian</p>
                    <img src={`${options[0].flag_url}`}
                        className={styles.flag}
                        alt='_flag_img'
                        ></img>
                </div>
                <div onClick={() => handleClick('ge')} className={`${styles.language_btn}`}>
                    <p>German</p>
                    <img src={`${options[2].flag_url}`}
                        className={styles.flag}
                        alt='_flag_img'
                        ></img>
                </div> 
                <div onClick={() => handleClick('sp')} className={`${styles.language_btn}`}>
                    <p>Spanish</p>
                    <img src={`${options[3].flag_url}`}
                        className={styles.flag}
                        alt='_flag_img'
                        ></img>
                </div>                             
            </div>             
        </div>
      )

}

export default MultiLanguageBtn;