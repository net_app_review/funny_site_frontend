import React from 'react';
import PropTypes from 'prop-types';
import styles from './LoadMoreButton.module.sass'
import DefaultMoonLoaderCss from '../../config/ReactLoader/DefaultMoonLoaderCss'
import { MoonLoader } from 'react-spinners'; 

const LoadMoreButton = props => {

    return (                
        <button className={styles.see_more_button} onClick={props.loadMore} style={{display: `${props.isShown}`}} >
            {props.displayedText}
            <MoonLoader          
                css={DefaultMoonLoaderCss}            
                size={16}
                color={'green'}
                loading={props.stIsLoading}                        
            />
        </button>        
    );
};

LoadMoreButton.propTypes = {
    isShown: PropTypes.any,
    stIsLoading: PropTypes.bool,
    loadMore: PropTypes.func,
    displayedText: PropTypes.string
};

export default LoadMoreButton;