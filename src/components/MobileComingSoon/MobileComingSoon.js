import React from "react";
import './MobileComingSoon.sass'
// import { useTranslation } from "react-i18next";
import swal from 'sweetalert'

const MobileComingSoon = () => {
  
    const comingSoon = () =>{
        swal("Coming soon", "We are testing and validating the app, it will be available for download anytime soon");
    }

    return (
        <div className="mobile_coming_soon">
            <div className='noti'>
                Coming soon !
            </div>
            <div className='app_cmp'>
                <div className='gg_app'>
                    <img    src='https://firebasestorage.googleapis.com/v0/b/funniq-78dbd.appspot.com/o/Capture_gg.PNG?alt=media&token=b5d9cad4-c1bf-42ee-a4e5-3d9acd1a98de' 
                            alt='gg_app'
                            onClick={comingSoon}
                            >
                    </img>
                </div>  
                <div className='iphone_app'>
                    <img src='https://firebasestorage.googleapis.com/v0/b/funniq-78dbd.appspot.com/o/Capture_apple.PNG?alt=media&token=288f4bf0-19b5-4e8e-8a7a-6630078a604c' 
                         alt='apple_app' 
                         onClick={comingSoon}
                         />
                </div>  
            </div>
          
        </div>
    );
  }

export default MobileComingSoon;