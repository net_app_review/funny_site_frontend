import React from "react";
import './CompanyInfo.sass'
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

const CompanyInfo = (props) => {

    const doNothing = () => {        
    }
      
    return (
        <div className="d-flex flex-wrap company_info mt-1 " style={{ width: '100%'}}>            
            <div className="p-0  pr-1">
                <Link to={`/about/rules`} onClick={props.navonClick ? props.navonClick : doNothing }>
                    <div className="p-0">Site Rules |</div>
                </Link>
            </div>
            <div className="p-0  pr-1">
                <Link to={`/about/privacy`} onClick={props.navonClick ? props.navonClick : doNothing }>
                    <div className="p-0">Privacy |</div>                                        
                </Link>
            </div>
            <div className="p-0 pr-1 ">
                <Link to={`/about/faqs`} onClick={props.navonClick ? props.navonClick : doNothing }>
                    <div className="p-0">FAQs |</div>    
                </Link>
            </div>
            <div className="p-0  pr-1">
                <Link to={`/about/terms`} onClick={props.navonClick ? props.navonClick : doNothing }>
                    <div className="p-0">Term of Service |</div>                                        
                </Link>
            </div>            
            <div className="p-0  pr-1">
                <Link to={`/about/donate`} onClick={props.navonClick ? props.navonClick : doNothing }>
                    <div className="p-0">Help Us</div>                                        
                </Link>
            </div>
        </div>
    );
  }

CompanyInfo.propTypes = {    
    width: PropTypes.string,
    navonClick: PropTypes.func,
    categoryList: PropTypes.array
};

export default CompanyInfo;