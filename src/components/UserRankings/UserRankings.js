import React, {useState, useEffect } from "react";
// import ReactDOM from "react-dom";
import {Link} from 'react-router-dom'
import {userRankService} from "../../services/userRankService";
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";
import "./UserRankings.sass"
import UserLevelBadge from '../../components/UserLevelBadge/UserLevelBadge'
import { MoonLoader } from 'react-spinners'; 
import LoginMoonLoaderCss from '../../config/ReactLoader/LoginMoonLoaderCss'
import swal from 'sweetalert'

const UserRankings = (props) => {

    const [stPage, setStPage] = useState(1);
    // const [stRankOverflowY, setstRankOverflowY] = useState("hidden");
    const [stUserRankings, setstUserRankings] = useState([]);
    const [stMoonLoader, setstMoonLoader] = useState(false);

    useEffect(() => {           
        fetchUserRankings()
    }, [stPage]);

    const fetchUserRankings = () => {
        setstMoonLoader(true)
        userRankService.getuserRankings(stPage)
            .then(result => {                                
                if (result.data) {
                    setstUserRankings([...stUserRankings, ...result.data.rankings])
                }
                
                setstMoonLoader(false)
            }).catch(error => {
                setstMoonLoader(false)
                swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
            })
    }

    const loadMoreRankingUsers = () => {
        setStPage(stPage + 1)
        // setstRankOverflowY("scroll")
    }

    return (
        <MDBContainer className="user_rankings_child">
            <MDBRow className="title">
                <Link to={`/rankings`} >
                    Ranking
                    {/* <div className='sup_info'>
                        i
                    </div> */}
                </Link>            
                
            </MDBRow>
            {stUserRankings.length > 0 && stUserRankings.map((user, index) => {
                return (
                    <MDBRow className="user_info" key={index}>
                        <MDBCol size="1" className="user_index">
                            {index + 1}
                        </MDBCol >
                        <MDBCol size="2" className="user_image img-fluid">
                            <Link to={`/users/${user.uuid}-${user.slug}`}>
                                <img src={user.image.url} alt={user.name}></img>                                
                            </Link>
                        </MDBCol>
                        <MDBCol size="9" className="user_stats">
                            <MDBRow className="name">
                                <Link to={`/users/${user.uuid}-${user.slug}`} >                                
                                    {user.name}
                                </Link>
                                &nbsp;                                
                                <UserLevelBadge level={user.level} height="1.2em"/>
                            </MDBRow>
                            <MDBRow className="points">
                                {user.points}
                                &nbsp;
                                pts
                            </MDBRow>                            
                        </MDBCol>                          
                    </MDBRow>
                )
            })}
            <MDBRow className="see_more">
                <button className="btn see_more_btn" onClick={loadMoreRankingUsers}     >
                    See more                    
                    <MoonLoader          
                        css={LoginMoonLoaderCss}            
                        size={20}
                        color={'#fafafa'}
                        loading={stMoonLoader}                        
                        />
                </button>
                {/* <button className="btn see_more_btn" onClick={loadMoreRankingUsers} style={{ "overflow-y": stRankOverflowY}}>
                    less
                </button> */}
            </MDBRow>       
        </MDBContainer>
    );
};

UserRankings.propTypes = {

};

export default UserRankings;
