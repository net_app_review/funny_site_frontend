import React, {useContext, useRef, useState} from 'react'
import axios from 'axios'
import { MDBContainer, MDBModal, MDBModalBody, MDBModalHeader} from 'mdbreact'
import {CurrentUserContext, SignUpModalContext, LoggedInContext, SignInModalContext} from '../../Store'
import './UserLogin.sass'
import FacebookAuth from './FacebookAuth/FacebookAuth'
import GoogleAuth from './GoogleAuth/GoogleAuth'
import Cookies from 'js-cookie'
import swal from 'sweetalert';
import LinedText from '../LinedText/LinedText'
import {Link} from 'react-router-dom'
import AgeConfirmOverlay from '../../components/AgeConfirmOverlay/AgeConfirmOverlay'
import {firestoreService} from '../../services/firestoreService'
import { MoonLoader } from 'react-spinners';
import LoginMoonLoaderCss from '../../config/ReactLoader/LoginMoonLoaderCss'

const UserSignIn = () => {
  
  const [, setSignInModal] = useContext(SignInModalContext)
  const [SignUpModal, setSignUpModal] = useContext(SignUpModalContext)
  const [, setCurrentUser] = useContext(CurrentUserContext)    
  const [, setLoggedIn] = useContext(LoggedInContext)
  const refSubmit = useRef(null);
  // const refEmail = useRef(null);
  const [stSignUpForm, setstSignUpForm] = useState({
    username: '',
    email: '',
    password: '',
    password_confirmation: ''
  });
  const [stIsAdult, setstIsAdult] = useState(true);
  const [stMoonLoading, setstMoonLoading] = useState(false);
  
const signup = () => {
  setstMoonLoading(true)
  axios.post(`${process.env.REACT_APP_API_URL}/api/v1/registrations`, {
        user: {
          name: stSignUpForm["username"],
          email: stSignUpForm["email"],
          password: stSignUpForm["password"],
          password_confirmation: stSignUpForm["password"]
        }
      }
      )
      .then(response => {        
        if (response.data.is_success === true){          
          setCurrentUser(response.data.data.user)
          Cookies.set('AuthToken', response.data.data.user.authentication_token);
          swal("Successful", "You have signed up successfully with Funniq, have fun.", "success");                      
          setLoggedIn("LOGGED_IN")
          setSignUpModal(false)
          firestoreService.createNotiChannelForUser(response.data.data.user.id)
          swal("Successful!","Welcome to Funniq, we hope that you will have the funniest and most relaxed time, we will refresh and get the most updated cosplay contents for you", "success")
            .then((value) => {
              if (value === true){
                window.location.reload();              
              }                  
            });
            setstMoonLoading(false)
        }
      })
      .catch(error => {          
        alert(error)
        if (error.response.status === 422) {
          swal("Error", "Something is wrong, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com", "error");                                
        } else if (error.response.status === 500) {
          swal("Internal Server Error", "Server error, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com", "error");                                
        } else {
          swal("Internal Server Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
        }
        setstMoonLoading(false)
      })      
}

// ---------------------------------------- handle Sign up form actions ----------------------------------- //

const _handleUsernameChange = (event) => {
  event.preventDefault();        
  // ststSignUpForm(event.target.value)
  setstSignUpForm({
    ...stSignUpForm,
    username: event.target.value
  })
};

const _handleEmailChange = (event) => {
  event.preventDefault();        
  // ststSignUpForm(event.target.value)
  setstSignUpForm({
    ...stSignUpForm,
    email: event.target.value
  })
};

const _handlePasswordChange = (event) => {
  event.preventDefault();
  setstSignUpForm({
    ...stSignUpForm,
    password: event.target.value
  })       
  // setstPassword(event.target.value)                
};

const _handleSignUpSubmit = (event) => {
  if (event) {
    event.preventDefault();
    if(!stIsAdult){
      swal("Error!","Sorry, you need to confirm that your age is over 13 to continue", "error")
      return
    }
    signup()
    setstSignUpForm({
      ...stSignUpForm,
      username: "",
      email: "",
      password: "",
      password_confirmation: ""
    })
  }               
};

const switchToLogin = () => {
  setSignUpModal(false)
  setSignInModal(true)  
}

const checkIsAdult = (event) => {
  // event.preventDefault()        
  setstIsAdult(event.target.checked)
}


    return (
      
      <MDBContainer className="login-cmp">      
      <MDBModal isOpen={SignUpModal} size='sm'>
        <MDBModalHeader toggle={() => setSignUpModal(!SignUpModal)} className="login_header"></MDBModalHeader>
        <MDBModalBody>

          <form onSubmit={_handleSignUpSubmit}>            
            <p className="h4 text-center mb-4">Sign in</p>            
            {/* <p className="text-center mb-4">
              Facebook Login is under maintainance, sorry for this inconvenience, please try google login
            </p> */}
            {/* <label htmlFor="defaultFormLoginEmailEx" className="grey-text">
              Your user name:
            </label> */}
            <input
              type="text"
              id="defaultFormLoginEmailEx"
              className="form-control"
              name="name"
              placeholder="Username"
              value={stSignUpForm.username}
              onChange={_handleUsernameChange} 
              required
            />
            
            {/* <label htmlFor="defaultFormLoginEmailEx" className="grey-text">
              Your email
            </label> */}
            <input
              type="email"
              id="defaultFormLoginEmailEx"
              className="form-control"
              name="email"
              placeholder="Email"              
              value={stSignUpForm.email}
              onChange={_handleEmailChange}
              required
            />            
            {/* <label htmlFor="defaultFormLoginPasswordEx" className="grey-text">
              Your password
            </label>             */}
            <input
              type="password"
              id="defaultFormLoginPasswordEx"
              className="form-control"
              minLength="6"
              name="password"    
              placeholder="Password"            
              value={stSignUpForm.password}
              onChange={_handlePasswordChange}
              required
            />            
             <div className="text-center mt-0"> 
                {/* <MDBBtn color="indigo" className="sign_up_btn" type="submit">Sign Up</MDBBtn>  */}
                <button type="submit" className="btn Login_btn" hidden ref={refSubmit}>Register</button>
            </div>
          </form>
                
          <div className ='accept_tos_policy'>
            By registering, I accept the Funniq 
              <Link to={`/about/terms`} className='tos' onClick={() => setSignUpModal(!SignUpModal)} >
                Term of Service 
              </Link>
              and acknowledge the
              <Link to={`/about/privacy`} className='tos' onClick={() => setSignUpModal(!SignUpModal)} >
                Privacy Policy
              </Link>                          
          </div>
          <div className="adult_check_container">
            <div className={`custom-control custom-checkbox is_adult_check`}>
                <input type="checkbox" className="custom-control-input" id="defaultUnchecked" onClick={(event) => checkIsAdult(event)} checked={stIsAdult} />
                <label className="custom-control-label" htmlFor="defaultUnchecked">I am older than 13</label>
            </div>
          </div>          
          <div className="d-flex flex-column px-0 py-2">
            <button type="button" type="submit" className="btn Login_btn" onClick={() => refSubmit.current.click()}>
              Register
              &nbsp;
              <MoonLoader          
                css={LoginMoonLoaderCss}            
                size={20}
                color={'#fafafa'}
                loading={stMoonLoading}                        
              />
            </button>
            {/* <button type="button" className="btn signup_cancel_up_btn flex-fill" onClick={() => setSignUpModal(!SignUpModal)} >Cancel</button> */}
          </div>

          <LinedText passedText='or' />

          <div className="social-buttons-group px-0 py-2 mb-2">
            <FacebookAuth/>
            <GoogleAuth/>
            <AgeConfirmOverlay isShown={stIsAdult ? 'd-none' : ''}
                                   message="Sorry, you need to confirm that your age is over 13 to continue"
                />
          </div>
          <div className='sign_up_now'>
            <button onClick={switchToLogin} className='sign_up_switch btn'>
              Already have an account? Login
            </button>
          </div>  
          </MDBModalBody>
      </MDBModal>
      </MDBContainer>
    );
  }


export default UserSignIn;