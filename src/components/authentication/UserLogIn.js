import React, {useContext, useState, useEffect} from 'react'
import axios from 'axios'
import { MDBContainer, MDBModal, MDBModalBody, MDBModalHeader} from 'mdbreact'
import {LoggedInContext, SignInModalContext, SignUpModalContext, CurrentUserContext} from '../../Store'
import FacebookAuth from './FacebookAuth/FacebookAuth'
import GoogleAuth from './GoogleAuth/GoogleAuth'
import './UserLogin.sass'
import {Link} from 'react-router-dom'
import { MoonLoader } from 'react-spinners'; 
import LoginMoonLoaderCss from '../../config/ReactLoader/LoginMoonLoaderCss'
import Cookies from 'js-cookie'
import swal from 'sweetalert';
import LinedText from '../LinedText/LinedText'
import AgeConfirmOverlay from '../../components/AgeConfirmOverlay/AgeConfirmOverlay'

const UserLogIn = (props) => {

  const [SignInModal, setSignInModal] = useContext(SignInModalContext)
  const [, setSignUpModal] = useContext(SignUpModalContext)
  const [LoggedIn, setLoggedIn] = useContext(LoggedInContext)
  const [stEmail, setstEmail] = useState("")
  const [stPassword, setstPassword] = useState("")
  const [stUserLoading, setstUserLoading] = useState(false);
  const [, setstCurrentUser] = useContext(CurrentUserContext)
  const [stIsAdult, setstIsAdult] = useState(true);
  const [stAgeConfirmOverlay, setstAgeConfirmOverlay] = useState('d-none')
  
const switchToSignUp = () => {
  setSignInModal(false)
  setSignUpModal(true)
}

useEffect(() => {
}, [LoggedIn]);

const login = () => {
  if(!stIsAdult){
    swal("Error!","Sorry, you need to confirm that your age is over 13 to continue", "error")
    return
  }
  setstUserLoading(true)
  
  axios.post(`${process.env.REACT_APP_API_URL}/api/v1/sign_in`, {
    sign_in: {
      email: stEmail,
      password: stPassword
    }
  }).then(response => {
    if (response.status === 200){      
      setstCurrentUser(response.data.data.user)
      Cookies.set('AuthToken', response.data.data.user.authentication_token);
      setLoggedIn("LOGGED_IN")
      setstUserLoading(false)      
      setSignInModal(false)      
      swal("Successful!","Welcome to Funniq, we hope that you will have the funniest and most relaxed time, we will refresh and get the most updated cosplay content for you", "success")
      .then((value) => {
        if (value === true){
          window.location.reload();              
        }                  
      });
    }
  })
  .catch(error => {        
    if (error.response.status === 403) {
      swal("Account is inactive!", "Your account is inactive, please contact us at facebook.funniq.com or send email to support@funniq.com for support", "error");
      setstUserLoading(false)
    } else {
      swal("Something is wrong", "Either your email or password is incorrect, please check again or contact support@funniq.com", "error");                  
      setstUserLoading(false)
    }
  })  
}

// ---------------------------------------- handle Emails input actions ----------------------------------- //

const _handleEmailChange = (event) => {
  event.preventDefault();        
  setstEmail(event.target.value)                
};

const _handlePasswordChange = (event) => {
  event.preventDefault();        
  setstPassword(event.target.value)                
};

const _handleLoginSubmit = (event) => {
  if (event) {
    event.preventDefault();      
    login ()          
    setstEmail("")
    setstPassword("")
  }               
};

const checkIsAdult = (event) => {
  // event.preventDefault()        
  setstIsAdult(event.target.checked)
  if(event.target.checked){
    setstAgeConfirmOverlay('d-none')
  } else {
    setstAgeConfirmOverlay('')
  }
}

    return (      
      <MDBContainer className="login-cmp">          
        <MDBModal isOpen={SignInModal} toggle={true} size="sm">    
          <MDBModalHeader className="login_header" toggle={() => setSignInModal(!SignInModal)}></MDBModalHeader>
          <MDBModalBody>
          {/* <div className="loading_overlay"></div> */}
            <form onSubmit={(e) => _handleLoginSubmit(e)} className="mt-0">
              <p className="h4 text-center mb-2">Log in</p>
                <input
                  type="email"
                  id="defaultFormLoginEmailEx"
                  className="form-control mt-1"

                  name="email"
                  placeholder="Email"
                  value={stEmail}
                  onChange={_handleEmailChange}
                  required
                />              
                <input
                  type="password"
                  id="defaultFormLoginPasswordEx"
                  className="form-control mt-2"
                  name="password"    
                  placeholder="Password"            
                  value={stPassword}
                  onChange={_handlePasswordChange}
                  required
                />
              <div className="adult_check_container">
                <div className={`custom-control custom-checkbox is_adult_check`}>
                  <input type="checkbox" className="custom-control-input" id="defaultUnchecked" onClick={(event) => checkIsAdult(event)} checked={stIsAdult} />
                  <label className="custom-control-label" htmlFor="defaultUnchecked">I am older than 13</label>
                </div>
              </div>              
              <div className="text-right mt-2">              
                <button type="button" type='submit' className="btn Login_btn">                  
                  Log in
                  &nbsp;
                  <MoonLoader          
                    css={LoginMoonLoaderCss}            
                    size={20}
                    color={'#fafafa'}
                    loading={stUserLoading}                        
                  />                  
                </button>
                {/* <button type="button" className=" btn cancel_btn" onClick={() => setSignInModal(!SignInModal)} >Cancel</button>               */}
              </div>
              <div className='forgot_password text-center my-2'>
                <Link to={`/password_forgot`} 
                      style={{ 
                        textDecoration: 'none',
                        fontSize: '1rem',
                        fontWeight: 400,
                        color: '#318f31'
                      }}
                      onClick={() => {setSignInModal(false)}}>
                  Forgot password
                </Link>
                {/* <div className='or text-center text-grey'>
                  Or
                </div> */}
                <LinedText passedText='or' />
              </div>            
            </form>
              <div className="social-buttons-group">
                <FacebookAuth isAdult={stIsAdult}/>
                <GoogleAuth isAdult={stIsAdult}/>
                <AgeConfirmOverlay isShown={stAgeConfirmOverlay}
                                   message="Sorry, you need to confirm that your age is over 13 to continue"
                />
              </div>
              <div className='sign_up_now'>
                <button onClick={switchToSignUp} className='sign_up_switch btn'>
                  Sign up for an account
                </button>
              </div>

              <div className='privacy_policy'>
                <Link to={`/about/privacy`}  onClick={() => setSignInModal(!SignInModal)}>
                  &#x2027;
                  Privacy Policy
                </Link>
                <Link to={`/about/rules`}  onClick={() => setSignInModal(!SignInModal)} className='pl-1'>
                  &#x2027;
                  Site Rules
                </Link>         
              </div>            
          </MDBModalBody>                  
        </MDBModal>
      </MDBContainer>
    );

}

export default UserLogIn
