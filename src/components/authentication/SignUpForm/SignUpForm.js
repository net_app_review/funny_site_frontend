import React, { Component, useContext, useState } from 'react';
import FormField from '../../FormField/FormField';
import EmailField from '../../EmailField/EmailField';
import PasswordField from '../../PasswordField/PasswordField';
import {Link} from 'react-router-dom'
import {SignUpModalContext} from '../../../Store'

const [SignUpModal, setSignUpModal] = useContext(SignUpModalContext)
const [stIsAdult, setstIsAdult] = useState(true);
const checkIsAdult = (event) => {
    // event.preventDefault()        
    setstIsAdult(event.target.checked)
}

class SignUpForm extends Component {

  // initialize state to hold validity of form fields
  state = { fullname: false, email: false, password: false }

  // higher-order function that returns a state change watch function
  // sets the corresponding state property to true if the form field has no errors
  fieldStateChanged = field => state => this.setState({ [field]: state.errors.length === 0 });

  // state change watch functions for each field
  emailChanged = this.fieldStateChanged('email');
  fullnameChanged = this.fieldStateChanged('fullname');
  passwordChanged = this.fieldStateChanged('password');
  

  render() {
    const { fullname, email, password } = this.state;
    const formValidated = fullname && email && password;    
      
    const registerUser = () => {

    }

    // validation function for the fullname
    // ensures that fullname contains at least two names separated with a space
    const validateFullname = value => {
    //   const regex = /^[a-z]{2,}(\s[a-z]{2,})+$/i;
    //   if (!regex.test(value)) throw new Error('Fullname is invalid');
      if (value.length < 5) throw new Error('Fullname is invalid');      
    };

    return (
      <div className=" position-relative align-middle w-100">
        <form action="/" method="POST" noValidate>

          <div className="d-flex flex-row justify-content-between align-items-center">
            {/* <legend className="form-label mb-0">Support Team</legend> */}
            {/** Show the form button only if all fields are valid **/}
            {/* { formValidated && <button type="button" className="btn btn-primary text-uppercase px-3 py-2">Join</button> } */}
          </div>

          <div className="py-0 w-100">
            {/** Render the fullname form field passing the name validation fn **/}
            <FormField  type="text" 
                        fieldId="fullname"                        
                        placeholder="Enter Full Name" 
                        validator={validateFullname} 
                        onStateChanged={this.fullnameChanged} 
                        required />

            {/** Render the email field component **/}
            <EmailField fieldId="email"                        
                        placeholder="Enter Email Address" 
                        onStateChanged={this.emailChanged} 
                        required />

            {/** Render the password field component using thresholdLength of 7 and minStrength of 3 **/}
            <PasswordField  fieldId="password"                          
                            placeholder="Enter Password" 
                            onStateChanged={this.passwordChanged} 
                            thresholdLength={7} 
                            minStrength={3} 
                            required />
            <div className ='accept_tos_policy'>
                By registering, I accept the Funniq 
                <Link to={`/about/privacy`} className='tos' onClick={() => setSignUpModal(!SignUpModal)} >
                    Term of Service 
                </Link>
                and acknowledge the
                <Link to={`/about/privacy`} className='tos' onClick={() => setSignUpModal(!SignUpModal)} >
                    Privacy Policy
                </Link>                          
            </div>
            <div className="adult_check_container">
                <div className={`custom-control custom-checkbox is_adult_check`}>
                    <input type="checkbox" className="custom-control-input" id="defaultUnchecked" onClick={(event) => checkIsAdult(event)} checked={stIsAdult} />
                    <label className="custom-control-label" htmlFor="defaultUnchecked">I am older than 13</label>
                </div>
            </div>          
            <div className="d-flex flex-column px-0 py-2">
                <button type="button" type="submit" className="btn Login_btn" onClick={(e) => registerUser(e)}>Register</button>
                {/* <button type="button" className="btn signup_cancel_up_btn flex-fill" onClick={() => setSignUpModal(!SignUpModal)} >Cancel</button> */}
            </div>                            
          </div>

        </form>
      </div>
    );
  }

}

export default SignUpForm;