import React, {useContext, useState} from 'react'
import GoogleLogin from 'react-google-login'
import axios from 'axios'
import {MDBBtn, MDBIcon} from "mdbreact";
import './GoogleAuth.sass'
import {CurrentUserContext, LoggedInContext, SignInModalContext, SignUpModalContext} from '../../../Store'
import Cookies from 'js-cookie'
import GoogleMoonLoaderCss from '../../../config/ReactLoader/GoogleMoonLoaderCss'
import { MoonLoader } from 'react-spinners'; 
import swal from 'sweetalert'
import {firestoreService} from '../../../services/firestoreService'

const GoogleAuth = () => {

    const [stGGLoginLoading, setstGGLoginLoading] = useState(false);
    const [SignInModal, setSignInModal] = useContext(SignInModalContext)
    const [SignUpModal, setSignUpModal] = useContext(SignUpModalContext)
    const [, setLoggedIn] = useContext(LoggedInContext)
    const [, setstCurrentUser] = useContext(CurrentUserContext)

    const SwitchAuthenModelOff = () => {
      if (SignInModal === true) {
        setSignInModal(false)    
      } else if (SignUpModal === true) {
        setSignUpModal(false)
      }
    }

    const responseGoogle = (response) => {  
        setstGGLoginLoading(true)
        axios.post(`${process.env.REACT_APP_API_URL}/api/v1/google`, {
            id_token: response.tokenId          
          }).then(response => {           
            // if user exists, do login
            if(response.status === 200){
                Cookies.set('AuthToken', response.data.data.user.authentication_token);                
                setstCurrentUser(response.data.data.user)                                   
                setLoggedIn("LOGGED_IN")   
                setstGGLoginLoading(false)
                SwitchAuthenModelOff()
                firestoreService.createNotiChannelForUser(response.data.data.user.id)
                swal("Successful!","Welcome to Funniq, we hope that you will have the funniest and most relaxed time, we will refresh and get the most updated cosplay contents for you", "success")
                .then((value) => {
                  if (value === true){
                    window.location.reload();              
                  }                  
                });         
            }

            //If user is not existed, create new user
            if (response.data.is_success === true){
              Cookies.set('AuthToken', response.data.data.user.authentication_token);
              setstCurrentUser(response.data.data.user)               
              setLoggedIn("LOGGED_IN")
              setstGGLoginLoading(false)
              SwitchAuthenModelOff()  
              firestoreService.createNotiChannelForUser(response.data.data.user.id)  
              swal("Successful!","Welcome to Funniq, we hope that you will have the funniest and most relaxed time, we will refresh and get the most updated cosplay contents for you", "success")
                .then((value) => {
                  if (value === true){
                    window.location.reload();              
                  }                  
                });  
            }
          })
          .catch(error => {            
            if (error.response.status === 403) {              
              swal("Account is inactive!", "Your account is inactive, please contact us at https://facebook.com/FunniqPage or send email to support@funniq.com for support", "error");              
            } else {
              // swal("Internal Server Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");              
              swal("Internal Server Error", `${error.response.data}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
            }  
            setstGGLoginLoading(false)
          }) 
        
      }
    return (
      <div className="GoogleAuthCmp flex-fill">
          <GoogleLogin
              clientId="804986229551-sbakp5qbh2jgjer4qjc4oshkecv88lhi.apps.googleusercontent.com"
              buttonText="Login"
              onSuccess={responseGoogle}
              onFailure={responseGoogle}
              cookiePolicy={'single_host_origin'}
              render={renderProps => (
                  <MDBBtn className="GoogleAuthBtn" onClick={renderProps.onClick} color="red">
                      
                      <MDBIcon fab icon="google" style={{marginRight: '1em'}}/>
                      Google
                      <MoonLoader          
                        css={GoogleMoonLoaderCss}            
                        size={20}
                        color={'#fafafa'}
                        loading={stGGLoginLoading}                        
                      />
                  </MDBBtn>
              )}
        />
      </div>
    );
}

export default GoogleAuth
