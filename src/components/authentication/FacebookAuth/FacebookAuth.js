import React, {useContext, useState} from 'react'
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import axios from 'axios'
import { MDBBtn, MDBIcon } from "mdbreact";
import './FacebookAuth.sass'
import {CurrentUserContext, LoggedInContext, SignInModalContext, SignUpModalContext} from '../../../Store'
import Cookies from 'js-cookie';
import { MoonLoader } from 'react-spinners'; 
import FacebookMoonLoaderCss from '../../../config/ReactLoader/GoogleMoonLoaderCss'
import swal from 'sweetalert';
import {firestoreService} from '../../../services/firestoreService'

// import { store } from 'react-notifications-component';
// import { Redirect } from 'react-router-dom';
// import { MoonLoader } from 'react-spinners'; 

const FacebookAuth = (props) => {    

    const [stFBLoginLoading, setstFBLoginLoading] = useState(false);
    const [SignInModal, setSignInModal] = useContext(SignInModalContext)
    const [SignUpModal, setSignUpModal] = useContext(SignUpModalContext)
    const [, setLoggedIn] = useContext(LoggedInContext)
    const [,setstCurrentUser] = useContext(CurrentUserContext)    
    const FacebookOnclick = () => {
      
    }
    

    // const renderRedirect = () => {
    //   if (stRedirect) {
    //     return <Redirect to='/' />
    //   }
    // }
  
    const SwitchAuthenModelOff = () => {
      if (SignInModal === true) {
        setSignInModal(false)    
      } else if (SignUpModal === true) {
        setSignUpModal(false)
      }
    }

    const responseFacebook = (response) => {        
        setstFBLoginLoading(true)
        axios.post(`${process.env.REACT_APP_API_URL}/api/v1/facebook`, {
            facebook_access_token: response.accessToken                            
          }                  
          ).then(response => {

            // alert(response.status)

            if(response.status === 200){
              Cookies.set('AuthToken', response.data.data.user.authentication_token);
              setstCurrentUser(response.data.data.user)
              setLoggedIn("LOGGED_IN")                            
              setstFBLoginLoading (false)
              // if (response.data.data.user !== undefined) {
              SwitchAuthenModelOff ()
              firestoreService.createNotiChannelForUser(response.data.data.user.id)
              // props.history.push("/")
              swal("Successful!","Welcome to Funniq, we hope that you will have the funniest and most relaxed time, we will refresh and get the most updated cosplay contents for you", "success")
                .then((value) => {
                  if (value === true){
                    // window.location.reload();              
                  }                  
                });                            
            }

            //If user is not existed, create new user
            if (response.data.is_success === true){              
              Cookies.set('AuthToken', response.data.data.user.authentication_token);
              setstCurrentUser(response.data.data.user)
              setLoggedIn("LOGGED_IN")
              // setstRedirect(true)                          
              setstFBLoginLoading(false)
              SwitchAuthenModelOff()
              firestoreService.createNotiChannelForUser(response.data.data.user.id)
              swal("Successful!","Welcome to Funniq, we hope that you will have the funniest and most relaxed time, we will refresh and get the most updated cosplay contents for you", "success")
                .then((value) => {
                  if (value === true){
                    window.location.reload();              
                  }                  
              });              
            }
          })
          .catch(error => {                 
            if (error.response.status === 403) {
              swal("Account is inactive!", "Your account is inactive, please contact us at https://facebook.com/FunniqPage or send email to support@funniq.com for support", "error");
            } else {
              swal("Internal Server Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
            }
            setstFBLoginLoading(false) 
            // setstRedirect(true)
          })

          response={}
          // response.accessToken = null
      }
    return (
      <div className="FacebookAuthCmp flex-fill">
        <FacebookLogin
            appId="1159821490885774"
            autoLoad={false}
            fields="name,email,picture"
            onClick={FacebookOnclick}
            callback={responseFacebook}
            redirectUri='https://funniq.com'
            disableMobileRedirect={true}
            isMobile={false}
            render={renderProps => (
                <MDBBtn color="primary" className="facebook_auth" onClick={renderProps.onClick}>
                    <MDBIcon fab icon="facebook" style={{marginRight: '1em'}}/>
                    Facebook
                    &nbsp;
                    <MoonLoader          
                        css={FacebookMoonLoaderCss}            
                        size={18}
                        color={'#fafafa'}
                        loading={stFBLoginLoading}                        
                      />   
                </MDBBtn>
            )}
        />
      </div>
    );
}

export default FacebookAuth
