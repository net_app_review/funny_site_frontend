import React, {useContext, useState}from 'react';
// import PropTypes from 'prop-types';
import {MDBContainer, MDBBtn, MDBIcon} from "mdbreact"
import styles from './StickyUploadButton.module.sass'
import {LoggedInContext, SignInModalContext} from '../../../Store'
import ModalUploader from "../../ModalUploader/ModalUploader";
import Draggable from 'react-draggable';

const StickyUploadButton = props => {

    const [LoggedIn, ] = useContext(LoggedInContext)
    const [stIsModalUploaderOpen, setStIsModalUploaderOpen] = useState(false);
    const [, setSignInModal] = useContext(SignInModalContext)

    const onOpenModalUploader = () => {
        if (LoggedIn === "LOGGED_IN") {
        setStIsModalUploaderOpen(true)
        } else {
            setSignInModal(true)
        }
    }
    
    const toggleModalUploader = () => {
        setStIsModalUploaderOpen(!stIsModalUploaderOpen)
    }

    return (
        <>            
            <MDBContainer className={styles.StickyUploadButton} >
                <Draggable
                    axis="both"
                    handle=".handle"
                    // positionOffset={{x: '100%',y: '100%'}}
                    position={null}
                    grid={[25, 25]}
                    scale={1}                                 
                    // onStart={this.handleStart}
                    // onDrag={this.handleDrag}
                    onStop={onOpenModalUploader}
                    disabled={false}
                    // onMouseDown={onOpenModalUploader}
                    >                    
                    <div className={`handle ${styles.drag_button}`} onTouchEnd={onOpenModalUploader}>
                        <div className={styles.BtnContainer}>
                            <MDBBtn className={styles.Button}  >
                                <MDBIcon icon="plus" className={styles.UpIcon}/>
                            </MDBBtn>
                            {/* <div className={styles.Shining}></div> */}
                        </div>
                    </div>                    
                    
                </Draggable>
                <ModalUploader isOpen={stIsModalUploaderOpen}
                            toggle={toggleModalUploader}/>
                {/* <div className={styles.BtnContainer}>
                    <MDBBtn className={styles.Button} onClick={onOpenModalUploader} >
                        <MDBIcon icon="plus" className={styles.UpIcon}/>
                    </MDBBtn>                    
                </div> */}
            </MDBContainer>
        </>
    );
};

StickyUploadButton.propTypes = {

};

export default StickyUploadButton;
