import React from 'react';

import {MDBContainer} from "mdbreact"

const Body = props => {
    return (
        <MDBContainer fluid style={{
            padding: 0,
            minHeight: '100vh'            
        }}        
        >
            {props.children}
        </MDBContainer>
    );
};

Body.propTypes = {

};

export default Body;
