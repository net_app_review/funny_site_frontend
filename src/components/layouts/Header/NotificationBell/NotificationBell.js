import React, {useState, useEffect, useContext} from 'react';
import { MDBIcon, MDBDropdown, MDBDropdownToggle, MDBDropdownMenu } from 'mdbreact'
import './NotificationBell.sass'
import {withRouter} from 'react-router-dom'
import UserNotification from '../../../../components/UserNotification/UserNotification'
import {CurrentUserContext} from '../../../../Store'
import firebase from '../../../../config/firebase'
import {notificationService} from "../../../../services/notificationService"
// import useUserAuthenForm from '../../authentication/useUserAuthenForm'
import TriangleDiv from '../../../../components/TriangleDiv/TriangleDiv'
// import { ActionCableProvider } from 'react-actioncable-provider';
// import Cookies from 'js-cookie'

const NotificationBell = props => {    

    // const [stIsShowNewNoti, setstIsShowNewNoti] = useState('d-block');
    // const [stUnseenCount, setstUnseenCount] = useState(0);
    // const [stRankOverflowY, setstRankOverflowY] = useState("hidden");    
    const [stNotifications, setstNotifications] = useState({});
    const [stNotiPage, setstNotiPage] = useState(5);
    // const [, setstShowSeeMore] = useState('none');
    const [stCurrentUser, ] = useContext(CurrentUserContext) 
    
    useEffect(() => {        
        getNotifications()
    }, [])
        
    const seeTheNotification = (e,notification) => {      
      // e.preventDefault()        
      // notificationService
      let data = {
        status: "seen"
      }

      notificationService.updateNotification(notification, data)
    }
    
    const getNotifications = () => {
      let db = firebase.firestore()
      return db.collection(`${process.env.REACT_APP_FIREBASE_NOTIFICATION_COLLECTION}`)
               .where("receiver", "==", stCurrentUser.uuid)
               .where("status", "==", "unseen")
               .limit(stNotiPage).onSnapshot((snapshot) => {
        const feedbackData = [];
        snapshot.forEach((doc) =>
          feedbackData.push({ ...doc.data(), id: doc.id })
        );          
        setstNotifications(feedbackData)
      });
    }

    const getUnseenNotiCount = () => {
      let count = 0
      for (var i = 0; i < stNotifications.length; i++) {
        if (stNotifications[i].status === "unseen"){
          count = count + 1
        }
      }
      return count
    }
    
    const renderSeeMoreButton = () => {    
        if(getUnseenNotiCount > 0) {
            return  <div className='see_more_cmp' onClick={loadNextPage}>
                      <button className='btn see_more_btn'>
                          See more
                      </button>                
                    </div>
        }
    }

    const loadNextPage = () => {
        setstNotiPage(stNotiPage + 1)
    }

    const markAllSeenNoti = () => {
      let data = {
        status: "seen"
      }
      for (var i = 0; i < stNotifications.length; i++) {
        if (stNotifications[i].status === "unseen"){          
          notificationService.updateNotification(stNotifications[i].id, data)
        }
      }      
    }

    const renderMarkAllAsSeen = () => {
      return (
            <button className='btn seen_all_btn' onClick={markAllSeenNoti} style={{ display: `${getUnseenNotiCount() > 0 ? 'block': 'none'}`}}>
              Mark all as seen
            </button>    
      )
    }

    return (
        <div className="notification_bell">
            
            <MDBDropdown className='bell_dropdown'>
                <MDBDropdownToggle color="primary" >
                    <MDBIcon far icon="bell" />
                    <div className='show_noti' style={{ display: `${getUnseenNotiCount() > 0 ? 'block': 'none'}`}}>
                        {getUnseenNotiCount()}
                    </div>
                </MDBDropdownToggle>                                                                                                          
                    <MDBDropdownMenu className='notification_dropdown_menu' right basic>                            
                        <TriangleDiv />
                        {renderMarkAllAsSeen()}
                        {stNotifications.length > 0 && stNotifications.map((notification, index) => {
                            return (                                        
                              < UserNotification notification={notification} key={index} onClick={seeTheNotification}/>                                
                            )
                        })}       
                        {/* {renderSeeMoreButton()}*/}
                    </MDBDropdownMenu>                                        
            </MDBDropdown>                                    
        </div>
    );  
};

NotificationBell.propTypes = {

};

export default withRouter( NotificationBell);
