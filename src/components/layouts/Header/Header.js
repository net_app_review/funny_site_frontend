import React, {useContext, useState, useEffect} from 'react';
import {
    MDBBtn,    
    MDBContainer,
    MDBDropdown,
    MDBDropdownItem,
    MDBDropdownMenu,
    MDBDropdownToggle,
    MDBIcon,
    MDBNavbar,
    MDBNavbarBrand,
    MDBNavbarNav,
    MDBNavItem, 
} from 'mdbreact';
import UserSignIn from '../../authentication/UserSignIn.js';
import UserLogIn from '../../authentication/UserLogIn.js'
import {CurrentUserContext, LoggedInContext, SignInModalContext} from '../../../Store'
import axios from 'axios'
import ModalUploader from '../../ModalUploader/ModalUploader';
import {Link} from 'react-router-dom'
// import Searchbar from './Searchbar'
import './Header.sass'
import { Redirect } from 'react-router'
import {categoriesService} from '../../../services/categoriesService';  
import LeftSideHidden from './LeftSideHidden/LeftSideHidden'
import Cookies from 'js-cookie';
// import MobilePopNav from  '../../../components/Mobile/MobilePopNav/MobilePopNav''
import { useTranslation } from "react-i18next";
import NotificationBell from './NotificationBell/NotificationBell'
import SquareSearchBar from '../../../components/SquareSearchBar/SquareSearchBar'
import swal from 'sweetalert'

const Header = props => {
    const [LoggedIn, setLoggedIn] = useContext(LoggedInContext)
    const [SignInModal, setSignInModal] = useContext(SignInModalContext)    
    const [stIsMenuOpen, ] = useState(false);
    const [stIsModalUploaderOpen, setStIsModalUploaderOpen] = useState(false);    
    const [stRedirect, ] = useState(false)  
    // const [stShowLeftNav, setstShowLeftNav] = useState(false)  
    const [stCate, setStCate] = useState([]);
    const [stLeftNavWidth, setstLeftNavWidth] = useState('0px');
    const [stCurrentUser, setstCurrentUser] = useContext(CurrentUserContext)
    const { t,  } = useTranslation();
    // const [stprevScrollpos, setstprevScrollpos] = useState(window.scrollY);
    // const [stHeaderVisible, setstHeaderVisible] = useState(true);
    const [stprevScrollpos, setstprevScrollpos] = useState(window.scrollY);
    const [stHeaderVisible, setstHeaderVisible] = useState('');
    
    // useEffect(() => {
    //     window.addEventListener('scroll', handleScroll);
    // }, [stprevScrollpos, stHeaderVisible])
    
    useEffect(() => {
        categoriesService.getCategories()
            .then(cates => {
                setStCate(cates)
            })        
    }, [])

    useEffect(() => {
        const onScroll = () => {
            let currentScrollPos = window.scrollY
            setstprevScrollpos(currentScrollPos)
            if (currentScrollPos > stprevScrollpos && currentScrollPos > 80 ){                     
                setstHeaderVisible('hidden')                
            } else if (currentScrollPos < stprevScrollpos - 10 ){
                setstHeaderVisible('')
            }
            
        }

        window.addEventListener('scroll', onScroll)

        return () => {
            window.removeEventListener('scroll', onScroll)
        };
    }, [stprevScrollpos])
   
    const redirect = () => {
        if (stRedirect === true) return <Redirect to='/'/>
    }

    const SignIntoggle = () => {        
        setSignInModal(!SignInModal)
    }

    const onOpenModalUploader = () => {
        if (LoggedIn === 'LOGGED_IN') {
            setStIsModalUploaderOpen(true)
        } else {
            setSignInModal(true)
        }
    }

    const toggleModalUploader = () => {
        setStIsModalUploaderOpen(!stIsModalUploaderOpen)
    }
    
    const handleLogoutClick = () => {
        // delete 'log_out', to: 'sessions#destroy'
        axios.delete(`${process.env.REACT_APP_API_URL}/api/v1/log_out`,{
            headers: {                
                'X-User-Token': `${stCurrentUser.authentication_token}`
            }
            }).then(response => {                         
                if (response.status === 200){
                    setLoggedIn('NOT_LOGGED_IN')
                    setstCurrentUser({})
                    Cookies.remove('AuthEmail');
                    Cookies.remove('AuthToken')
                    // setstRedirect(true)
                    window.location.reload();                    
                }
          }).catch(error => {
            swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");        
          })          
    }    

    const renderAuthenNav = () => {
        return (
            <>
            {
                LoggedIn === 'LOGGED_IN'
                ? <MDBNavItem className='Authen_Nav'>                    
                    <MDBDropdown className='user_dropdown float-rights'>
                        <MDBDropdownToggle nav>
                                <img className='img-fluid rounded-circle avatar_img' src={stCurrentUser.image ? stCurrentUser.image.url : ''} alt="img" ></img>                     
                                {/* <span className='mr-2'>My Account</span>                             */}
                        </MDBDropdownToggle>
                        <MDBDropdownMenu right className='user_dropdown_menu'>
                            {/* <Link to={`/about/faqs`} style={{ textDecoration: 'none' }} >
                                <MDBDropdownItem >FAQs</MDBDropdownItem>
                            </Link> */}                           
                            <Link to={`/users/${stCurrentUser.uuid}`} style={{ textDecoration: 'none' }} >
                                <MDBDropdownItem >{t("header.peronal")}</MDBDropdownItem>
                            </Link>
                            <Link to={`/settings`} style={{ textDecoration: 'none' }} >
                                <MDBDropdownItem >{t("header.settings")}</MDBDropdownItem>
                            </Link>
                            <MDBDropdownItem onClick={handleLogoutClick}>{t("header.logout")}</MDBDropdownItem>
                        </MDBDropdownMenu>
                    </MDBDropdown>
                </MDBNavItem>
                :
                <>
                    {redirect()}
                    <>                
                    <div className='Authen_Nav'>  
                        {/* <img className='img-fluid rounded-circle' 's'rc='https://www.memedroid.com/images/icons/icon_sign_in_white.png' style={{height: '38px', width: '38px', 'marginLeft' : '4px'}} onClick={SignIntoggle} alt='blank user icon' ></img>                                                                              */}
                        <MDBIcon far icon="user" onClick={SignIntoggle}/>
                    </div>                
                    </>
                </>
            }
            </>
        )
    }

    const showLeftNav = () => {
        if (stLeftNavWidth === '0px'){
            setstLeftNavWidth('100vw')
        } else if (stLeftNavWidth === '100vw'){
            setstLeftNavWidth('0px')
        }
    }


    const renderNotificationBell = () => {
        if (LoggedIn === 'LOGGED_IN') {
            return <div>
                <NotificationBell />
            </div>
        } else {
            return <> </>
        }
    }

    const clickLogo = () => {        
        if (
            window.location.href === "https://funniq.com/" || 
            window.location.href === "https://test.funniq.com/" ||
            window.location.href === "https://localhost:3000/"
        ){
            window.scrollTo({
                top: 0,
                behavior: 'smooth',
            });
            // window.location.reload()
        }
    }

    return (
        <>
            
            <ModalUploader isOpen={stIsModalUploaderOpen}
                           toggle={toggleModalUploader}/>
            <UserLogIn/>
            <UserSignIn/>
            <LeftSideHidden navonClick={showLeftNav} width={stLeftNavWidth} categoryList={stCate} />            
            
            <MDBNavbar expand={'md'} dark className={`HeaderTopNav ${stHeaderVisible}`}  onMouseMove={props.onMouseMove}>   
                <MDBContainer className='header'>
                    <MDBNavbarBrand className='pr-2 m-0 open_left_nav'>
                        <MDBIcon icon='bars' onClick={() => showLeftNav()} className='d-lg-none'/>
                    </MDBNavbarBrand>

                    <MDBNavbarBrand className='MainLogo'>                            
                        {/* <MenuIcon /> */}
                            <Link to={`/`} style={{ textDecoration: 'none', color: 'white' }} onClick={(e) => clickLogo(e)}>                                
                                {t("header.title")}                                  
                                    {/* &nbsp; */}
                                    {/* <sub>{t("header.subTitle")}</sub>                                 */}
                            </Link>
                    </MDBNavbarBrand> 

                    <div className='navbar-brand search_bar d-none d-lg-block ' >
                        {/* <Searchbar /> */}
                        <SquareSearchBar />
                    </div>       
                                                                    
                    <MDBNavbarNav right className='right_nav_header'>                                                
                        <MDBNavItem className='d-flex flex-row '>                                                          
                            {renderNotificationBell()}      
                            
                            {renderAuthenNav()}
                        </MDBNavItem>
                        <MDBNavItem className={stIsMenuOpen? 'upload-wrapper--open':'upload-wrapper--close'} >
                            <MDBBtn color='primary' size='sm' onClick={onOpenModalUploader}
                            className='btn-upload d-none d-lg-block '>
                                {/*<MDBNavLink >*/}
                                <MDBIcon far icon='arrow-alt-circle-up' />
                                {' '}
                                Upload Now!
                                {/*</MDBNavLink>*/}
                            </MDBBtn>
                        </MDBNavItem>
                    </MDBNavbarNav>                                                                              
                </MDBContainer>

                </MDBNavbar>                        
        </>
    );
};

Header.propTypes = {

};

export default Header;
