import React from 'react';
import {MDBIcon, Container, MDBCol, MDBRow} from 'mdbreact'
import styles from './LeftSideHidden.module.sass'
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import SocialButtons from '../../../SocialButtons/SocialButtons'
import LeftNavSearchBar from '../../../../components/Mobile/LeftNavSearchBar/LeftNavSearchBar'
import { useTranslation } from "react-i18next";
import CompanyInfo from '../../../../components/CompanyInfo/CompanyInfo'


const LeftSideHidden = props => {    
    
    const {navonClick, width, categoryList} = props
    const { t, } = useTranslation();    

    return (        
        <Container className={styles.LeftSideHidden} style={{ width: `${width}`}}>
            <div className={`d-flex flex-column ${styles.Category_Area}`}>
                <div className={styles.closeButton} onClick={navonClick}>
                    <div className={`${styles.close_button}`}></div>
                </div>
                <LeftNavSearchBar onClick={navonClick}/>
                <MDBRow>
                <MDBCol size="6">
                    {/* <h3>
                        <strong>
                            {t("header.categories")}
                        </strong>                    
                    </h3> */}
                    {
                        categoryList.map(cate => {
                            return (
                                <Link to={`/categories/${cate.id}-${cate.slug}`} key={cate.id} onClick={navonClick} className={styles.category_link}>
                                    <div className="d-flex">
                                        <div className="p-2 flex-shrink-1 bd-highlight ">
                                            <MDBIcon icon={cate.icon} />
                                        </div>
                                        <div className="p-2 col-example text-right"> 
                                            {cate.name} 
                                        </div>                                
                                    </div>                            
                                </Link>
                            )
                        })
                    }
                </MDBCol>
                    <MDBCol size="6">
                        {/* <h3>
                            <strong>
                                {t("header.popular")}
                            </strong>                        
                        </h3> */}
                        <Link to={`/hot`} onClick={navonClick}>
                            <div className="p-0 d-flex flex-row" >                            
                                <div className="p-2 flex-shrink-1">
                                    <MDBIcon icon="fire" className={styles.popular_icon}/>
                                </div>
                                
                                    <div className="p-2">{t("header.hot")}</div>                                
                            </div>
                        </Link>
                        <Link to={`/fresh`} onClick={navonClick}>
                            <div className="p-0 d-flex flex-row" >
                                <div className="p-2 flex-shrink-1 ">
                                    <MDBIcon icon="sync-alt" className={styles.popular_icon}/>
                                </div>                                
                                <div className="p-2">{t("header.fresh")}</div>                                
                            </div>
                        </Link>
                        <Link to={`/trending`} onClick={navonClick}>
                            <div className="p-0 d-flex flex-row" >
                                <div className="p-2 flex-shrink-1 ">
                                    <MDBIcon icon="chart-line" className={styles.popular_icon}/>
                                </div>                                
                                    <div className="p-2">{t("header.trending")}</div>                            
                            </div>                        
                        </Link>
                        <Link to={`/rankings`} onClick={navonClick}>
                            <div className="p-0 d-flex flex-row" >
                                <div className="p-2 flex-shrink-1 ">
                                    <MDBIcon fab icon="hackerrank" className={styles.popular_icon}/>
                                </div>                                
                                    {/* <div className="p-2">{t("header.rankings")}</div>                             */}
                                    <div className="p-2">Rankings</div>                            
                            </div>                        
                        </Link>
                        <Link to={`/about/feedback`} onClick={navonClick}>
                            <div className="p-0 d-flex flex-row" >
                                <div className="p-2 flex-shrink-1 ">
                                    <MDBIcon icon="envelope" className={styles.popular_icon}/>
                                </div>                                
                                    {/* <div className="p-2">{t("header.rankings")}</div>                             */}
                                    <div className="p-2">Feedback</div>                            
                            </div>                        
                        </Link>
                        <a href="https://status.funniq.com" aria-label="status_page" target="_blank" rel="noopener noreferrer" className="popular_link" >
                            <div className="p-0 d-flex flex-row" >
                                <div className="p-2 flex-shrink-1 ">
                                    <MDBIcon icon="signal" className={styles.popular_icon}/>
                                </div>                                                                    
                                    <div className="p-2">Service status</div>
                            </div>   
                        </a>
                    </MDBCol>    
                </MDBRow>
                <div className={styles.comp_info}>
                    <CompanyInfo navonClick={navonClick}/>
                </div>                
                <div className="pl-0 ml-auto mr-auto">&copy; {(new Date().getFullYear())} 
                    &nbsp;
                    <a href="https://funniq.com">
                        funniq.com
                    </a>                
                </div>                
            </div>
            <div className={`${styles.social_buttons}`}>
                <div className="ml-auto mr-auto pl-2">
                    <SocialButtons />
                </div>
                
            </div>
            
            <button type="button" className={`btn btn-success ${styles.return_button}`} onClick={navonClick}>{t("header.back")}</button>                   
                            
        </Container>        
    );  
};

LeftSideHidden.propTypes = {    
    width: PropTypes.string,
    navonClick: PropTypes.func,
    categoryList: PropTypes.array
};

export default LeftSideHidden
