import React, {useEffect, useRef, useState} from 'react';
import PropTypes from 'prop-types';
import style from './Uploader.module.sass'
import {MDBBtn, MDBIcon} from "mdbreact";
import swal from 'sweetalert';
import { useTranslation } from "react-i18next";
import Resizer from 'react-image-file-resizer';

const useDrag = (dropDOM) => {
    const [stFile, setStFile] = useState(null);

    const handleDrag = (e) => {
        e.preventDefault()
        e.stopPropagation()
    }
    const handleDragIn = (e) => {
        e.preventDefault()
        e.stopPropagation()
    }
    const handleDragOut = (e) => {
        e.preventDefault()
        e.stopPropagation()
    }
    const handleDrop = (e) => {        
        e.preventDefault()
        e.stopPropagation()
        if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
            e.dataTransfer.clearData()
            setStFile(e.dataTransfer.files[0])
        }
    }

    useEffect(() => {        
        if (!dropDOM) return
        dropDOM.current.addEventListener('dragenter', handleDragIn)
        dropDOM.current.addEventListener('dragleave', handleDragOut)
        dropDOM.current.addEventListener('dragover', handleDrag)
        dropDOM.current.addEventListener('drop', handleDrop)
        return () => {            
            dropDOM.current.removeEventListener('dragenter', handleDragIn)
            dropDOM.current.removeEventListener('dragleave', handleDragOut)
            dropDOM.current.removeEventListener('dragover', handleDrag)
            dropDOM.current.removeEventListener('drop', handleDrop)
        }
    }, [dropDOM])

    return stFile
}

const Uploader = props => {
    const refInputUpload = useRef(null);
    const refDropZone = useRef(null);
    const [stCurrentImage, setStCurrentImage] = useState(null);
    const { t, i18n } = useTranslation();
    const dropFile = useDrag(refDropZone)

    useEffect(() => {
        
        setStCurrentImage(dropFile)
        
    }, [dropFile])

    const onUploadClick = () => {        
        if (stCurrentImage) return
        refInputUpload.current.click()
    }

    const onClickBtnRemove = () => {
        setStCurrentImage(null)
    }


    const checkImageSize = (image, size) => {        
        if (image) {
          if(image.size > size ){
            return "oversized"
        }
        }
    }

    const onFileUploaded = (e) => {

        const files = e.currentTarget.files

        if (files.length > 0) {

            //check file size
            if (files[0].type.includes('mp4') || files[0].type.includes('webm')){
                if (checkImageSize (files[0], 6500000) === "oversized"){
                    swal("Error!", "At the moment we only support 6.5MB video, if you need to upload larger files, please contact us on suppport@funniq.com or our Facebook page https://facebook.com/FunniqPage!", "error");
                } else {
                    setStCurrentImage(files[0])
                    if (props.onUploaded) props.onUploaded(files[0])
                }                
            } else if (files[0].type.includes('gif')){
                if (checkImageSize (files[0], 3500000) === "oversized"){
                    swal("Error!", "At the moment we only support 3.5MB gif, if you need to upload larger files, please contact us on suppport@funniq.com or our Facebook page, https://facebook.com/FunniqPage!", "error");
                } else {
                    setStCurrentImage(files[0])
                    if (props.onUploaded) props.onUploaded(files[0])
                }
            } else {
                if (checkImageSize (files[0],20000000) === "oversized"){                    
                    swal("Error!", "At the moment we only support 20MB images, if you need to upload larger files, please contact us on suppport@funniq.com or our Facebook page, https://facebook.com/FunniqPage!", "error");
                } else {                    
                    Resizer.imageFileResizer(
                        files[0],
                        550,
                        50000,
                        'JPEG',
                        90,
                        0,
                        uri => {
                            // setstImageFile(uri)                        
                        setStCurrentImage(uri)
                        if (props.onUploaded) props.onUploaded(uri)
                        },
                        'blob'                    
                    );
                }                    
                // setStCurrentImage(files[0])                                                                    
            }
            
        }
    }

    const {className, onUploaded, uploadObj, ...other} = props
    return (
        <>
            {
                <div className={[style.wrapper, className].join(' ')} {...other}
                    ref={refDropZone}
                     hidden={!!stCurrentImage}
                     onClick={onUploadClick}>
                            <div  className={style.uploadIcon}>
                                <MDBIcon icon="upload" />
                            </div>
                            <div className={style.uploadSubtitle}>
                            {t("modalUploader.chooseFile")}
                            </div>
                    <input type="file"
                           hidden
                           accept={uploadObj === 'image' ? "image/png, image/jpeg" : "video/mp4"}
                           ref={refInputUpload}
                           onChange={onFileUploaded}
                    />
                </div>
            }
            {stCurrentImage &&
                <div className={style.previewImage}>
                    {uploadObj === 'image' ? 
                        <img src={`${URL.createObjectURL(stCurrentImage)}`} alt='_img'  className={style.theImage}/> 
                        : 
                        <video width="auto" muted controls autoPlay className={style.previewVideo}>
                            <source src={`${URL.createObjectURL(stCurrentImage)}`}/>
                            Your browser does not support HTML5 video.
                        </video>
                    }                    
                        <MDBBtn color="red" className={style.btnRemove} onClick={onClickBtnRemove}>
                            <MDBIcon icon="times" />
                        </MDBBtn>
                </div>
            }
        </>
    );
};

Uploader.propTypes = {
    uploadObj: PropTypes.string,
    className: PropTypes.string,
    onUploaded: PropTypes.func,
};

export default Uploader;
