import React from 'react';
import { WithContext as ReactTags } from 'react-tag-input';
import './MultipleTag.css'
import swal from 'sweetalert'

const KeyCodes = {
  comma: 188,
  enter: 13,
};
 
const delimiters = [KeyCodes.comma, KeyCodes.enter];

export default class MultipleTag extends React.Component {
    constructor(props) {
        super(props);         
        this.state = {
            tags: props.defaultList.map (tag => ({id: tag, text: tag}))
            ,
            suggestions: [
                { id: 'USA', text: 'USA' },
                { id: 'German', text: 'German' },
                { id: 'Austria', text: 'Austria' },
                { id: 'Costa Rica', text: 'Costa Rica' },
                { id: 'Sri Lanka', text: 'Sri Lanka' },
                { id: 'Thailand', text: 'Thailand' }
            ]
        };
        
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAddition = this.handleAddition.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
    }   

    handleDelete(i) {
        const { tags } = this.state;
        this.setState({
         tags: tags.filter((tag, index) => index !== i),
        });
        this.passToModalUploader()
    }
 
    handleAddition(tag) {

        if (this.state.tags.length >= 10) {
            swal("Tags limit", "At the moment we only allow 10 tags per post, sorry for the inconvenience. Please click to close the notification", "error");
            return
        }         

        this.state.tags.push(tag) 
        // this.setState(state => ({ tags: [...state.tags, tag] }));
        this.passToModalUploader()
    }
 
    handleDrag(tag, currPos, newPos) {
        const tags = [...this.state.tags];
        const newTags = tags.slice();
 
        newTags.splice(currPos, 1);
        newTags.splice(newPos, 0, tag);
 
        // re-render
        this.setState({ tags: newTags });
        this.passToModalUploader()
    }

    passToModalUploader(){
        let tempTagList = this.state.tags.map( ({text}) => text )        
        this.props.tagList(tempTagList);
    }
 
    render() {
        const { tags, suggestions } = this.state;
        return (
            <div>
                <ReactTags
                    tags={tags}
                    suggestions={suggestions}
                    handleDelete={this.handleDelete}
                    handleAddition={this.handleAddition}
                    handleDrag={this.handleDrag}
                    delimiters={delimiters}
                    placeholder="Add tag then press enter..."
                />
            </div>
        )
    }
};

// export default class MultipleTag{};