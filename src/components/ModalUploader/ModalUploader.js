import React, {useEffect, useState, useContext, useRef} from 'react';
import { withRouter } from 'react-router'
import PropTypes from 'prop-types';
import style from './ModalUploader.module.sass'
import {    
    MDBIcon,    
    MDBModal,
    MDBModalHeader,
    MDBNav,
    MDBNavItem,
    MDBNavLink,    
    MDBTabContent,
    MDBTabPane,
    MDBProgress
} from "mdbreact";
import Uploader from "./Uploader/Uploader";
import {StorageService} from "../../services/storageService";
import {categoriesService} from "../../services/categoriesService";
import {articlesService} from "../../services/articlesService";
import {mediaService} from "../../services/mediaService"
import {CurrentUserContext} from '../../Store'
import MultipleTag from './MultipleTag/MultipleTag';
import swal from 'sweetalert'
import { useTranslation } from "react-i18next";

const TABS = {
    IMAGE: 'image',
    VIDEO: 'video'
}

const FORM_FIELDS = {
    TITLE: 'title',
    FROM_URL: 'from_url',
    CATEGORY: 'category'
}

const ModalUploader = props => {
    const [stFormModel, setStFormModel] = useState({
        [FORM_FIELDS.TITLE]: '',
        [FORM_FIELDS.FROM_URL]: '',
        [FORM_FIELDS.CATEGORY]: []
    });
    // const [stFormFromUrl, setStFormFromUrl] = useState('');
    const [stFile, setStFile] = useState(null);
    const [, setStDownloadLink] = useState('');    
    const [stCurrentTab, setStCurrentTab] = useState(TABS.IMAGE);
    const [stCategories, setStCategories] = useState([]);
    // const [stLoadingBar, setstLoadingBar] = useState(false);
    const [stCategorySelect, setstCategorySelect] = useState('Fun');
    const [stCurrentUser,] = useContext(CurrentUserContext)
    const [stTagLists, setstTagLists] = useState(['Funniq']);
    const { t, i18n } = useTranslation();
    const [stIsSensitiveArticle, setstIsSensitiveArticle] = useState(false);
    const [stUploadProgress, setstUploadProgress] = useState(0);
    // const [stModalIsOpen, setstModalIsOpen] = useState(props.is);
    const refArticleDescription = useRef(null);

    useEffect(() => {        
        categoriesService.getCategories()
            .then(result => {
                setStCategories(result)
                setStFormModel({
                    ...stFormModel,
                    [FORM_FIELDS.CATEGORY]: result[0].name
                })
            })        
    }, []);


// ----------------------------------------------------- Handle Title actions ---------------------------------------//

    // const _handleTitleOnChange =  (e) => {
    //     e.preventDefault()
    //     const value = e.currentTarget.value        
    //     setStFormModel({
    //         ...stFormModel,
    //         [FORM_FIELDS.TITLE]: value
    //     })                
    // }

    // function _handleTitleKeyPress (e) {        
    //     if (stFormModel[FORM_FIELDS.TITLE].length >= 200) {
    //         e.preventDefault();
    //         e.stopPropagation();    
            
    //         setStFormModel({
    //             ...stFormModel,
    //             [FORM_FIELDS.TITLE]: stFormModel[FORM_FIELDS.TITLE].substring(0,200)
    //         })            
                    
    //         return
    //     }
    // }

// ---------------------------------------------------- Handle Upload Form actions ---------------------------------------//

    const onFormChange = (e, name) => {
        
        const value = e.currentTarget.value        
        setStFormModel({
            ...stFormModel,
            [name]: value
        })        

        // checkTitleCharacterCount (e)
    }
    
    const onUploaded = (file) => {
        setStFile(file) 
        refArticleDescription.current.focus();
    }

    const getRandomInt = (min, max) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
    }

    const onClickBtnPost = () => {
        if (stCurrentTab === TABS.IMAGE) {            
            if (stFile === null || stFormModel[FORM_FIELDS.TITLE] === "" || stCategorySelect === "" || stTagLists.length === 0 || stTagLists === null ){
                // setstLoadingBar(false)                
                swal("Missing information", "Please fill the description, category and tag to upload", "error");                            
            } else {
            // setstLoadingBar(true)
            setstUploadProgress(stUploadProgress + 70)
            var metadata = {
                // contentType: 'application/octet-stream',
                // contentType: 'application/jpeg',
                contentType: 'image/jpeg'
            };
            StorageService.upload(stFile, 'u' + stCurrentUser.id + '_' + getRandomInt(1,100)  + '_' + Date.now(), metadata)
                .then(fileSnapshot => {
                    
                    if(fileSnapshot.bytesTransferred === fileSnapshot.totalBytes){
                        setstUploadProgress(100)
                    }                    

                    fileSnapshot.ref.getDownloadURL()
                        .then(downloadLink => {
                            setStDownloadLink(downloadLink)                            
                            if (downloadLink) {
                                const img = new Image()
                                img.onload = (e) => {
                                    const width = e.path[0].width
                                    const height = e.path[0].height
                                    
                                    let data = {
                                        article: {                                            
                                            header: stFormModel[FORM_FIELDS.TITLE],                                            
                                            content: "",
                                            mediatype: 'image',
                                            category: stCategorySelect,
                                            width: width,
                                            height: height,
                                            url: downloadLink,
                                            status: "approved",
                                            tags: stTagLists.toString(),
                                            sensitive: stIsSensitiveArticle
                                        }
                                    }

                                    articlesService.createArticles(data)
                                        .then(response => {
                                            if (response.status === 201){
                                                // setstLoadingBar(false)                                                
                                                // props.history.push(`/articles/${response.data.id}-${response.data.slug}`)
                                                props.history.push({
                                                    pathname: `/users/${stCurrentUser.uuid}`,                                                    
                                                    state: { action: "fetch" }
                                                })
                                                // swal("Successful!","Welcome to Funniq, we hope that you will have the funniest and most relaxed time, we will refresh and get the most updated cosplay contents for you", "success")
                                                //     .then((value) => {
                                                //     if (value === true){
                                                //         window.location.reload();              
                                                //     }                  
                                                // });
                                                window.location.reload();
                                            }
                                        })
                                        .catch(error => {       
                                            setstUploadProgress(0)                                     
                                            if (error.response.data.messages[0]){
                                                swal("Error", `${error.response.data.messages[0]}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
                                            } else {
                                                swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
                                            }
                                            
                                        })
                                }
                                img.src = URL.createObjectURL(stFile)
                            }
                        })
                })
        }
        }

        if (stCurrentTab === TABS.VIDEO) {   
            if (stFormModel[FORM_FIELDS.TITLE] === "" || stCategorySelect === "" || stTagLists.length === 0 || stTagLists === null){                
                // setstLoadingBar(false)
                swal("Missing information", "Please fill the title, category and tag to upload", "error");                
            }  else {
            if (stFile) {
                
                setstUploadProgress(90)
                // Upload to media and firebase service
                mediaService.uploadVideo(stFile,'u' + stCurrentUser.id + '_' + getRandomInt(1,100)  + '_' + Date.now())
                    .then(result => {                        
                        if (result.status === 200){
                            setstUploadProgress(95)
                            let data = {
                                article: {                                            
                                    header: stFormModel[FORM_FIELDS.TITLE],                                            
                                    content: "",
                                    mediatype: 'video',
                                    category: stCategorySelect,                                    
                                    url: result.data.public_url,
                                    status: "approved",
                                    tags: stTagLists.toString(),
                                    sensitive: stIsSensitiveArticle,
                                    thumbnail: result.data.thumbnail_url
                                }
                            }                            
                            
                            articlesService.createArticles(data)
                                .then(response => {
                                    if (response.status === 201){
                                        setstUploadProgress(100)
                                        // setstLoadingBar(false)                                                
                                        // props.history.push(`/articles/${response.data.id}-${response.data.slug}`)
                                        props.history.push({
                                            pathname: `/users/${stCurrentUser.uuid}`,                                                    
                                            state: { action: "fetch" }
                                        })
                                        // swal("Successful!","Welcome to Funniq, we hope that you will have the funniest and most relaxed time, we will refresh and get the most updated cosplay contents for you", "success")
                                        //     .then((value) => {
                                        //     if (value === true){
                                        //         window.location.reload();              
                                        //     }                  
                                        // });
                                        window.location.reload();
                                    }
                                })
                                .catch(error => {                                          
                                    setstUploadProgress(0)
                                    swal("Error", `${error}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
                                })
                        }
                    })
                    .catch(error => {
                        if (error.response.status === 406){
                            swal("Unsupported video codec", `At the moment we only support H264 video codec, your video codec is ${error.response.data.data.video_codec}, if you want to upload different codec, please contact us at https://facebook.com/FunniqPage`, "error");
                            setstUploadProgress(0)
                        } else {
                            swal(`Error`, `${error}`, "error");
                        }

                        // if (error.response.data.messages[0]){
                        //     swal("Error", `${error.response.data.messages[0]}, Please contact us at https://facebook.com/FunniqPage or email to support@funniq.com`, "error");
                        //     return
                        // }
                    })
            }
        
            if (stFormModel[FORM_FIELDS.FROM_URL]) {
                const data = {
                    article: {
                        name: stFormModel[FORM_FIELDS.TITLE],
                        header: stFormModel[FORM_FIELDS.TITLE],
                        sub_header: "",
                        content: "",
                        mediatype: 'video',
                        category: stCategorySelect,                        
                        url: stFormModel[FORM_FIELDS.FROM_URL],
                        status: "approved",
                        tags: stTagLists.toString(),
                        sensitive: stIsSensitiveArticle
                    }
                }

                articlesService.createArticles(data)
                    .then(response => {
                        if (response.status === 201){
                            window.location.reload();
                        }
                    })
                    .catch(error => {   
                        swal("Error", `Something is wrong ${error}, please contact us at https://facebook.com/FunniqPage`, "error");
                    })
            }
        }
    }    

    }

    const tabToggle = (currentTab) => {
        setStCurrentTab(currentTab)
        setStFile(null)
    }

    const getTagList = (tagList) => {
      setstTagLists(tagList);      
    }

    const checkSensitivity = (event) => {
        // event.preventDefault()        
        setstIsSensitiveArticle(event.target.checked)
    }

    const renderImageUploader = (uploadObj) => {
        return (
            <>
                <div className='d-flex flex-row justify-content-between'>
                    <div className='flex-grow-1'>
                        <div className={style.uploader_panel}>
                            <Uploader uploadObj={uploadObj}
                                      className={[style.uploader].join(' ')} onUploaded={onUploaded} />
                        </div>
                        <div className="pl-2 pr-2 mb-1">
                            <MDBProgress value={stUploadProgress} className='m-0' >
                                {
                                    stUploadProgress > 0 ? `Processing ${stUploadProgress} %` : ``
                                }
                            </MDBProgress>
                        </div>

                    </div>                    
                    <div className={`flex-column pl-2 ${uploadObj === 'image'? 'd-flex': 'd-none' }`}>
                        <textarea label="Title"                             
                            onBlur={(e) => onFormChange(e, 'title')} 
                            // onKeyPress={ _handleTitleKeyPress }
                            ref={refArticleDescription}
                            required 
                            maxLength="200"                                
                            className={`${style.article_title_input} flex-grow-1`}
                            placeholder='post description ...'
                        />
                        <div className={`custom-control custom-checkbox ${style.sensitive_check}`}>
                            <input type="checkbox" className="custom-control-input" id={`defaultUnchecked`} onClick={(event) => checkSensitivity(event)} checked={stIsSensitiveArticle}/>
                            <label className="custom-control-label" htmlFor="defaultUnchecked">sensitive content</label>
                        </div>
                    </div>
                </div>

                <div className={`flex-column pl-2 ${uploadObj === 'video'? 'd-flex': 'd-none' }`}>
                    <textarea label="Title"                             
                        onBlur={(e) => onFormChange(e, 'title')} 
                        // onKeyPress={ _handleTitleKeyPress }                                
                        required
                        ref={refArticleDescription}
                        rows="1"
                        maxLength="200"                               
                        className={`${style.article_title_input} flex-grow-1`}
                        placeholder='post description ...'
                    />
                    <div className={`custom-control custom-checkbox ${style.sensitive_check}` }>
                        <input type="checkbox" className="custom-control-input" id="defaultUnchecked" onClick={(event) => checkSensitivity(event)} checked={stIsSensitiveArticle}/>
                        <label className="custom-control-label" htmlFor="defaultUnchecked">sensitive content</label>
                    </div>
                </div>                    
                                                                
            <div className='d-flex flex-row mt-3'>
                <select className={["browser-default mb-2", 'custom-select', style.selectBox].join(' ')}
                    // defaultValue={stCategories[0]}
                    id="category_select"
                    value={stCategorySelect}
                    onChange={(e) => setstCategorySelect(e.target.value)}
                    required
                >
                    <option value="" defaultValue>Fun</option>
                    {stCategories.map(category => {
                        return (
                            <option key={category.id}
                                    value={category.name}>
                                {category.name}
                            </option>
                        )
                    })}
                </select>
            </div>   

            <div className='d-flex flex-row'>                            
                <div className="text-center pb-2 flex-grow-1">
                    <MultipleTag tagList={getTagList} defaultList={stTagLists} />                                                                                       
                </div>
            </div>                     
                
            <button color="primary" className={`btn button d-flex justify-content-center ${style.Post_Button}`} onClick={onClickBtnPost}>
                <MDBIcon icon="upload" className="pt-0 pr-2" />
                {t("modalUploader.post")}
            </button>
        </>
        )
    }

    const {isOpen, onOk, toggle, ...other} = props
    return (
        <MDBModal isOpen={isOpen} toggle={toggle} {...other} className={style.modal_uploader}>
            <div className="d-flex flex-column pl-2 pr-2">
            
                <MDBModalHeader className="pt-2 pb-0 pl-0 pr-0 col-example text-center" toggle={toggle}>{t("modalUploader.uploads")}</MDBModalHeader>                                       

                <MDBNav className="nav-tabs mt-2">
                    <MDBNavItem>
                        <MDBNavLink to="#"
                                    active={stCurrentTab === TABS.IMAGE}
                                    onClick={() => tabToggle(TABS.IMAGE)}
                                    role="tab" >
                            <MDBIcon icon="image" />
                            {' '}
                            {t("modalUploader.image")}
                        </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                        <MDBNavLink to="#"
                                    active={stCurrentTab === TABS.VIDEO}
                                    onClick={() => tabToggle(TABS.VIDEO)}
                                    role="tab" >
                            <MDBIcon icon="video" />
                            {' '}
                            {t("modalUploader.video")}
                        </MDBNavLink>
                    </MDBNavItem>
                </MDBNav>

                <MDBTabContent activeItem={stCurrentTab} >
                    <MDBTabPane tabId={TABS.IMAGE} role="tabpanel"
                                className={style.tabPane}
                    >   
                       {renderImageUploader('image')}
                    </MDBTabPane>
                    <MDBTabPane tabId={TABS.VIDEO} role="tabpanel"
                                className={style.tabPane}
                    >
                        <div className={`${style.video_announcement} ${stCurrentTab === TABS.IMAGE? 'd-none': 'd-flex' }`}>
                            *At the moment we only support codec h.264 video, up to 6.5MB size, would be increased in the future
                        </div>

                        {/* <MDBInput label="Video Link"
                            onBlur={(e) => onFormChange(e, 'from_url')}
                            required
                        />                         */}
                        {renderImageUploader('video')}

                        {/* <div className="p-2 border mt-2 mb-4 mr-2 ml-2">
                            {t("modalUploader.maintainance")}
                            {t("modalUploader.email")}
                            {t("modalUploader.support")}{" "}
                            <a href="https://facebook.com/FunniqPage">
                                {" "}
                                Funniq Facebook page
                            </a>
                            {t("modalUploader.thanks")}
                        </div> */}

                    </MDBTabPane>
                </MDBTabContent>                                        
            </div>                                           
        </MDBModal>
    );
};

ModalUploader.propTypes = {
    isOpen: PropTypes.bool,
    onOk: PropTypes.func,
    toggle: PropTypes.func,
};

export default withRouter(ModalUploader);
