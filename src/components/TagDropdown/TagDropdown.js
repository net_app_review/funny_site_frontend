import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom'
// import PropTypes from 'prop-types';
import './TagDropdown.sass'
// import {CurrentUserContext,SignInModalContext,LoggedInContext} from '../../../../Store'
import {MDBDropdown, MDBDropdownToggle, MDBDropdownMenu, MDBDropdownItem} from 'mdbreact'
// import { store } from 'react-notifications-component';


const TagDropdown = props => {
    
    useEffect(() => {		
    // fetchArticle()
        setstTags(props.tags)
    },[])

    const [stTags, setstTags] = useState([]);
    // const [stCurrentUser, setstCurrentUser] = useContext(CurrentUserContext)

    // const tag_badge_exist = () => {
    //     if (stTags.length > 0) {
    //       return <i className="fas fa-tags"></i>
    //     } 
    // }
   
    return (
        <div className="TagDropdown">

            <MDBDropdown className="more-dropdown"  dropdown>
                <MDBDropdownToggle className="tag-dropdown-btn custom-dropdown-toggle" aria-label='tag-dropdown-btn' color="dark">
                    <i className="fas fa-tags"></i>                    
                </MDBDropdownToggle>
                <MDBDropdownMenu right className="custom-dropdown-meu">
                    <MDBDropdownItem>
                        <strong>
                            Tags:
                        </strong>                        
                    </MDBDropdownItem>
                    <MDBDropdownItem divider/> 
                    {stTags.map((tag, index) => {
                        return (                      
                            <Link to={`/tags/${tag.id}-${tag.slug}`} key={index} className="tag_list" >
                                <MDBDropdownItem key={tag.id} >
                                    {tag.name}                                    
                                </MDBDropdownItem>
                                {/* <MDBDropdownItem divider/> */}
                            </Link>                      
                            )
                        })}                                     
                </MDBDropdownMenu>
            </MDBDropdown>
        </div>
    );
};

TagDropdown.propTypes = {
    
};

export default TagDropdown;