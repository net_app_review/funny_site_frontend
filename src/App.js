import React from 'react';
import { CookiesProvider } from 'react-cookie';
// import logo from './logo.svg';
import './App.css';
import AppRouters from "./AppRouters";
import Store from './Store';
import ReactNotification from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css'

function App() {

  return (
    <CookiesProvider>
      <ReactNotification />
      <Store>
        <AppRouters/>
      </Store>
    </CookiesProvider>
  );
}

export default App;
