import {NavPath} from "./NavPath";

export const NavRoute = {
    [NavPath.home]: {
        title: 'Funniq | Made for cosplay community'
    },
    [NavPath.root]: {
        title: 'Funniq | Made for cosplay community'
    },
    [NavPath.user]: {
        title: 'User'
    },
    // [NavPath.articles]: {
    //     title: 'Articles'
    // },    
    [NavPath.categories]: {
        title: 'Categories'
    },
    [NavPath.hot]: {
        title: 'Hot'
    },
    [NavPath.fresh]: {
        title: 'Fresh'
    },
    [NavPath.trending]: {
        title: 'Trending'
    },
    [NavPath.donate]: {
        title: 'Donate'
    },
    [NavPath.faqs]: {
        title: 'FAQs'
    },
    [NavPath.termofservice]: {
        title: 'Term of Service'
    },
    [NavPath.pagerules]: {
        title: 'Pagerules'
    },
    [NavPath.feeddback]: {
        title: 'Feedbacks'
    },
    [NavPath.password_forgot]: {
        title: 'Passwords Forgot'
    },
    [NavPath.password_reset]: {
        title: 'Passwords Reset'
    },
    [NavPath.settings]: {
        title: 'Settings'
    },
    [NavPath.search]: {
        title: 'Search'
    },
    [NavPath.rankings]: {
        title: 'Ranking'
    }
}

