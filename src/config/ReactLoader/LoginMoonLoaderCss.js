import { css } from '@emotion/core';

const LoginMoonLoaderCss = css`
  display: inline-block;
  margin: 0;
  border-color: red;  
  font-size: 20px;
`

export default LoginMoonLoaderCss