import { css } from '@emotion/core';

const DefaultMoonLoaderCss = css`  
  margin: 0 auto;
  border-color: transparent;
  margin-top: px;
  font-size: 12px;
  background-color: transparent;
  border: none;
  display: inline-block;
`

export default DefaultMoonLoaderCss