import axios from 'axios'
import Cookies from 'js-cookie';

// const BASE_URL = `${process.env.REACT_APP_API_URL}`
const BASE_URL = `${process.env.REACT_APP_MEDIA_API_URL}`

// const BASE_URL = 'https://funniq-load-balancer-988876443.ap-southeast-1.elb.amazonaws.com'

const apiMedia = axios.create({
    baseURL: BASE_URL
})

apiMedia.interceptors.request.use( config => {
    config.headers = {
        'X-User-Email': Cookies.get('AuthEmail'),
        'X-User-Token': Cookies.get('AuthToken'),
        'X-Guest-Token': Cookies.get('GuestTkn'),
    }

    return config
})

export default apiMedia