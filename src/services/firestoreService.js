import firebase from '../config/firebase'

// http://52.77.199.182:5050/api/v1/articles/user_articles?user_id=31&page=1

const NOTI_COLLECTION = process.env.REACT_APP_FIREBASE_NOTIFICATION_COLLECTION

const fetchPage = (page, doc) => {    
    let md =''
    firebase.firestore().collection('pages').doc('faqs')
    .get()
    .then(doc => {            
        // setStContent(doc.data())        
        md = doc.data().content
        // return doc.data()
    })    
    return md
}

function createNotiChannelForUser (user_id) {    
    firebase.firestore().collection(`${NOTI_COLLECTION}`).doc(`user${user_id}`)
    .set({
        noti1: {
            creator: 'system',
            content: 'welcome to Funniq',
            status: 'unseen'
        }
    })    
}

const createNoti = (creator, action, referred_object) => {
    firebase.firestore().collection(`${NOTI_COLLECTION}`).doc(`user${creator.id}`)
    .set({
        noti1: {
            creator: 'system',
            content: 'welcome to Funniq',
            status: 'unseen'
        }
    })
    .then(doc => {            
        // setStContent(doc.data())        
    })
    .catch(function(error) {
        alert("Error writing document: ", error);
    });
}

const listenForNoti = (user_id) => {
    let notifications = {}
    firebase.firestore().collection(`${NOTI_COLLECTION}`).doc(`user${user_id}`)
        .onSnapshot(function(doc) {                    
            // notifications = 
            // return (doc.data())
            notifications = Object.assign(doc.data(), notifications);
        });

    

    return notifications
}

export const firestoreService = {
    fetchPage,
    createNotiChannelForUser,
    listenForNoti,
    createNoti
}