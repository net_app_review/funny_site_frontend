import firebase from './../../src/config/firebase'

// import 'firebase/*';

const storageRef = firebase.storage().ref();

const upload = (blob, fileName, metadata, folder = `${process.env.REACT_APP_FIREBASE_FOLDER}`) => {
    // return new Promise( (resolve, reject) => {
    //     const filePathRef = storageRef.child(folder + "/" + fileName);
    //     filePathRef.put(blob)
    //         .then(snapshot => {
    //             resolve(snapshot)
    //         })
    //         .catch(reject)            
    // })

    return new Promise( (resolve, reject) => {
        const filePathRef = storageRef.child(folder + "/" + fileName);
        filePathRef.put(blob, metadata)
            .then(snapshot => {
                resolve(snapshot)
            })
            .catch(reject)            
    })

}

export const StorageService = {
    upload
}