import apiClient from "../config/apiClient";

// http://52.77.199.182:5050/api/v1/users/current_user

const SVC_NAME = '/api/v1/users/current_user'

const getCurrentUser = () => {
    return new Promise ( (resolve, reject) => {
        apiClient.get(`${SVC_NAME}`)
            .then( result => {                
                resolve(
                    {
                        data: result.data
                    }
                )
            }).catch(reject)
    })
}

export const currentUserService = {
    getCurrentUser
}