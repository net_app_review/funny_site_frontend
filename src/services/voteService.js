import apiClient from "../config/apiClient";

const SVC_NAME = '/api/v1/likes'

const handle_vote =  (candidateId, likeStatus, likeId)  => {
    let data = {}
        switch (likeStatus) {
            case "like": // => do nothing                
                break
            case "no_value": // => change from dislike to unlike                
                break
            case "no_action":
                data = {
                    id : "",
                    like: {
                        vote_candidate_id: candidateId,
                        liketype: "upvote"
                    }
                }
                break                
            case "dis_vote":                                                    
                data = {
                    id : likeId,
                    like: {
                        vote_candidate_id: candidateId,
                        liketype: "upvote"
                    }
                }
                break
            default:
                data = {}                
        }   

    return new Promise( (resolve, reject ) => {
        apiClient.post(`${SVC_NAME}/handle_vote`, data)
            .then(result => {
                resolve(result.data)                
                // window.location.reload();
            })
            .catch(reject)
    })
}

export const voteService = {
    handle_vote    
}