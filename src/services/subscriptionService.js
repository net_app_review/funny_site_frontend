import apiClient from "../config/apiClient";

// http://localhost:5054/api/v1/subscriber_records/create

const SVC_NAME = '/api/v1/subscriber_records'

const createSubscription = (subscribe_to) => {

    let data = {
        subscriber_record: {
            subscribeto: subscribe_to
        }
    }

    return new Promise( (resolve, reject) => {
        apiClient.post(`${SVC_NAME}/create`, data)
            .then(result => {
                resolve(result)
            })
            .catch(reject)
    })
}

// http://localhost:5054/api/v1/subscriber_records/delete

const updateSubscription = (subscribe_to, status) => {

    let data = {}

    if ( status === 'active'){
        data = {
            subscriber_record: {
                subscribeto: subscribe_to,
                status: 'inactive'
            }
        }
    } else if (status === 'inactive'){
        data = {
            subscriber_record: {
                subscribeto: subscribe_to,
                status: 'active'
            }
        }
    }   

    return new Promise( (resolve, reject) => {
        apiClient.put(`${SVC_NAME}/update`, data)
            .then(result => {
                resolve(result)
            })
            .catch(reject)
    })
}

// http://localhost:5054/api/v1/subscriber_records/delete

const deleteSubscription = (subscribe_to) => {

    let data = {
        subscriber_record: {
            subscribeto: subscribe_to            
        }
    }
    return new Promise( (resolve, reject) => {
        apiClient.post(`${SVC_NAME}/delete`, data)
            .then(result => {
                resolve(result)
            })
            .catch(reject)
    })
}



export const subscriptionService = {
    createSubscription,
    deleteSubscription,
    updateSubscription
}