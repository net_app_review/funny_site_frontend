import apiClient from "../config/apiClient";

//http://52.77.199.182:5050/api/v1/articles/hot_articles?page=1

const SVC_NAME = '/api/v1/articles/trending_articles'

const getArticles = (page) => {
    return new Promise ( (resolve, reject) => {
        apiClient.get(`${SVC_NAME}?page=${page}`)
            .then( result => {                
                resolve(
                    {
                        data: result.data,
                        totalPage: result.headers["total_page"],
                        totalCount: result.headers["total_article"]
                    }
                )
            }, reject)
    })
}

export const TrendingPageService = {
    getArticles
}