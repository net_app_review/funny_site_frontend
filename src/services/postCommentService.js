import apiClient from "../config/apiClient";
import {StorageService} from './storageService'

// http://52.77.199.182:5050/api/v1/articles/user_articles?user_id=31&page=1

const postComment = (image_file, obj, id, comment_content) => {

    let data = {}

    const getDataForPost = (downloadLink) => {


        if (downloadLink !== "") {
            if (obj === 'article'){
                return {
                    comment: {
                      content: comment_content,
                      article_id: id,
                      image_url: downloadLink
                    }              
                }
            } else if (obj === 'votefight'){
                return {
                    comment: {
                      content: comment_content,
                      votefight_id: id,
                      image_url: downloadLink
                    }              
                }
            }
        } else if (downloadLink === "") {            

            if (obj === 'article'){                
                return {
                    comment: {
                      content: comment_content,
                      article_id: id                      
                        }              
                    }
            } else if (obj === 'votefight'){
                return {
                    comment: {
                      content: comment_content,
                      votefight_id: id                      
                    }              
                }
            }

        }
    }                       

    let SVC_URL ='/api/v1/comments/create'

    return new Promise ( (resolve, reject) => {
        if (image_file !== null){          
            StorageService.upload(image_file, obj + id + '-' + Date.now())
                .then(fileSnapshot => {                      
                    fileSnapshot.ref.getDownloadURL()
                        .then(downloadLink => {                                  
                            if (downloadLink){                                                 
                            data = getDataForPost(downloadLink)
                            apiClient.post(`${process.env.REACT_APP_API_URL}${SVC_URL}`, data)
                                .then( result => {                
                                    resolve(
                                        {
                                            data: result
                                        }
                                    )
                                }, reject)
                        }
                    }
                )
            }
        )        
        } else {
            data = getDataForPost("")              
            apiClient.post(`${process.env.REACT_APP_API_URL}${SVC_URL}`, data)
                .then( result => {                
                    resolve(
                        {
                            data: result                   
                        }
                    )
        }, reject)
        }
    })                    
}            

export const postCommentService = {
    postComment
}