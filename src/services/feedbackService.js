import firebase from "../config/firebase"
import apiClient from "../config/apiClient";

// http://52.77.199.182:5050/api/v1/articles/user_articles?user_id=31&page=1

const FEEDBACK_COLLECTION = process.env.REACT_APP_FIREBASE_FEEDBACK_COLLECTION
// const FieldValue = require('firebase/app').firestore.FieldValue;
const SVC_NAME = '/api/v1/feedbacks'

const createFeedback = (user_id, username, email, is_anonymous, content, rating, feedback_type) => {    
  // ref Cookies.get("GuestTkn"), stUsername, stEmail, true, stNewFeedback, stRating, stNewFeedbackType

  let requestBody ={
    feedback: {
      user_id: user_id,
      username: username,
      email: email,
      is_anonymous: is_anonymous,
      content: content,
      rating: rating,
      feedback_type: feedback_type
    }
  }

  return new Promise( (resolve, reject) => {
    apiClient.post(`${SVC_NAME}/create`, requestBody)
        .then(result => {
            resolve(result)
        })
        .catch(reject)
})

    
}

const getAllFeedbacks = (page, feedback_type) => {
  return new Promise ( (resolve, reject) => {
    apiClient.get(`${SVC_NAME}?feedback_type=${feedback_type}&page=${page}`)
        .then( result => {
            resolve(
              {
                data: result.data                        
              }
            )
        }, reject)
  })
}

const getRatings = (rating) => {
  return new Promise( (resolve, reject ) => {
    // Create a query against the collection.
    firebase.firestore().collection(`${FEEDBACK_COLLECTION}`).where("rating", ">=", rating)
      .get()
      .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
            // Object.keys(doc.data()).length            
        });        
      })
      .catch(reject);        
  })
}

export const feedbackService = {
  createFeedback,
  getAllFeedbacks,
  getRatings
}