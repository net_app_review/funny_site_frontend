import apiClient from "../config/apiClient";

// http://localhost:5054/api/v1/users/rankings

const SVC_NAME = '/api/v1/users/rankings'

const getuserRankings = (page) => {    

    return new Promise ( (resolve, reject) => {
        apiClient.get(`${SVC_NAME}?page=${page}`)
            .then( result => {                
                resolve(
                    {
                        data: result.data                        
                    }
                )
            }, reject)
    })
}

const getCompetitionUserRankings = (page) => {    

    return new Promise ( (resolve, reject) => {
        apiClient.get(`/api/v1/users/weekly_rankings?page=${page}`)
            .then( result => {                
                resolve(
                    {
                        data: result.data                        
                    }
                )
            }, reject)
    })
}

export const userRankService = {
    getuserRankings,
    getCompetitionUserRankings 
}