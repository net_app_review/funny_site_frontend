import apiClient from "../config/apiClient";

const SVC_NAME = '/api/v1/categories'

const getCategories = () => {
    return new Promise ( (resolve, reject) => {
        apiClient.get(`${SVC_NAME}`)
            .then( result => {
                resolve(result.data)
            }, reject)
    })
}

export const categoriesService = {
    getCategories
}