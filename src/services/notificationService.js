import firebase from '../config/firebase'

// http://52.77.199.182:5050/api/v1/articles/user_articles?user_id=31&page=1

const NOTI_COLLECTION = process.env.REACT_APP_FIREBASE_NOTIFICATION_COLLECTION

const fetchPage = (page, doc) => {    
    let md =''
    firebase.firestore().collection('pages').doc('faqs')
    .get()
    .then(doc => {            
        // setStContent(doc.data())        
        md = doc.data().content
        // return doc.data()
    })    
    return md
}

const createNotification = (creator, creatorname, creatorImageUrl, articleID, notiObj, notiObjId, objUrl, objImageUrl, receiver, content) => {
    firebase.firestore().collection(`${NOTI_COLLECTION}`)
    .add({        
        creator: creator,
        creatorname: creatorname,
        creatorImageUrl: creatorImageUrl,
        articleID: articleID,
        notiObj: notiObj,
        notiObjId: notiObjId,
        objUrl: objUrl,
        objImageUrl: objImageUrl,        
        content: content,
        receiver: receiver,
        status: "unseen"
    })
    .then(doc => {            
        // setStContent(doc.data())        
    })
    .catch(function(error) {
        alert("Error writing document: ", error);
    });
}

const updateNotification = (document, data) => {
    firebase.firestore().collection(`${NOTI_COLLECTION}`)
    .doc(document)
    .update(data)
    .then(doc => {            
        // setStContent(doc.data())        
    })
    .catch(function(error) {
        alert("Error writing document: ", error);
    });
}

const listenForNoti = (user_id) => {
    let notifications = {}
    firebase.firestore().collection(`${NOTI_COLLECTION}`).doc(`user${user_id}`)
        .onSnapshot(function(doc) {                    
            // notifications = 
            // return (doc.data())
            notifications = Object.assign(doc.data(), notifications);
        });
    return notifications
}

export const notificationService = {
    fetchPage,
    createNotification,
    listenForNoti,
    updateNotification
}