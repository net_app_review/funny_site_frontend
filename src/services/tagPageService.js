import apiClient from "../config/apiClient";

// http://52.77.199.182:5050/api/v1/articles/user_articles?user_id=31&page=1

const SVC_NAME = '/api/v1/articles/tag_articles'

const getArticles = (tag_id,page) => {
    return new Promise ( (resolve, reject) => {
        apiClient.get(`${SVC_NAME}?tag_id=${tag_id}&page=${page}`)
            .then( result => {                
                resolve(
                    {
                        data: result.data,
                        totalPage: result.headers["total_page"],
                        totalCount: result.headers["total_article"]
                    }
                )
            }, reject)
    })
}

export const tagPageService = {
    getArticles
}