import apiClient from "../config/apiClient";

// http://localhost:5054/api/v1/searchs/user?search[search_string]=a&search[page]=1

const SVC_NAME = '/api/v1/searchs/'

const getArticles = (search_string,page) => {
    return new Promise ( (resolve, reject) => {
        apiClient.get(`${SVC_NAME}article?search[search_string]=${search_string}&search[page]=${page}`)
            .then( result => {                      
                resolve(
                    {
                        data: result.data,
                        totalPage: result.headers["total_page"],
                        totalCount: result.headers["total_articles"]
                    }
                )
            }, reject)
    })
}

const getTags = (search_string,page) => {
    return new Promise ( (resolve, reject) => {
        apiClient.get(`${SVC_NAME}tag?search[search_string]=${search_string}&search[page]=${page}`)
            .then( result => {                      
                resolve(
                    {
                        data: result.data,
                        totalPage: result.headers["total_page"],
                        totalCount: result.headers["total_tags"]
                    }
                )
            }, reject)
    })
}

const getUsers = (search_string,page) => {
    return new Promise ( (resolve, reject) => {
        apiClient.get(`${SVC_NAME}user?search[search_string]=${search_string}&search[page]=${page}`)
            .then( result => {                      
                resolve(
                    {
                        data: result.data,
                        totalPage: result.headers["total_page"],
                        totalCount: result.headers["total_users"]
                    }
                )
            }, reject)
    })
}

export const searchPageService = {
    getArticles,
    getTags,
    getUsers
}