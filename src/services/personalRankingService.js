import apiClient from "../config/apiClient";

// http://localhost:5054/api/v1/users/rankings

const SVC_NAME = '/api/v1/users/personal_ranking'

const getpersonalRankings = () => {    

    return new Promise ( (resolve, reject) => {
        apiClient.get(`${SVC_NAME}`)
            .then( result => {                
                resolve(
                    {
                        data: result.data                        
                    }
                )
            }, reject)
    })
}

export const personalRankingService = {
    getpersonalRankings    
}