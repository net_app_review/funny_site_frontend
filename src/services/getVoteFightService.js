import apiClient from "../config/apiClient";

// http://localhost:5054/api/v1/votefights/1-clinton-vs-trump

const SVC_NAME = '/api/v1/votefights'

const getVoteFight = (vote_id) => {    

    return new Promise ( (resolve, reject) => {
        apiClient.get(`${SVC_NAME}/${vote_id}`)
            .then( result => {                
                resolve(
                    {
                        data: result.data                        
                    }
                )
            }, reject)
    })
}

export const getVoteFightService = {
    getVoteFight    
}