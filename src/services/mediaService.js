import apiMedia from "../config/apiMedia"

const SVC_NAME = '/api/v1/videos/create'

// http://localhost:5055/api/v1/videos/create

const uploadVideo = (videofile, unique_id) => {
    return new Promise( (resolve, reject) => {
        
        let fileForm = new FormData();        
        // fileForm.append('videofile', videofile, unique_id + videofile.name)
        fileForm.append('videofile', videofile, unique_id + "_video.mp4")
        
        // post the video
        apiMedia.post(`${SVC_NAME}`, fileForm)
            .then(result => {                
                resolve(result)
            })
            .catch(reject)        
    })
}

export const mediaService = {
    uploadVideo
}