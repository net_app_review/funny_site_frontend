import apiClient from "../config/apiClient";

const SVC_NAME = '/api/v1/tags/featured-tags'

const getFeaturedTags = () => {    
    return new Promise ( (resolve, reject) => {
        apiClient.get(`${SVC_NAME}`)
            .then( result => {                
                resolve(result.data)  
            }, reject)
    })
}

export const featuredTagsService = {
    getFeaturedTags    
}