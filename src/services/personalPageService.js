import apiClient from "../config/apiClient";

// http://52.77.199.182:5050/api/v1/articles/user_articles?user_id=31&page=1

const SVC_NAME = '/api/v1/articles/user_articles'
const USERINFO_SVC_NAME = '/api/v1/users'

const getArticles = (user_id,page) => {
    return new Promise ( (resolve, reject) => {
        apiClient.get(`${SVC_NAME}?user_id=${user_id}&page=${page}`)
            .then( result => {                
                resolve(
                    {
                        data: result.data,
                        totalPage: result.headers["total_page"],
                        totalCount: result.headers["total_article"]
                    }
                )
            }, reject)
    })
}

// axios.get(`${process.env.REACT_APP_API_URL}/api/v1/users/${props.user_id}`)
			

const getUserInfo = (user_uuid) => {
    return new Promise ( (resolve, reject) => {
        apiClient.get(`${USERINFO_SVC_NAME}/${user_uuid}`)
            .then( result => {                
                resolve(
                    {
                        data: result.data,
                        subscribed: result.subscribed
                    }
                )
            }, reject)
    })
}

export const personalPageService = {
    getUserInfo,
    getArticles
}