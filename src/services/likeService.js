import apiClient from "../config/apiClient";
import {LikeType} from "../model/LikeTypeEnum";

const SVC_NAME = '/api/v1/likes'

const like =  (likedObj, objId, likeStatus, likeId)  => {

    // let likedObj_id = 'article_id'

    let data = {}
        switch (likeStatus) {
            case LikeType.LIKE: // => change from like to unlike
                data = {
                    id: likeId,
                    like: {
                        [likedObj]: objId,
                        liketype: LikeType.NO_VALUE
                    }
                }
                break
            case LikeType.DISLIKE: // => change from dislike to unlike
                data = {
                    id : likeId,
                    like: {
                        [likedObj]: objId,
                        liketype: LikeType.LIKE
                    }
                }
                break
            case LikeType.NO_VALUE: // => change from no_value to like
                data = {
                    id : likeId,
                    like: {
                        [likedObj]: objId,
                        liketype: LikeType.LIKE
                    }
                }
                break
            case LikeType.NO_ACTION:
                data = {
                    id : "",
                    like: {
                        [likedObj]: objId,
                        liketype: LikeType.LIKE
                    }
                }
                break
            case LikeType.HIDE:
                data = {
                    id : likeId,
                    like: {
                        [likedObj]: objId,
                        liketype: LikeType.HIDE
                    }
                }
                break
            default:
                console.log("default")
            
        }
    return new Promise( (resolve, reject ) => {
        apiClient.post(`${SVC_NAME}/handle_like`, data)
            .then(result => {
                resolve(result.data)
            })
            .catch(reject)
    })
}

const dislike = (likedObj, objId, likeStatus, likeId) => {
    let data = {}
        switch (likeStatus) {
            case LikeType.LIKE: // => change from like to dislike
                data = {
                    id: likeId,
                    like: {
                        [likedObj]: objId,
                        liketype: LikeType.DISLIKE
                    }
                }
                break
            case LikeType.DISLIKE: // => change from dislike to un dislike
                data = {
                    id : likeId,
                    like: {
                        [likedObj]: objId,
                        liketype: LikeType.NO_VALUE
                    }
                }
                break
            case LikeType.NO_VALUE: // => change from no_value to dislike
                data = {
                    id : likeId,
                    like: {
                        [likedObj]: objId,
                        liketype: LikeType.DISLIKE
                    }
                }
                break
            case LikeType.NO_ACTION:
                data = {
                    id : "",
                    like: {
                        [likedObj]: objId,
                        liketype: LikeType.DISLIKE
                    }
                }
                break
            default:
                console.log("default")
        }
    return new Promise( (resolve, reject ) => {
        apiClient.post(`${SVC_NAME}/handle_like`, data)
            .then(result => {
                resolve(result.data)
            })
            .catch(reject)
    })
}

const hide = (articleId, likeStatus, likeId) => {
    let data = {}
    data = {
        id: likeId,
        like: {
            article_id: articleId,
            liketype: likeStatus
        }
    }

    return new Promise( (resolve, reject ) => {
        apiClient.post(`${SVC_NAME}/handle_like`, data)
            .then(result => {
                resolve(result.data)
            })
            .catch(reject)
    })
}

export const LikeService = {
    like,
    dislike,
    hide
}