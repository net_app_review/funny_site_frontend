import {useContext} from 'react'
import apiClient from "../config/apiClient";
import {LoggedInContext} from '../Store'

const SVC_NAME = '/api/v1/articles'

const CheckLoginService = () => {    

    const [stLoggedIn, setstLoggedIn] = useContext(LoggedInContext)
    

    if (stLoggedIn === "LOGGED_IN"){
        return "LOGGED_IN"
    } else {
        return "NOT_LOGGED_IN"
    }

}

export default CheckLoginService 