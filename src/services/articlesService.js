import apiClient from "../config/apiClient";

const SVC_NAME = '/api/v1/articles'

const getArticles = (page) => {    

    return new Promise ( (resolve, reject) => {
        apiClient.get(`${SVC_NAME}?page=${page}`)
            .then( result => {                
                resolve(
                    {
                        data: result.data                        
                    }
                )
            }, reject)
    })
}

const createArticles = (requestBody) => {
    return new Promise( (resolve, reject) => {
        apiClient.post(`${SVC_NAME}/create_article`, requestBody)
            .then(result => {
                resolve(result)
            })
            .catch(reject)
    })
}

const updateArticles = (requestBody) => {
    return new Promise( (resolve, reject) => {
        apiClient.post(`${SVC_NAME}/update_article`, requestBody)
            .then(result => {
                resolve(result)
            })
            .catch(reject)
    })
}

export const articlesService = {
    getArticles,
    createArticles,
    updateArticles
}