import React from 'react';
import Helmet from 'react-helmet';

const SEO = ({ pageProps }) => {
  return (
    <Helmet>
      <title>{pageProps.title}</title>

      {/* facebook og protocol messages */}
      
      <meta property="og:type" content={pageProps.type} />
      <meta property="og:title" content={pageProps.title} />
      <meta property="og:description" content={pageProps.description} />
      <meta property="og:url" content={pageProps.url} />
      <meta property="og:image" content={pageProps.image} />      
      <meta property="og:image:width" content={pageProps.image_width} />
      <meta property="og:image:height" content={pageProps.image_height} />

      {/* twitter card protocol messages */}

      <meta name="twitter:card" content="funniq_large_image" />
      <meta name="twitter:site" content="@FunniqPage" />
      <meta name="twitter:title" content={pageProps.title} />
      <meta name="twitter:description" content={pageProps.description} />
      <meta name="twitter:image" content={pageProps.image} />
    </Helmet>    
  )
}

export default SEO;