import apiClient from "../config/apiClient";

const postSubComment = (sub_comment_content, parent_comment_id, stRepliedUserId) => {
        let data = {
          subcomment: {
            content: sub_comment_content,
            comment_id: parent_comment_id,
            replytouser: stRepliedUserId
          }
        }

        
        

    let SVC_URL="/api/v1/subcomments/create"

    return new Promise ( (resolve, reject) => {                              
        apiClient.post(`${process.env.REACT_APP_API_URL}${SVC_URL}`, data)
            .then( result => {                
                resolve(
                    {
                        data: result                                                
                    }
                )
        }, reject)
    })
}            

export const postSubCommentService = {
    postSubComment
}