import apiClient from "../config/apiClient";

// http://52.77.199.182:5050/api/v1/articles/user_articles?user_id=31&page=1

const fetchComment = (obj, id, page) => {

    let SVC_NAME=''

    if (obj === "article") {
        SVC_NAME = '/api/v1/comments/article_comments'
    } else if (obj === "votefight") {
        SVC_NAME = '/api/v1/comments/votefight_comments'
    }

    return new Promise ( (resolve, reject) => {
        apiClient.get(`${SVC_NAME}?${obj}_id=${id}&page=${page}`)
            .then( result => {                
                resolve(
                    {
                        data: result.data,
                        totalPage: result.headers["total_page"],
                        totalCount: result.headers["total_article"]
                    }
                )
            }, reject)
    })
    
}

export const fetchCommentService = {
    fetchComment
}