import React, {useState} from 'react';
// import { currencies } from 'country-data';

export const SignUpModalContext = React.createContext(false);
export const SignInModalContext = React.createContext(false);
export const LoggedInContext = React.createContext("NOT_LOGGED_IN");
export const CurrentUserContext = React.createContext({});

const Store = ({children}) => {
    const[SignUpModal, setSignUpModal] = useState(false);
    const[SignInModal, setSignInModal] = useState(false);
    const[LoggedIn, setLoggedIn] = useState("NOT_LOGGED_IN");
    const[CurrentUser, setCurrentUser] = useState({});

    return(
        <CurrentUserContext.Provider value={[CurrentUser, setCurrentUser]}>
            <LoggedInContext.Provider value={[LoggedIn, setLoggedIn]}>
                <SignUpModalContext.Provider value={[SignUpModal, setSignUpModal]}>
                    <SignInModalContext.Provider value={[SignInModal, setSignInModal]}>
                        {children}
                    </SignInModalContext.Provider>
                </SignUpModalContext.Provider>
            </LoggedInContext.Provider>
        </CurrentUserContext.Provider> 
    );
};

export default Store;