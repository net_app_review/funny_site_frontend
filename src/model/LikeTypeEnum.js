export const LikeType = {
    UPVOTE: 'upvote',
    LIKE: 'like',
    DISLIKE: 'dislike',
    NO_VALUE: 'no_value',
    NO_ACTION: 'no_action',
    DIS_VOTE: 'dis_vote',
    HIDE: 'hide'
}
