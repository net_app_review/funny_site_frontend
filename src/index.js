import React, {Suspense} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './_style.sass'
import "./multiLanguage/i18next";
// import swal from 'sweetalert'

// swal("Welcome to Funniq", "At the moment we are in beta version and will release the most viable version very soon , please contact us at facebook.funniq.com or send email to support@funniq.com for support", "error");

ReactDOM.render(
    <Suspense fallback="">
        <App />
    </Suspense>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

//react snap

// const rootElement = document.getElementById("root");

// if (rootElement.hasChildNodes()) {
//   ReactDOM.hydrate(<App />, rootElement);
// } else {
//   ReactDOM.render(<App />, rootElement);
// }

// serviceWorker.unregister();
serviceWorker.register();
